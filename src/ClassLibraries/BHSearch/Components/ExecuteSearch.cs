﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BHSearch.Models;
using SolrNet;
using SolrNet.Commands.Parameters;
using SolrNet.Impl;
using System.Web;
using CommonServiceLocator;

namespace BHSearch.Components
{
    public class ExecuteSearch
    {
        public SolrResponse Execute(Models.SolrQuery query)
        {
            try
            {
                // Filters
                FiltersFacets filtersFacets = new FiltersFacets();

                // Create an object to hold results
                SolrQueryResults<Dictionary<string, object>> solrResults;
                SolrResponse queryResponse = new SolrResponse
                {
                    OriginalQuery = query
                };

                // Echo back the original query 
               // queryResponse.OriginalQuery = query;


                // Set the Handler
                var executor = ServiceLocator.Current.GetInstance<ISolrQueryExecuter<Dictionary<string, object>>>() as SolrQueryExecuter<Dictionary<string, object>>;

                if (string.IsNullOrEmpty(query.Handler))
                    throw new Exception("Handler must be set");
                //executor.Handler = query.Handler;
                executor.DefaultHandler = query.Handler;
                //Set Options
                var start = new StartOrCursor.Start(query.Start);
                QueryOptions queryOptions = new QueryOptions
                {
                    Rows = query.Rows,
                    //Start = query.Start,
                    StartOrCursor =  start, // set initial state
                    FilterQueries = filtersFacets.BuildFilterQueries(query),
                    Facet = filtersFacets.BuildFacets(query)
                };

                // Append Distance Filters
                if (!string.IsNullOrEmpty(query.Location) && query.Location.IndexOf(',') > -1 && !string.IsNullOrEmpty(query.Distance))
                {
                    queryOptions.OrderBy = new[] { new SortOrder("geodist()", Order.ASC) };
                    queryOptions.FilterQueries.Add(new LocalParams { { "type", "geofilt" } } + new SolrNet.SolrQuery(""));
                    queryOptions.ExtraParams = new Dictionary<string, string>
                    {
                        { "d", query.Distance },
                        { "sfield", "location_p" },
                        { "pt", query.Location },
                        // return distance in miles
                        { "fl", "Distance:product(geodist(),0.62137)"},
                    };
                }
                else if (!string.IsNullOrEmpty(query.Location) && query.Location.IndexOf(',') > -1)
                {

                    queryOptions.ExtraParams = new Dictionary<string, string>
                    {
                        { "sfield", "location_p" },
                        { "pt", query.Location },
                        // return distance in miles
                        { "fl", "Distance:product(geodist(),0.62137)"},
                    };
                }

                // Sort
                //if (!string.IsNullOrEmpty(query.Sort) && query.Sort.Equals("ASC", StringComparison.OrdinalIgnoreCase))
                //{
                //    queryOptions.OrderBy = new[] { new SortOrder("sort_field", Order.ASC) };
                //}
                //else if (!string.IsNullOrEmpty(query.Sort) && query.Sort.Equals("DESC", StringComparison.OrdinalIgnoreCase))
                //{
                //    queryOptions.OrderBy = new[] { new SortOrder("sort_field", Order.DESC) };
                //}
                //else if (!string.IsNullOrEmpty(query.Location))
                //{
                //    queryOptions.OrderBy = new[] { new SortOrder("geodist()", Order.ASC) };
                //}

                // Sort
                if (!string.IsNullOrEmpty(query.Sort) && query.Sort.Equals("ASC", StringComparison.OrdinalIgnoreCase))
                {
                    queryOptions.OrderBy = new[] {
                       new SortOrder("sort_field", Order.ASC),
                       new SortOrder("lastname_s", Order.ASC)
                   };
                }
                else if (!string.IsNullOrEmpty(query.Sort) && query.Sort.Equals("DESC", StringComparison.OrdinalIgnoreCase))
                {
                    queryOptions.OrderBy = new[] {
                       new SortOrder("sort_field", Order.DESC),
                       new SortOrder("lastname_s", Order.DESC)
                   };
                }
                else if (!string.IsNullOrEmpty(query.Location))
                {
                    queryOptions.OrderBy = new[] { new SortOrder("geodist()", Order.ASC) };
                }


                //Execute the query
                ISolrQuery solrQuery = new SolrNet.SolrQuery(query.Query);

                solrResults = executor.Execute(solrQuery, queryOptions);

                //Set Response
                ResponseExtraction extractResponse = new ResponseExtraction();

                extractResponse.SetHeader(queryResponse, solrResults);
                extractResponse.SetBody(queryResponse, solrResults);
                extractResponse.SetFacets(queryResponse, solrResults);

                //Return response;
                return queryResponse;
            }
            catch (Exception e)
            {
                throw new Exception("Search Execution Error", e);
            }

        }
    }
}
