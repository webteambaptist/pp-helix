﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BHSearch.Helpers;
using BHSearch.Models;
using SolrNet;
using SolrNet.Commands.Parameters;
using System.Web;

namespace BHSearch.Components
{
    public class FiltersFacets
    {
        public ICollection<ISolrQuery> BuildFilterQueries(Models.SolrQuery query)
        {
            try
            {
                // Initialize filters to return
                ICollection<ISolrQuery> filters = new List<ISolrQuery>();

                // Loop through values and add to filters
                foreach (KeyValuePair<string, string> entry in query.Filters)
                {
                    if (!String.IsNullOrEmpty(entry.Value))
                    {
                        List<SolrQueryByField> filter = new List<SolrQueryByField>();

                        foreach (string val in entry.Value.Split(','))
                        {
                            filter.Add(new SolrQueryByField(entry.Key, val));
                        }
                        filters.Add(new LocalParams { { "tag", entry.Key.Substring(0, 3) } } + new SolrMultipleCriteriaQuery(filter, "OR"));
                    }
                }
                return filters;
            }
            catch (Exception e)
            {
                throw new Exception("Set Filter Error", e);
            }
        }

        internal FacetParameters BuildFacets(Models.SolrQuery query)
        {
            try
            {
                // Set Facets
                var facetParams = new FacetParameters();
                foreach (string facet in query.Facets)
                {
                    facetParams.Queries.Add(new SolrFacetFieldQuery(new LocalParams { { "ex", facet.Substring(0, 3) } } + facet) { MinCount = 1 });
                }
                return facetParams;
            }
            catch (Exception e)
            {
                throw new Exception("Set Facet Error", e);
            }
        }

    }
}
