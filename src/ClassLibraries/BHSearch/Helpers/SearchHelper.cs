﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BHSearch.Models;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace BHSearch.Helpers
{
    public class ViewHelper
    {
        public static string RenderViewToString(ControllerContext controllerContext, string viewName, object model)
        {
            try
            {
                controllerContext.Controller.ViewData.Model = model;

                using (var stringWriter = new System.IO.StringWriter())
                {
                    var viewResult = ViewEngines.Engines.FindPartialView(controllerContext, viewName);
                    var viewContext = new ViewContext(controllerContext, viewResult.View, controllerContext.Controller.ViewData, controllerContext.Controller.TempData, stringWriter);
                    viewResult.View.Render(viewContext, stringWriter);
                    viewResult.ViewEngine.ReleaseView(controllerContext, viewResult.View);
                    return stringWriter.GetStringBuilder().ToString();
                }
            }
            catch (Exception ex)
            {
                return "Error - " + ex.ToString();
            }
        }
    }
}
