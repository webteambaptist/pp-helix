﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BHSearch.Models
{
    public class PhysicianResultsView
    {
        public PhysicianResultsView()
        {
            Start = 0;
            Physicians = new List<PhysicianSearch>();
        }

        public int Start { get; set; }
        public List<PhysicianSearch> Physicians { get; set; }
    }
}