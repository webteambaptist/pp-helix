﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BHSearch.Models
{
    public class Query
    {
        public Query()
        {
            Rows = 10;
            Start = 0;
            Q = "*";
            Location = "";
            Distance = "";
        }
        public string Q { get; set; }
        public int Start { get; set; }
        public int Rows { get; set; }
        public string Location { get; set; }
        public string Distance { get; set; }
    }
}
