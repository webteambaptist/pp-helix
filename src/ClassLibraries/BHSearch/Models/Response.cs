﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BHSearch.Models
{
    public class Response
    {
        public int TotalHits { get; set; }

        public int QueryTime { get; set; }

        public int Status { get; set; }
    }
}
