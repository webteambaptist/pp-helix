﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BHSearch.Models
{
    public class SiteSearchResult
    {
        //Base
        public string ContentID { get; set; }  // ContentID == Name in Sitecore
        public string UniqueID { get; set; }   // UniqueID == Item ID in Sitecore
        public string Path { get; set; }
        public string Template { get; set; }
        public string Type { get; set; }

        //People
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Suffix { get; set; }
        public string PrimarySpecialty { get; set; }
        public ArrayList Biography { get; set; }
        public string PhotoURL { get; set; }

        //Event
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public bool AllDay { get; set; }
        public string Location { get; set; }
        public ArrayList Details { get; set; }

        //Other
        public string Title { get; set; }
        public ArrayList SubHeading { get; set; }
    }
}
