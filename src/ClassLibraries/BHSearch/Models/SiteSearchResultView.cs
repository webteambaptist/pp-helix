﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BHSearch.Models
{
    public class SiteSearchResultView
    {
        public SiteSearchResultView()
        {
            Start = 0;
            Results = new List<SiteSearchResult>();
        }

        public int Start { get; set; }
        public List<SiteSearchResult> Results { get; set; }
    }
}
