﻿using System;
using BHUser.Models;

namespace BHUser.Components
{
    public class Login
    {
        /// <summary>
        /// Sample Authentication check
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public bool IsAuthenticated(string userName, string password)
        {
            try
            {
                if (!string.IsNullOrEmpty(userName) && !string.IsNullOrEmpty(password))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                //throw;
                return false;
            }
        }

        public User GetUser(string userName, string password)
        {
            User _bppUser = new User();
            try
            {
                if (!string.IsNullOrEmpty(userName) && !string.IsNullOrEmpty(password))
                {
                    if (userName.ToString().ToLower().Equals("admin"))
                        _bppUser.UserType = BH_User_Types.Admin;
                    else
                        _bppUser.UserType = BH_User_Types.Provider;
                }
                else
                {
                    _bppUser.UserType = BH_User_Types.Public;
                }
                return _bppUser;

                //if (!string.IsNullOrEmpty(userName) && !string.IsNullOrEmpty(password))
                //{
                //    _bppUser.UserType = User.User_Types.Admin;
                //    return _bppUser;
                //}
                //else
                //{
                //    _bppUser.UserType = User.User_Types.Provider;
                //    return _bppUser;
                //}
            }
            catch (Exception)
            {
                //throw;
                return _bppUser;
            }
        }
    }
}
