﻿using System;
using System.Collections.Generic;

namespace BHUser.Models
{
    public class Physician : User
    {
        #region Physician Specific Properties
        public string PhotoURL { get; set; }
        public string PrimarySpecialty { get; set; }
        public string[] OtherSpecialties { get; set; }
        public string JobTitle { get; set; }
        public string Suffix { get; set; }
        public string[] Education { get; set; }
        public string[] Certification { get; set; }
        public string[] Internship { get; set; }
        public string[] Residency { get; set; }
        public string Biography { get; set; }
        public string[] AcceptedInsurance { get; set; }
        public string[] AgesTreated { get; set; }
        public string[] Publications { get; set; }
        public string[] Languages { get; set; }
        //public bool IsAcceptingPatients { get; set; }
        //public bool IsBPP { get; set; }
        public string PracticeName { get; set; }
        public string[] PrimaryLocation { get; set; }
        public string PrimaryLocationLatLng { get; set; }
        //public string[] OtherLocations { get; set; }
        public List<Address> OtherLocations { get; set; }
        public string[] OtherLocationsLatLngs { get; set; }
        #endregion

        #region Additional Properties
        public string EchoID { get; set; }
        public List<SocialFeed> SocialFeed { get; set; }
        public SocialFollow SocialFollow { get; set; }
        public string CellPhoneNumber { get; set; }
        public string PagerNumber { get; set; }
        #endregion
    }

    public class SocialFeed
    {
        public int SFID { get; set; }
        public int PostID { get; set; }
        public string PostAuthor { get; set; }
        public string PostDate { get; set; }
        public string PostBody { get; set; }
        public string CreatedDT { get; set; }
        public string LastUpdateDT { get; set; }
        public string ECHOID { get; set; }
        public string PostAuthorEchoID { get; set; }
        public List<SocialAttachments> attachments { get; set; }
    }

    public class SocialFollow
    {
        public bool Subscribed { get; set; }
        public List<SocialFollowers> Subscribers { get; set; }
        public List<SocialFollowers> Subscriptions { get; set; }
    }

    public class SocialFollowers
    {
        public int ID { get; set; }
        public string SubscriptionPhysicianID { get; set; }
        public string SubscriptionPhysicianName { get; set; }
        public string SubscribersPhysicianID { get; set; }
        public string SubscribersPhysicianName { get; set; }
    }

    public class SocialAttachments
    {
        public int ID { get; set; }
        public int PostID { get; set; }
        public Nullable<DateTime> CreateDT { get; set; }
        public string PostFileName { get; set; }
        public string PostAttachment { get; set; }
        public string SocialFeedMain { get; set; }
    }

    public class Address
    {
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string Fax1 { get; set; }
        public string Fax2 { get; set; }
    }
}
