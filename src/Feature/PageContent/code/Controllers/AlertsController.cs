﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PP93Helix.Feature.PageContent.Models;
using Sitecore.Data;
using Sitecore.Data.Fields;
using System.Net;
using PP93Helix.Feature.PageContent.Helpers;
using Sitecore.Configuration;
using System.IO;
using System.Web.Script.Serialization;

namespace PP93Helix.Feature.PageContent.Controllers
{
  public class AlertsController : Controller
  {

    string ppAPIGetAlerts = Settings.GetSetting("ppAPIGetAlerts");

    public ActionResult _Alerts()
    {
      Alerts alerts = new Alerts();
      string now = DateTimeOffset.UtcNow.ToString("yyyyMMddTHHmmssZ");
      //string now = DateTime.Now.ToString("yyyyMMddTHHmmssZ");
      string query = "//*[@@templateid = '{0586BAD9-5044-474F-B160-548BEE8DC43B}' and @@name != '__Standard Values' and @StartDate < '" + now + "' and @ExpireDate > '" + now + "']";
      Database web = Sitecore.Configuration.Factory.GetDatabase("web");
      var currentAlertItems = web.SelectItems(query);

      alerts.CurrentAlerts = (from alert in currentAlertItems.OrderBy(x => x.Fields["ExpireDate"].Value)
                              select new Alert
                              {
                                Title = alert.Fields["Title"].Value,
                                Details = alert.Fields["Details"].Value,
                                ExpireDate = (DateField)alert.Fields["ExpireDate"]
                              }).ToList();

      alerts.SystemAlerts = new List<SystemAlert>();

      alerts.SystemAlerts = GetSystemAlerts();

      return View(alerts);
    }

    private List<SystemAlert> GetSystemAlerts()
    {
      List<SystemAlert> sysAlerts = new List<SystemAlert>();

      try
      {
        HttpWebResponse httpWebResponse = null;
        httpWebResponse = APIHelper.GetDataFromAPI(ppAPIGetAlerts);
        if (httpWebResponse != null)
        {
          Sitecore.Diagnostics.Log.Info("PhysicanPortal.Controllers.AlertsController :: GetSystemAlerts -> API Response : " + httpWebResponse, httpWebResponse);
          Sitecore.Diagnostics.Log.Info("PhysicanPortal.Controllers.AlertsController :: GetSystemAlerts -> API Response Code: " + httpWebResponse.StatusCode, httpWebResponse);

          using (var reader = new StreamReader(httpWebResponse.GetResponseStream()))
          {
            JavaScriptSerializer js = new JavaScriptSerializer();
            var objText = reader.ReadToEnd();
            sysAlerts = js.Deserialize<List<SystemAlert>>(objText);
          }
        }
      }
      catch (Exception ex)
      {
        Sitecore.Diagnostics.Log.Error("PhysicanPortal.Controllers.AlertsController :: GetSystemAlerts -> Exception : " + ex.ToString(), ex, ex.Data);
        sysAlerts = null;
        TempData["SystemAlertsResponse"] = "500 Internal Error";
      }

      return sysAlerts;
    }
  }
}