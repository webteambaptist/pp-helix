﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Sitecore.Data;
using Sitecore.Data.Fields;
using PP93Helix.Feature.PageContent.Helpers;
using PP93Helix.Feature.PageContent.Models;
using Sitecore.Configuration;
using System.Net;
using System.IO;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Script.Serialization;
using Sitecore;
using Newtonsoft.Json;
using RestSharp;
using Sitecore.Diagnostics;
using HttpCookie = System.Web.HttpCookie;


namespace PP93Helix.Feature.PageContent.Controllers
{
    public class AppCatalogQuickLinksController : Controller
    {
        private readonly string _microserviceBaseAddress = Settings.GetSetting("ppMicroServiceBaseAddress");
        //Quicklinks
        private readonly string _ppMicroserviceGetQLinks = Settings.GetSetting("ppMicroserviceGetQLinks");
        private readonly string _ppMicroserviceSaveQLinks = Settings.GetSetting("ppMicroserviceSaveQLinks");

        //Citrix
        private readonly string _ppMicroserviceGetSfAuth = Settings.GetSetting("ppMicroserviceGetSfAuth");
        private readonly string _ppMicroservicePostAppsList = Settings.GetSetting("ppMicroservicePostAppsList");
        private readonly string _ppMicroservicePostLaunchApp = Settings.GetSetting("ppMicroservicePostLaunchApp");

        private readonly string _icaPath = Settings.GetSetting("icaPath");
        private readonly string _icaPathUrl = Settings.GetSetting("icaPathUrl");

        public ActionResult _appCatalogQuickLinks()
        {
            var appCatQLinks = new Models.AppCatalogQuickLinks();
            var userName = string.Empty;
            if (Context.User.IsAuthenticated)
            {
              Log.Info("Hit Index " + Context.User.Name, Context.User.Name);
              userName = Context.GetUserName();
              Log.Info(string.Concat("User Authenticated - ", userName), Context.User);
              
              var claim = ClaimsPrincipal.Current.Identities.First<ClaimsIdentity>().Claims.FirstOrDefault<Claim>((Claim x) => x.Type == "pwd");
              if (claim != null)
              {
                try
                {
                  var value = claim.Value;
                  var str = string.Concat(Context.GetUserName(), ":", value);
                  Response.Cookies.Clear();
                  Response.Cookies.Add(new HttpCookie("scuser", str));
                }
                catch (Exception e)
                {
                  Log.Error("Exception occurred while saving cookie :: _appCatalogQuickLinks " + e.Message, this);
                }
                 
              }
            }
            else
            {
              return Redirect("~/Login");
            }
            try
            {

              //Citrix Apps
    
              appCatQLinks.AppCred = new AppCred();
              appCatQLinks.AppCred = GetCitrixCreds();
              
              //Get Citrix List
              appCatQLinks.AppThumbs = new List<AppThumb>();
              var sessionId = appCatQLinks.AppCred.SessionId;
              var csrfToken = appCatQLinks.AppCred.CsrfToken;
              var authId = appCatQLinks.AppCred.AuthID;
              appCatQLinks.AppThumbs = GetCitrixApps(sessionId, authId, csrfToken);
             

              //Quicklinks
              appCatQLinks.QuickLinks = new List<QuickLink>();
              var claim = ClaimsPrincipal.Current.Identities.First().Claims.FirstOrDefault(x => x.Type == "echoid");
              
              var echoID = "";
              if (claim!=null)
              {
                echoID = claim.Value;
                Log.Info($"User {userName}'s EchoId is {echoID}", this);
                appCatQLinks.QuickLinks = GetQuickLinks(echoID);
              }
              else
              {
                Log.Info($"User {userName} does not have an echoId, using username for Quick Links.", this);
                if (!string.IsNullOrEmpty(userName))
                  appCatQLinks.QuickLinks = GetQuickLinks(userName);
              }

              appCatQLinks.BHLinks = new List<QuickLink>();

              var linkFolderId = new ID("99CD6607-EB6B-424A-A301-C441DEC79C99");
              var linkFolder = Context.Database.GetItem(linkFolderId);

              var filteredItems =
                RenderingHelper.FilterItemsByTagsMatchingUserRoles(linkFolder.GetChildren().ToArray());
              // we're not going to filter by role anymore. 
              foreach (var link in filteredItems)
              {
                var newLink = new QuickLink();
                LinkField _link = link.Fields["Link"];
                if (_link == null) continue;
                var url = _link.GetFriendlyUrl();
                url = RenderingHelper.parseGeneralLink(_link);
                newLink.LinkURL = url;
                newLink.LinkTitle = _link.Text;
                appCatQLinks.BHLinks.Add(newLink);
                
              }
            }
            catch (Exception ex) {
               Log.Error("PhysicianPortal.Controllers.AppCatalogQuickLinksController Error -> " + ex.ToString(), this);
            }
            return PartialView(appCatQLinks);
        }



       /// <summary>
       /// step 2 get list of apps per user
       /// </summary>
       /// <param name="sessionID"></param>
       /// <param name="AuthID"></param>
       /// <param name="CsrfToken"></param>
       /// <returns></returns>
        public List<AppThumb> GetCitrixApps(string sessionID, string AuthID, string CsrfToken)
        {

            var citrixApps = new List<AppThumb>();
            try
            {
              var json = "{\"SessionId\":\" " + sessionID.Trim() + "\",\"CsrfToken\":\" " + CsrfToken.Trim() +
                            "\",\"AuthID\":\"" + AuthID.Trim() + "\"}".ToString();
                var httpWebResponse = APIHelper.PostDataToMicroService(_ppMicroservicePostAppsList, json);
                if (httpWebResponse != null)
                {
                
                    using (var reader = new StreamReader(httpWebResponse.GetResponseStream()))
                    {
                        var js = new JavaScriptSerializer();
                        var objText = reader.ReadToEnd();
                        citrixApps = js.Deserialize<List<AppThumb>>(objText);
                    }
                }
            }
            catch (Exception)
            {

            }
            return citrixApps;
        }

        /// <summary>
        /// step 1, get citrix creds
        /// </summary>
        /// <returns></returns>
        public AppCred GetCitrixCreds()
        {
            var _appCreds = new AppCred();
            //get scuser auth cookie
            if (Request.Cookies["scuser"] == null) return _appCreds;
            var scuser = Request.Cookies["scuser"].Value;
            try
            {
              //Authenticate User
              var httpWebResponse = APIHelper.GetDataFromMicroService(_ppMicroserviceGetSfAuth, "scuser|" + scuser);
              if (httpWebResponse != null)
              {
                       
                using (var reader = new StreamReader(httpWebResponse.GetResponseStream()))
                {
                  var js = new JavaScriptSerializer();
                  var objText = reader.ReadToEnd();
                  objText = objText.Replace("ASP.NET_SessionId", "SessionId");
                  objText = objText.Replace("CtxsAuthId", "AuthID");
                  _appCreds = js.Deserialize<AppCred>(objText.ToString());
                }
              }
            }
            catch (Exception ex)
            {
              _appCreds = null;
              TempData["QuickLinksResponse"] = "500 Internal Error" + ex.Message;
            }
            return _appCreds;
        }


        public List<QuickLink> GetQuickLinks(string echoID)
        {
            var qlinks = new List<QuickLink>();

            if (!string.IsNullOrEmpty(echoID))
            {
                try
                {
                    var httpWebResponse = APIHelper.GetDataFromMicroService(_ppMicroserviceGetQLinks, "echoid|" + echoID);
                    if (httpWebResponse != null)
                    {
                      
                        using (var reader = new StreamReader(httpWebResponse.GetResponseStream()))
                        {
                            var js = new JavaScriptSerializer();
                            var objText = reader.ReadToEnd();
                            qlinks = js.Deserialize<List<QuickLink>>(objText);
                        }
                    } 
                }
                catch (Exception)
                {
                     qlinks = null;
                    TempData["QuickLinksResponse"] = "500 Internal Error";
                }
            }
            else
            {
                qlinks = null;
                TempData["QuickLinksResponse"] = "No Echo ID";
            }
            return qlinks;
        }

        /// <summary>
        /// version for retrieving ica as part of initial load...
        /// </summary>
        /// <param name=""></param>
        /// <param name="strApiInput"></param>
        /// <param name="title"></param>
        /// <returns></returns>
        public string DownloadICA2(string strApiInput, string title)
        {
            var req = Request.InputStream;
            req.Seek(0, SeekOrigin.Begin);
            
            var request = (HttpWebRequest)WebRequest.Create(_microserviceBaseAddress + _ppMicroservicePostLaunchApp);
            request.ContentType = "Application/Json";
            request.Method = "POST";


            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                streamWriter.Write(strApiInput);
                streamWriter.Flush();
                streamWriter.Close();
            }
            
            try
            {

                using (var response = (HttpWebResponse)request.GetResponse())
                {
                    Log.Info("PP.Controllers.ApplicationController :: DownloadAttachment -> response : " + JsonConvert.SerializeObject(response), response);
                    var dataStream = response.GetResponseStream();
                    var reader = new StreamReader(dataStream);
                    var responseFromServer = reader.ReadToEnd();
                    var bytes = System.Text.Encoding.ASCII.GetBytes(responseFromServer);

                    var r = new Random();
                    var next = r.Next().ToString();

                    ImpersonationHelper.Impersonate("bh", "wedadmsvc", "w3d4DM$vC", delegate //to do make config
                    {
                      var path = _icaPath;

                      if (!Directory.Exists(path))
                      {
                          Directory.CreateDirectory(path);
                      }
                      var icapath = path + next + ".ica";
                      System.IO.File.WriteAllBytes(icapath, bytes.ToArray());
                    });
                    return _icaPathUrl + next + ".ica";
                }
            }
            catch (Exception ex)
            {
                Log.Error("PP.Controllers.ApplicationController :: DownloadAttachment -> Exception : " + ex.ToString(), ex.Message);
                return ex.Message;
            }

        }

        public ActionResult DownloadICA()
        {

            Stream req = Request.InputStream;
            req.Seek(0, System.IO.SeekOrigin.Begin);
            string text = new StreamReader(req).ReadToEnd();

            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(_microserviceBaseAddress + _ppMicroservicePostLaunchApp);
            request.ContentType = "Application/Json";
            request.Method = "POST";


            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                string json = text;
                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }
            try
            {

                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    Log.Info("PP.Controllers.ApplicationController :: DownloadAttachment -> response : " + JsonConvert.SerializeObject(response), response);
                    Stream dataStream = response.GetResponseStream();
                    StreamReader reader = new StreamReader(dataStream);
                    string responseFromServer = reader.ReadToEnd();
                    byte[] bytes = System.Text.Encoding.ASCII.GetBytes(responseFromServer);

                    Random r = new Random();
                    string next = r.Next().ToString();

                    ImpersonationHelper.Impersonate("bh", "wedadmsvc", "w3d4DM$vC", delegate //to do make config
                    {
                        string path = _icaPath;
                        
                        if (!Directory.Exists(path))
                        {
                            Directory.CreateDirectory(path);
                        }
                        string icapath = path + next + ".ica";
                        System.IO.File.WriteAllBytes(icapath, bytes.ToArray());
                    });
            return Content(_icaPathUrl + next + ".ica");

          }
            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "File not Found");
            }
        }

        [HttpPost]
        public void PostQuickLinks()
        {
            var req = Request.InputStream;
            req.Seek(0, SeekOrigin.Begin);
            var qlinks = new StreamReader(req).ReadToEnd();

            try
            {
              var client = new RestClient(_microserviceBaseAddress);
              var request = new RestRequest(_ppMicroserviceSaveQLinks) {Method = Method.POST};
              request.AddHeader("ContentType", "application/json");
              request.AddHeader("links",qlinks);
              Log.Info("PhysicianPortal.AppCatalogQuickLinksController :: Header qlinks" + qlinks, qlinks);
              var r = client.Execute(request);
              Log.Info("PhysicianPortal.AppCatalogQuickLinksController :: Response" + r.ResponseStatus, r);
              if (!r.IsSuccessful)
              {
                Log.Error("PhysicianPortal.AppCatalogQuickLinksController :: PostQuickLinks(" + _microserviceBaseAddress + _ppMicroserviceSaveQLinks +") -> Input : " + qlinks, r);
              }
            }
            catch (Exception e)
            {
              Log.Error("EXCEPTION :: PhysicianPortal.AppCatalogQuickLinksController :: PostQuickLinks(" + _microserviceBaseAddress + _ppMicroserviceSaveQLinks +") -> Input : " + qlinks + " " + e.StackTrace, e.Message);
            }
        }
    }
}