﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using System.Web.Mvc;
using PP93Helix.Feature.PageContent.Helpers;
using PP93Helix.Feature.PageContent.Models;
using Sitecore.Links;
using Sitecore.Web.UI.WebControls;

namespace PP93Helix.Feature.PageContent.Controllers
{
    public class ArticleListingController : Controller
    {
        // GET: ArticleListing
        public ActionResult Index()
        {
            ArticleListing listing = new Models.ArticleListing();

            listing.ArticleTeasers = new List<ArticleTeaser>();
            listing.isValid = false;
            listing.ComponentTitle = RenderingHelper.GetValueFromCurrentRenderingParameters("ComponentTitle");
            int max = 5;
            int.TryParse(RenderingHelper.GetValueFromCurrentRenderingParameters("MaxShown"), out max);

            ID ArticleTemplateID = new Sitecore.Data.ID("28345347-C9B4-460E-B4E7-7F5890A42724");

            Item[] folders = RenderingHelper.GetItemsFromCurrentRenderingParameters("SelectedFolders");
            var defaultUrlOptions = LinkManager.GetDefaultUrlBuilderOptions();
            defaultUrlOptions.EncodeNames = true;
            for (int i = 0; i < folders.Length || listing.ArticleTeasers.Count >= max; i++)
            {
                Item start = Sitecore.Context.Database.GetItem(folders[i].ID);

                foreach (Item j in start.GetChildren())
                {
                    if (j.TemplateID == ArticleTemplateID)
                    {
                        ArticleTeaser articleTeaser = new Models.ArticleTeaser();

                        articleTeaser.Title = j.Fields["Title"].Value;

                        //articleTeaser.Path = j.Paths.Path;
                        articleTeaser.Path = LinkManager.GetItemUrl(j, defaultUrlOptions);

                        articleTeaser.Subheading = new MvcHtmlString(FieldRenderer.Render(j, "Subheading"));//j.Fields["Subheading"].Value;

                        ImageField imageField = j.Fields["Main Image"];
                        if (imageField != null && imageField.MediaItem != null)
                        {
                            MediaItem image = new MediaItem(imageField.MediaItem);
                            articleTeaser.Thumb = Sitecore.StringUtil.EnsurePrefix('/', Sitecore.Resources.Media.MediaManager.GetMediaUrl(image));
                        }
                        else
                        {
                            articleTeaser.Thumb = "";
                        }

                        articleTeaser.Date = j.Fields["__Created"];

                        listing.ArticleTeasers.Add(articleTeaser);

                        if (!listing.isValid && listing.ArticleTeasers.Count >= 1)
                        {
                            listing.isValid = true;
                        }

                        if (listing.ArticleTeasers.Count >= max)
                        {
                            break;
                        }
                    }
                }
            }

            return View(listing);
        }
    }
}