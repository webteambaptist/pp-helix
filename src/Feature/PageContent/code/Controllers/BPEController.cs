﻿using PP93Helix.Feature.PageContent.Helpers;
using PP93Helix.Feature.PageContent.Models;
using Sitecore.Data;
using System;
using System.Collections.Generic;
using Sitecore.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PP93Helix.Feature.PageContent.Controllers
{
    public class BPEController : Controller
    {
        string BPEFolderKey = Settings.GetSetting("BPEFolderKey");
        // GET: BPE
        public ActionResult Index()
        {

            MediaLibrary bpeList = new MediaLibrary();
            bpeList.FileList = new List<MediaFiles>();

            ID ArticleFolderID = new ID(BPEFolderKey);

            bpeList.FileList = MediaLibraryController.GetMediaArticles(ArticleFolderID);

            return PartialView(bpeList);
        }
    }
}