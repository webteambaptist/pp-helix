﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PP93Helix.Feature.PageContent.Models;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using Newtonsoft.Json;
using PP93Helix.Feature.PageContent.Helpers;

namespace PP93Helix.Feature.PageContent.Controllers
{
    public class CalendarController : Controller
    {
        // GET: Calendar
        public ActionResult Index()
        {
            AllEventsList EvList = new AllEventsList();
            EvList.Events = new List<Event>();

            ID eventFolderID = new ID("67BB51AB-1A96-4224-BDE6-3ECF33C22A25");
            Item eventFolder = Sitecore.Context.Database.GetItem(eventFolderID);
    
            foreach(Item ev in eventFolder.GetChildren())
            {
                Event _event = new Event();

                _event.id = ev.ID.ToString().Replace("{", "").Replace("}", "");
                _event.className = _event.id;
                _event.title = ev.Fields["Title"].Value;
                _event.location = ev.Fields["Location"].Value;
                _event.details = ev.Fields["Details"].Value;

                ImageField imageField = ev.Fields["Main Image"];
                if (imageField != null && imageField.MediaItem != null)
                {
                    MediaItem img = new MediaItem(imageField.MediaItem);
                    _event.imgUrl = Sitecore.StringUtil.EnsurePrefix('/', Sitecore.Resources.Media.MediaManager.GetMediaUrl(img));
                }
                else
                {
                    _event.imgUrl = "";
                }

                string isAllDay = ev.Fields["All Day Event"].Value;
                _event.allDay = isAllDay == "1" ? true : false;

                DateField startField = ev.Fields["Start"];
                if (_event.allDay == true)
                {
                    _event.start = startField.DateTime.ToString("yyyyMMdd");

                    _event.end = null;
                    
                } else
                {
                    Sitecore.Diagnostics.Log.Info("Date - " + Sitecore.DateUtil.IsoDateToServerTimeIsoDate(startField.Value), startField);
                    //Sitecore.DateUtil.FormatIsoDate(startField.DateTime.ToString(), "yyyy-MM-dd hh:mm t");
                    _event.start = Sitecore.DateUtil.IsoDateToServerTimeIsoDate(startField.Value);

                    //_event.start = String.Format("{0}", startField.DateTime);
                    DateField endField = ev.Fields["End"];
                    _event.end = Sitecore.DateUtil.IsoDateToServerTimeIsoDate(endField.Value);
                    //_event.end = String.Format("{0}", endField.DateTime);
                }

                EvList.Events.Add(_event);
            }

            return View((object)JsonConvert.SerializeObject(EvList));
        }

        public ActionResult _upcomingEventsList()
        {
            UpcomingEventsList evList = new UpcomingEventsList();
            evList.Events = new List<UpcomingEvent>();
            int numToShow = 5;

            evList.ComponentTitle = RenderingHelper.GetValueFromCurrentRenderingParameters("ComponentTitle");

            int.TryParse(RenderingHelper.GetValueFromCurrentRenderingParameters("NumEventsToShow"), out numToShow);

            Database web = Sitecore.Configuration.Factory.GetDatabase("web");
            string now = DateTime.Now.ToString("yyyyMMddTHHmmssZ");

            //This query will only find content items made with the Event template
            //excluding the Template's Standard values item
            //excluding any Events which the start time is in the past
            string query = "//*[@@templateid = '{693F3916-A543-4FD2-8946-28B9CE58BF53}' and @@name != '__Standard Values' and @Start > '"+now+"']";

            var events = web.SelectItems(query);

            var upcomingEvents = (from ev in events.OrderBy(x => x.Fields["Start"].Value).Take(numToShow)
                                  select new UpcomingEvent
                                  {
                                      ID = ev.ID.ToString().Replace("{","").Replace("}",""),
                                      Title = ev.Fields["Title"].Value,
                                      Location = ev.Fields["Location"].Value,
                                      Details = ev.Fields["Details"].Value,
                                      Start = (DateField)ev.Fields["Start"],
                                      End = (DateField)ev.Fields["End"],
                                      AllDay = ev.Fields["All Day Event"].Value
                                  }).ToList();

            evList.Events = upcomingEvents;

            return PartialView(evList);
        }
    }
}