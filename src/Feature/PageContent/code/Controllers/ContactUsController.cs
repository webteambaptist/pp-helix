﻿using System;
using System.Collections.Generic;
using Sitecore.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using PP93Helix.Feature.PageContent.Helpers;
using PP93Helix.Feature.PageContent.Models;
using Sitecore.Data;
using Sitecore.Links;
using Sitecore.Links.UrlBuilders;
using Sitecore.Mvc.Configuration;

namespace PP93Helix.Feature.PageContent.Controllers
{
    public class ContactUsController : Controller
    {
        // GET: ContactUs
        public ActionResult Index(string error)
        {
            var us = new ContactUs {Error = error};

            return View(us);
        }

        public ActionResult SubmitContact(ContactUs us)
        {
            var o = new DefaultItemUrlBuilderOptions();
            var message = "";
            var recipient = Settings.GetSetting("contactRecipient");
            var from = Settings.GetSetting("contactFrom");

            var body = "A new Contact Us submission has been processed.";
            body += "<br/><br/>";
            body += "===========================================";
            body += "<br/><br/>";
            body += "First Name: " + us.FirstName + "<br/>";
            body += "<br/>";
            body += "Last Name: " + us.LastName + "<br/>";
            body += "<br/>";
            body += "Email: " + us.Email + "<br/>";
            body += "<br/>";
            body += "Subject: " + us.Email + "<br/>";
            body += "<br/>";
            body += "Comments: " + us.Comments + "<br/>";
            body += "<br/>";

            var pathInfo = (LinkManager.GetItemUrl(Sitecore.Context.Database.GetItem(new ID("{CB35FCDA-2719-40AF-935C-2204925BFB27}")), o));
            try
            {


                var m = new MailMessage(from, recipient)
                {
                    Subject = "Physician Portal Contact Us - Subject: " + us.Subject,
                    Body = body
                };

                message = Mail.SendMail(m);
                if (string.IsNullOrEmpty(message)) return Redirect("~/Home/ContactUs/SubmitContact");
            }
            catch (Exception e)
            {
                return RedirectToRoute(MvcSettings.SitecoreRouteName, new { pathInfo = pathInfo.TrimStart(new char[] { '/' }), error=e.Message });
            }

            return RedirectToRoute(MvcSettings.SitecoreRouteName, new { pathInfo = pathInfo.TrimStart(new char[] { '/' }), error=message });
        }

    }
}