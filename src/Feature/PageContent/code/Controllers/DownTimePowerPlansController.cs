﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PP93Helix.Feature.PageContent.Helpers;
using PP93Helix.Feature.PageContent.Models;
using System;
using System.Collections.Generic;
using Sitecore.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace PP93Helix.Feature.PageContent.Controllers
{
    public class DownTimePowerPlansController : Controller
    {
        private readonly string _microserviceBaseAddress = Settings.GetSetting("ppMicroServiceBaseAddress");
        private readonly string _ppMicroserviceDowntimePowerPlansAdults = Settings.GetSetting("ppMicroserviceDowntimePowerPlansAdults");

        private readonly string _ppMicroserviceDowntimePowerPlansPeds = Settings.GetSetting("ppMicroserviceDowntimePowerPlansPeds");

    // GET: DownTimePowerPlans
    public ActionResult GetAdultPowerPlans()
        {
            /**
             * Call API and get the massive list of Facilities
             * */
            HttpWebResponse httpWebResponse = null;
            httpWebResponse = APIHelper.GetDataFromMicroService(_ppMicroserviceDowntimePowerPlansAdults);
            List<DownTimePowerPlans> powerplans = new List<DownTimePowerPlans>();
            var jsonData = "";
            if (httpWebResponse != null)
            {
                Sitecore.Diagnostics.Log.Info("PhysicanPortal.Controllers.DownTimePowerPlansController :: GetAdultPowerPlans -> API Response : " + httpWebResponse, httpWebResponse);
                Sitecore.Diagnostics.Log.Info("PhysicanPortal.Controllers.DownTimePowerPlansController :: GetAdultPowerPlans -> API Response Code: " + httpWebResponse.StatusCode, httpWebResponse);

                using (var reader = new StreamReader(httpWebResponse.GetResponseStream()))
                {
                    try
                    {
                        var objText = reader.ReadToEnd();
                        Sitecore.Diagnostics.Log.Info("PhysicanPortal.Controllers.DownTimePowerPlansController :: GetAdultPowerPlans ->  Value of objText before check : ", objText);
                        if (!string.IsNullOrEmpty(objText))
                        {
                            Sitecore.Diagnostics.Log.Info("PhysicanPortal.Controllers.DownTimePowerPlansController :: GetAdultPowerPlans ->  Value of objText : ", objText + " Stream " + httpWebResponse.GetResponseStream());
                            Sitecore.Diagnostics.Log.Info("PhysicanPortal.Controllers.DownTimePowerPlansController :: GetAdultPowerPlans ->  Value of objText : ", httpWebResponse.StatusDescription);
                            JObject data = JObject.Parse(objText);

                            powerplans = data["PowerPlansAdultsItem"].Select(c => c.ToObject<DownTimePowerPlans>()).ToList();
                            jsonData = JsonConvert.SerializeObject(powerplans);
                        }
                        else
                        {
                            Sitecore.Diagnostics.Log.Error("PhysicanPortal.Controllers.DownTimePowerPlansController :: GetAdultPowerPlans -> objText is null", objText);
                        }
                    }
                    catch (Exception e)
                    {
                        Sitecore.Diagnostics.Log.Error("PhysicanPortal.Controllers.DownTimePowerPlansController :: GetAdultPowerPlans -> Error with JsonObject ", e.Message + " " + e.InnerException.Message);
                    }
                }
                //facility.facilities = facilities;
                var jsonResult = Json(new { data = jsonData }, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = Int32.MaxValue;
                return jsonResult;
            }
            else
            {
                var jsonResult = Json(new { data = "" }, JsonRequestBehavior.AllowGet);
                return jsonResult;
            }
        }
        public ActionResult GetPedsPowerPlans()
        {
            /**
             * Call API and get the massive list of Facilities
             * */
            HttpWebResponse httpWebResponse = null;
            httpWebResponse = APIHelper.GetDataFromMicroService(_ppMicroserviceDowntimePowerPlansPeds);
            List<DownTimePowerPlans> powerplans = new List<DownTimePowerPlans>();
            var jsonData = "";
            if (httpWebResponse != null)
            {
                Sitecore.Diagnostics.Log.Info("PhysicanPortal.Controllers.DownTimePowerPlansController :: GetPedsPowerPlans -> API Response : " + httpWebResponse, httpWebResponse);
                Sitecore.Diagnostics.Log.Info("PhysicanPortal.Controllers.DownTimePowerPlansController :: GetPedsPowerPlans -> API Response Code: " + httpWebResponse.StatusCode, httpWebResponse);

                using (var reader = new StreamReader(httpWebResponse.GetResponseStream()))
                {
                    try
                    {
                        var objText = reader.ReadToEnd();
                        Sitecore.Diagnostics.Log.Info("PhysicanPortal.Controllers.DownTimePowerPlansController :: GetPedsPowerPlans ->  Value of objText before check : ", objText);
                        if (!string.IsNullOrEmpty(objText))
                        {
                            Sitecore.Diagnostics.Log.Info("PhysicanPortal.Controllers.DownTimePowerPlansController :: GetPedsPowerPlans ->  Value of objText : ", objText + " Stream " + httpWebResponse.GetResponseStream());
                            Sitecore.Diagnostics.Log.Info("PhysicanPortal.Controllers.DownTimePowerPlansController :: GetPedsPowerPlans ->  Value of objText : ", httpWebResponse.StatusDescription);
                            JObject data = JObject.Parse(objText);

                            powerplans = data["PowerPlansPedsItem"].Select(c => c.ToObject<DownTimePowerPlans>()).ToList();
                            jsonData = JsonConvert.SerializeObject(powerplans);
                        }
                        else
                        {
                            Sitecore.Diagnostics.Log.Error("PhysicanPortal.Controllers.DownTimePowerPlansController :: GetPedsPowerPlans -> objText is null", objText);
                        }
                    }
                    catch (Exception e)
                    {
                        Sitecore.Diagnostics.Log.Error("PhysicanPortal.Controllers.DownTimePowerPlansController :: GetPedsPowerPlans -> Error with JsonObject ", e.Message + " " + e.InnerException.Message);
                    }
                }
                //facility.facilities = facilities;
                var jsonResult = Json(new { data = jsonData }, JsonRequestBehavior.AllowGet);
                jsonResult.MaxJsonLength = Int32.MaxValue;
                return jsonResult;
            }
            else
            {
                var jsonResult = Json(new { data = "" }, JsonRequestBehavior.AllowGet);
                return jsonResult;
            }
        }
        public ActionResult GetPDF(int ID, string ppAPIGetPDF)
        {
            WebClient client = new WebClient();
            client.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
            client.Headers.Add("ID", ID.ToString());

      byte[] result = client.UploadData(_microserviceBaseAddress + ppAPIGetPDF,
        "POST",
        System.Text.Encoding.UTF8.GetBytes("ID="));

      string fileName = System.DateTime.Now.ToString();
            if (!String.IsNullOrEmpty(client.ResponseHeaders["Content-Disposition"]))
            {
                fileName = client.ResponseHeaders["Content-Disposition"].Substring(client.ResponseHeaders["Content-Disposition"].IndexOf("filename=") + 9).Replace("\"", "");
            }
            return File(result, "application/octet-stream", fileName);
        }
    }
}