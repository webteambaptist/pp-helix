﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PP93Helix.Feature.PageContent.Helpers;
using PP93Helix.Feature.PageContent.Models;
using System;
using System.Collections.Generic;
using Sitecore.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace PP93Helix.Feature.PageContent.Controllers
{

  public class EROnCallController : Controller
  {
    private readonly string _ppApiGetSpecialtiesByFacility = Settings.GetSetting("ppAPIGetSpecialtiesByFacility");
    private readonly string _ppApiGetPdf = Settings.GetSetting("ppAPIGetPDF");
    private readonly string _ppApiBaseAddress = Settings.GetSetting("ppAPIBaseAddress");

    private readonly string _microserviceBaseAddress = Settings.GetSetting("ppMicroServiceBaseAddress");
    private readonly string _ppMicroserviceEROnCallScheduleList = Settings.GetSetting("ppMicroserviceEROnCallScheduleList");
    private readonly string _ppMicroserviceEROnCallSchedulesItemFilesPDFByID = Settings.GetSetting("ppMicroserviceEROnCallSchedulesItemFilesPDFByID");


    public ActionResult GetSpecialties()
    {
      /**
       * Call API and get the massive list of Facilities
       * */
      HttpWebResponse httpWebResponse = null;
      httpWebResponse = APIHelper.GetDataFromMicroService(_ppMicroserviceEROnCallScheduleList);
      var jsonData = "";
      if (httpWebResponse != null)
      {
        Sitecore.Diagnostics.Log.Info("PhysicanPortal.Controllers.EROnCallController :: GetSpecialties -> API Response : " + httpWebResponse, httpWebResponse);
        Sitecore.Diagnostics.Log.Info("PhysicanPortal.Controllers.EROnCallController :: GetSpecialties -> API Response Code: " + httpWebResponse.StatusCode, httpWebResponse);

        using (var reader = new StreamReader(httpWebResponse.GetResponseStream()))
        {
          try
          {
            var objText = reader.ReadToEnd();
            Sitecore.Diagnostics.Log.Info("PhysicanPortal.Controllers.EROnCallController :: GetSpecialties ->  Value of objText before check : ", objText);
            if (!string.IsNullOrEmpty(objText))
            {
              Sitecore.Diagnostics.Log.Info("PhysicanPortal.Controllers.EROnCallController :: GetSpecialties ->  Value of objText : ", objText + " Stream " + httpWebResponse.GetResponseStream());
              Sitecore.Diagnostics.Log.Info("PhysicanPortal.Controllers.EROnCallController :: GetSpecialties ->  Value of objText : ", httpWebResponse.StatusDescription);
              var data = JObject.Parse(objText);

              var facilities = data["Facilities"].Select(c => c.ToObject<Facility>()).ToList();
              jsonData = JsonConvert.SerializeObject(facilities);
            }
            else
            {
              Sitecore.Diagnostics.Log.Error("PhysicanPortal.Controllers.EROnCallController :: GetSpecialties -> objText is null", objText);
            }
          }
          catch (Exception e)
          {
            Sitecore.Diagnostics.Log.Error("PhysicanPortal.Controllers.EROnCallController :: GetSpecialties -> Error with JsonObject ", e.Message + " " + e.InnerException.Message);
          }
        }
        //facility.facilities = facilities;
        var jsonResult = Json(new { data = jsonData }, JsonRequestBehavior.AllowGet);
        jsonResult.MaxJsonLength = int.MaxValue;
        return jsonResult;
      }
      else
      {
        var jsonResult = Json(new { data = "" }, JsonRequestBehavior.AllowGet);
        return jsonResult;
      }
    }
    public ActionResult GetPdf(int id)
    {
      var client = new WebClient();
      client.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
      client.Headers.Add("ID", id.ToString());

      //var result = client.UploadData(_ppApiBaseAddress + _ppApiGetPdf,
      //                                    "POST", 
      //                                     System.Text.Encoding.UTF8.GetBytes("ID="));

      var result = client.UploadData(_microserviceBaseAddress + _ppMicroserviceEROnCallSchedulesItemFilesPDFByID,
        "POST",
        System.Text.Encoding.UTF8.GetBytes("ID="));

      var fileName = System.DateTime.Now.ToString();
      if (!string.IsNullOrEmpty(client.ResponseHeaders["Content-Disposition"]))
      {
        fileName = client.ResponseHeaders["Content-Disposition"].Substring(client.ResponseHeaders["Content-Disposition"].IndexOf("filename=", StringComparison.Ordinal) + 9).Replace("\"", "");
      }
      return File(result, "application/octet-stream", fileName);
    }

  }
}