using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Sitecore.Data.Items;
using BHUser.Models;
using BHSearch.Models;
using System.IO;
using BHSearch.Helpers;
using Sitecore.Configuration;
using System.Globalization;
using System.Web.Script.Serialization;
using System.Net;
using PP93Helix.Feature.PageContent.Helpers;
using Newtonsoft.Json;

using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mime;
using System.Security.Claims;
using Newtonsoft.Json.Linq;
using Sitecore.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using PP93Helix.Feature.PageContent.Models;
using RestSharp;
using Sitecore.ContentSearch.Data;
using Sitecore.Links;
using Sitecore.Pipelines;

namespace PP93Helix.Feature.PageContent.Controllers
{
    public class FAPController : Controller
    {
        private static readonly string PpMicroserviceBaseAddress = Settings.GetSetting("ppMicroServiceBaseAddress");
        private static readonly string Bingapi = Settings.GetSetting("bingapi");
        private static string FAPFolderKey = Settings.GetSetting("FAPFolderKey");
    // GET: FAP
    public ActionResult FapResults()
        {
            refreshCustomPipelines();
            return View();
        }

        public ActionResult FapProfilePP()
        {
            var physician = new Physician();
            var profile = Sitecore.Context.Item;
            var defaultUrlOptions = LinkManager.GetDefaultUrlBuilderOptions();
            defaultUrlOptions.EncodeNames = true;
            
            Models.MediaLibrary fapList = new Models.MediaLibrary();
            fapList.FileList = new List<Models.MediaFiles>();

            Sitecore.Data.ID ArticleFolderID = new Sitecore.Data.ID(FAPFolderKey);

            fapList.FileList = MediaLibraryController.GetMediaArticles(ArticleFolderID);
            try
            {
                #region Get Sitecore Profile
                //Person Basic Fields
                physician.EchoID = profile.Name;

                physician.FirstName = profile.Fields["FirstName"].Value;
                physician.MiddleName = profile.Fields["MiddleName"].Value;
                physician.LastName = profile.Fields["LastName"].Value;
                physician.Gender = profile.Fields["Gender"].Value;
                physician.JobTitle = profile.Fields["JobTitle"].Value;
                physician.Email = profile.Fields["Email"].Value;
                physician.CellPhoneNumber = profile.Fields["CellPhoneNumber"].Value;
                physician.PagerNumber = profile.Fields["PagerNumber"].Value;
                physician.Languages = !string.IsNullOrEmpty(profile.Fields["Languages"].Value) ?
                profile.Fields["Languages"].Value.Split('|') : null;

                if (!string.IsNullOrEmpty(profile.Fields["PhotoURL"].Value))
                {
                  var providerPhoto = profile.Fields["PhotoURL"].Value;
                  physician.PhotoURL = providerPhoto;
                  
                }
                // else get it from the file folder
                else
                {
                  bool found = false;
                  foreach (var t in fapList.FileList)
                  {
                    if (t.Title != physician.EchoID) continue;
                    physician.PhotoURL = t.Path;
                    found = true;
                    break;
                  }
                  if (!found)
                  {
                    physician.PhotoURL = "../../Images/placeholder.png";
                  }
                }
                
                //Person Location Fields
                physician.PrimaryLocation = !string.IsNullOrEmpty(profile.Fields["PrimaryLocation"].Value) ?
                  profile.Fields["PrimaryLocation"].Value.Split('|') : null;
                physician.PrimaryLocationLatLng = profile.Fields["PrimaryLocationLatLng"].Value;

                physician.OtherLocations = new List<BHUser.Models.Address>();
                if (!string.IsNullOrEmpty(profile.Fields["OtherLocations"].Value))
                {
                    foreach (var otherLocation in profile.Fields["OtherLocations"].Value.Split('^'))
                    {
                        var location = otherLocation.Split('|');

                        var loc = new BHUser.Models.Address
                        {
                            Address1 = (!string.IsNullOrEmpty(location[0].ToString())) ? location[0].ToString() : "",
                            Address2 = (!string.IsNullOrEmpty(location[1].ToString())) ? location[1].ToString() : "",
                            Address3 = (!string.IsNullOrEmpty(location[2].ToString())) ? location[2].ToString() : "",
                            City = (!string.IsNullOrEmpty(location[3].ToString())) ? location[3].ToString() : "",
                            State = (!string.IsNullOrEmpty(location[4].ToString())) ? location[4].ToString() : "",
                            Zip = (!string.IsNullOrEmpty(location[5].ToString())) ? location[5].ToString() : "",
                            Phone1 = (!string.IsNullOrEmpty(location[6].ToString())) ? location[6].ToString() : "",
                            Phone2 = (!string.IsNullOrEmpty(location[7].ToString())) ? location[7].ToString() : "",
                            Fax1 = (!string.IsNullOrEmpty(location[8].ToString())) ? location[8].ToString() : ""
                        };

                        physician.OtherLocations.Add(loc);
                    }
                }

                physician.OtherLocationsLatLngs = !string.IsNullOrEmpty(profile.Fields["OtherLocationsLatLngs"].Value) ?
                profile.Fields["OtherLocationsLatLngs"].Value.Split('|') : null;

                //Physician Template Fields
                physician.PracticeName = profile.Fields["PracticeInformation"].Value;
                physician.PrimarySpecialty = profile.Fields["PrimarySpecialty"].Value;
                physician.OtherSpecialties = !string.IsNullOrEmpty(profile.Fields["OtherSpecialties"].Value) ?
                    profile.Fields["OtherSpecialties"].Value.Split('|') : null;

                physician.Suffix = profile.Fields["Suffix"].Value;

                physician.Education = !string.IsNullOrEmpty(profile.Fields["Education"].Value) ?
                    profile.Fields["Education"].Value.Split('|') : null;

                physician.Certification = !string.IsNullOrEmpty(profile.Fields["Certification"].Value) ?
                    profile.Fields["Certification"].Value.Split('|') : null;

                physician.Residency = !string.IsNullOrEmpty(profile.Fields["Residency"].Value) ?
                    profile.Fields["Residency"].Value.Split('|') : null;

                physician.Internship = !string.IsNullOrEmpty(profile.Fields["Internship"].Value) ?
                    profile.Fields["Internship"].Value.Split('|') : null;

                physician.Biography = !string.IsNullOrEmpty(profile.Fields["Biography"].Value) ?
                    profile.Fields["Biography"].Value : profile.Fields["BackupBiography"].Value;

                physician.AcceptedInsurance = !string.IsNullOrEmpty(profile.Fields["AcceptedInsurance"].Value) ?
                    profile.Fields["AcceptedInsurance"].Value.Split('|') : null;

                physician.AgesTreated = !string.IsNullOrEmpty(profile.Fields["AgesTreated"].Value) ?
                    profile.Fields["AgesTreated"].Value.Split('|') : null;

                physician.Publications = !string.IsNullOrEmpty(profile.Fields["Publications"].Value) ?
                    profile.Fields["Publications"].Value.Split('|') : null;
                #endregion
            }
            catch (Exception ex)
            {
                Log.Error("PP.Controllers.FAPController :: FapResults -> Exception : " + ex.ToString(), ex.Message);
                //throw;
            }
            return View(physician);
        }

        public Physician GetPhysicianProfile()
        {
            var physician = new Physician();
            return physician;
        }

        public void refreshCustomPipelines()
        {
            try
            {
                // refresh pipelines
                var args = new PipelineArgs();
                CorePipeline.Run("customPipelines", args);
            }
            catch (Exception e)
            {
                Sitecore.Diagnostics.Log.Error("PhysicianPortal.Controllers.refreshCustomPipelines ", e.InnerException.Message);
            }
        }
        [HttpPost]
        public JsonResult PhysicianQuery(SitecoreQuery obj)
        {
            try
            {
                // Map Results to Physician Model
                var properties = typeof(PhysicianSearch).GetProperties();
                var master = (List<Item>)System.Web.HttpContext.Current.Cache["sitecoredata"];
                if (master==null)
                {
                    refreshCustomPipelines();
                    master = (List<Item>)System.Web.HttpContext.Current.Cache["sitecoredata"];
                }
                var response = Search(master, obj);
                // Map Results to Physician Model
                var physicians = MapPhysician(response.Results);
                
                // Map Views to Response
                var json = new SitecoreViewResponse
                {
                    TotalHits = response.TotalHits, OriginalQuery = response.OriginalQuery
                };

                // etc
                if (!string.IsNullOrEmpty(obj.Distance) && !string.IsNullOrEmpty(obj.SearchZip))
                {
                    physicians = physicians.OrderBy(x => x.Distance).ToList();
                }
                else if (obj.Sort!=null&&obj.Sort.Equals("desc"))
                {
                    physicians = physicians.OrderByDescending(x => x.LastName).ThenBy(x=>x.FirstName).ToList();
                }
                else
                {
                    physicians = physicians.OrderBy(x => x.LastName).ThenBy(x => x.FirstName).ToList();
                }
                // results
                json.Results = ViewHelper.RenderViewToString(this.ControllerContext, "_SearchResults", new PhysicianResultsView()
                {
                    Start = obj.Start,
                    Physicians = physicians
                });

                // facets
                RenderFacetView(response, "specialties_pipe", json, "_FacetDropdown");
                RenderFacetView(response, "languages_pipe", json, "_FacetDropdown");
                RenderFacetView(response, "hospitalaffiliations_pipe", json, "_FacetDropdown");
                RenderFacetView(response, "gender_s", json, "_FacetGender");

                return new JsonResult()
                {
                    Data = json,
                    MaxJsonLength = int.MaxValue
                };
            }
            catch (Exception ex)
            {
                Log.Error("FAPController -> PhysicianQuery : " + ex.ToString(), this);
                return BuildJsonErrorResponse(ex);
            }
        }

        [HttpPost]
        public JsonResult PhysicianTypeahead(SitecoreQuery obj)
        {
            try
            {
                var master = (List<Item>)System.Web.HttpContext.Current.Cache["sitecoredata"];
                if (master==null)
                {
                    refreshCustomPipelines();
                    master = (List<Item>)System.Web.HttpContext.Current.Cache["sitecoredata"];
                }
                // Execute Search (Solr code)
                var response = Search(master, obj);
                
                // Map Results to Physician Model
                var physicians = MapPhysician(response.Results);
                
                return Json(physicians);
            }
            catch (Exception e)
            {
                return BuildJsonErrorResponse(e);
            }
        }
        public static List<PhysicianSearch> MapPhysician(List<Dictionary<string,object>> results)
        {
            var properties = typeof(PhysicianSearch).GetProperties();
            var physicians = new List<PhysicianSearch>();
            try
            {

                foreach (var doc in results)
                {
                    var physician = new PhysicianSearch();

                    foreach (var prop in properties)
                    {
                        object value = "";
                        if (doc.TryGetValue(prop.Name, out value))
                        {
                            if ((prop.PropertyType == typeof(string[])))
                            {
                                var strMultiData = value.ToString().Split('|');
                                prop.SetValue(physician, strMultiData);
                            }
                            else if (prop.PropertyType == typeof(List<BHUser.Models.Address>))
                            {
                                var locations = (from otherLocations in value.ToString().Split('^')
                                    select otherLocations.Split('|')
                                    into otherLocation
                                    where otherLocation != null & otherLocation.Length > 0
                                    select new BHUser.Models.Address
                                    {
                                        Address1 = (!string.IsNullOrEmpty(otherLocation[0].ToString())) ? otherLocation[0].ToString() : "",
                                        Address2 = (!string.IsNullOrEmpty(otherLocation[1].ToString())) ? otherLocation[1].ToString() : "",
                                        Address3 = (!string.IsNullOrEmpty(otherLocation[2].ToString())) ? otherLocation[2].ToString() : "",
                                        City = (!string.IsNullOrEmpty(otherLocation[3].ToString())) ? otherLocation[3].ToString() : "",
                                        State = (!string.IsNullOrEmpty(otherLocation[4].ToString())) ? otherLocation[4].ToString() : "",
                                        Zip = (!string.IsNullOrEmpty(otherLocation[5].ToString())) ? otherLocation[5].ToString() : "",
                                        Phone1 = (!string.IsNullOrEmpty(otherLocation[6].ToString())) ? otherLocation[6].ToString() : "",
                                        Phone2 = (!string.IsNullOrEmpty(otherLocation[7].ToString())) ? otherLocation[7].ToString() : "",
                                        Fax1 = (!string.IsNullOrEmpty(otherLocation[8].ToString())) ? otherLocation[8].ToString() : ""
                                    }).ToList();

                                prop.SetValue(physician, locations);
                            }
                            else if (prop.PropertyType == typeof(Single))
                            {
                                prop.SetValue(physician, Convert.ToSingle(value));
                            }
                            else
                                prop.SetValue(physician, value);
                        }
                        else
                        {
                            prop.SetValue(physician, null);
                        }
                    }

                    physicians.Add(physician);

                }
                return physicians;
            
            }
            catch (Exception e)
            {
                Log.Error("Exception occurred in MapPhysicians ", e.Message);
                throw;
            }
        }
        
        [HttpPost]
        public JsonResult PhysicianFacets(SitecoreQuery obj)
        {
            try
            {
                // Execute Search
                var master = (List<Item>)System.Web.HttpContext.Current.Cache["sitecoredata"];
                if (master==null)
                {
                    refreshCustomPipelines();
                    master = (List<Item>)System.Web.HttpContext.Current.Cache["sitecoredata"];
                }
                var response = Search(master, obj);

                return Json(RenderFacetList(response, obj));
            }
            catch (Exception e)
            {
                return BuildJsonErrorResponse(e);
            }
        }
        private void RenderFacetView(SitecoreResponse response, string facetField, SitecoreViewResponse json, string partial)
        {
            var facetView = new FacetViews();
            var facets = new List<string>();
            if (!response.Facets.TryGetValue(facetField, out facets)) return;
            var filter = "";
            if (response.OriginalQuery.Filters.TryGetValue(facetField, out filter))
            {
                facetView.Filter = filter.Split(',').ToList();
            }

            facetView.Facet = new KeyValuePair<string, List<string>>(facetField, facets);
            json.Facets.Add(facetField, ViewHelper.RenderViewToString(this.ControllerContext, partial, facetView));
        }
        
        private static Dictionary<string, List<string>> RenderFacetList(SitecoreResponse response, SitecoreQuery obj)
        {
            var facetList = new Dictionary<string, List<string>>();
            foreach (var facetField in obj.Facets)
            {
                var facets = new List<string>();
                if (response.Facets.TryGetValue(facetField, out facets))
                {
                    facetList.Add(facetField, facets);
                }
            }

            return facetList;
        }

        private JsonResult BuildJsonErrorResponse(Exception e)
        {
            var error = new Dictionary<string, object>
            {
                {"Status", "Error"},
                {
                    "Exception",
                    new
                    {
                        e.Message,
                        Trace = e.StackTrace,
                        InnerException = new
                        {
                            Message = e.InnerException?.Message ?? "",
                            Trace = e.InnerException?.StackTrace ?? ""
                        }
                    }
                }
            };
            // Comment Below to Hide Debugging

            return Json(error);
        }

        public static SitecoreResponse Search(List<Item> master, SitecoreQuery obj)
        {
            var response = new SitecoreResponse();
            var temp = new List<Item>();
            
            if (!obj.query.Equals("*"))
            {
                foreach (var item in master)
                {
                    // Exact match from type ahead
                    
                    var fullMatch = new StringBuilder(item.Fields["FirstName"].ToString());
                    fullMatch = fullMatch.Append(" ");
                    fullMatch = fullMatch.Append(item.Fields["LastName"].ToString());
                    fullMatch = fullMatch.Append(", ");
                    fullMatch = fullMatch.Append(item.Fields["JobTitle"].ToString());

                    var firstAndLast = new StringBuilder(item.Fields["FirstName"].ToString());
                    firstAndLast = firstAndLast.Append(" ");
                    firstAndLast = firstAndLast.Append(item.Fields["LastName"].ToString());

                    // full match
                    if (fullMatch.ToString().Equals(obj.query.ToString(),StringComparison.InvariantCultureIgnoreCase))
                    {
                        temp.Add(item);
                    }
                    // last name match
                    else if (item.Fields["LastName"].Value.ToString().Equals(obj.query, StringComparison.InvariantCultureIgnoreCase))
                    {
                        temp.Add(item);
                    }
                    else if (item.Fields["FirstName"].Value.ToString().Equals(obj.query, StringComparison.InvariantCultureIgnoreCase))
                    {
                        temp.Add(item);
                    }
                    else if (firstAndLast.ToString().Equals(obj.query.ToString(), StringComparison.InvariantCultureIgnoreCase))
                    {
                        temp.Add(item);

                    }

                    else if (item.Fields["PrimarySpecialty"].Value.ToString().Equals(obj.query, StringComparison.InvariantCultureIgnoreCase) ||
                         item.Fields["OtherSpecialties"].Value.ToString().ToUpper().Split('|').Contains(obj.query.ToUpper()))
                    // item.Fields["OtherSpecialties"].ToString().Equals(obj.query))
                    {
                        temp.Add(item);
                    }
                    // do a last name like
                    else
                    {
                        if (item.Fields["LastName"].Value.ToUpper().ToString().Contains(obj.query.ToUpper())||
                            item.Fields["FirstName"].Value.ToUpper().ToString().Contains(obj.query.ToUpper()))
                        {
                            temp.Add(item);
                        }
                    }
                    if (obj.Filters.Count > 0)
                    {
                        //filter results
                        temp = Filter(temp, obj);
                    }
                };

                // take top 10
                try
                {
                    response.TotalHits = temp.Count;
                    response.OriginalQuery = obj;
                    if (!string.IsNullOrEmpty(obj.Distance) && !string.IsNullOrEmpty(obj.SearchZip))
                    {
                        // loop through and get distance from zip entered
                        response = GetClosestPhysicians(temp, obj, response);
                    }
                    else
                    {
                        response.Results = GetPhysicians(temp, obj);
                    }
                }
                
                catch (Exception e)
                {
                    Log.Error("Search :: without filter :: PhysicianPortal.Controllers.FAPController.Search " + e.Message, e.Message);
                }
            }
            else
            {
                try
                {
                    if (obj.Filters.Count > 0)
                    {                     
                        //filter results before sorting and picking
                        master = Filter(master, obj);
                    }
                    response.OriginalQuery = obj;
                    response.Results = new List<Dictionary<string, object>>();
                    response.TotalHits = master.Count;
                    if (!string.IsNullOrEmpty(obj.Distance) && !string.IsNullOrEmpty(obj.SearchZip))
                    {
                        response = GetClosestPhysicians(master, obj, response);
                    }

                    else
                    {
                        response.Results = GetPhysicians(master, obj);
                    }
                }
                catch (Exception e)
                {
                    Log.Error("Search :: with filter :: PhysicianPortal.Controllers.FAPController.Search " + e.Message, e.Message);
                }
            }
            
            var specList = new List<string>();
            var langList = new List<string>();
            var genderList = new List<string>();
            var hospitalList = new List<string>();

            foreach (var item in master)
            {
                var specialty = item.Fields["PrimarySpecialty"].ToString();
                if (!string.IsNullOrEmpty(specialty) && !specList.Contains(specialty))
                {
                    specList.Add(specialty);
                }
                var otherSpecialties = item.Fields["OtherSpecialties"].ToString();
                if (!string.IsNullOrEmpty(otherSpecialties))
                {
                    var others = otherSpecialties.Split('|');
                    foreach (var o in others)
                    {
                        if (!specList.Contains(o))
                        {
                            specList.Add(o);
                        }
                    }
                }
                var languages = item.Fields["Languages"].ToString();
                if (!string.IsNullOrEmpty(languages))
                {
                    var langs = languages.Split('|');
                    foreach (var l in langs)
                    {
                        if (!langList.Contains(l))
                        {
                            langList.Add(l);
                        }
                    }
                }
                var gender = item.Fields["Gender"].ToString();
                if (!string.IsNullOrEmpty(gender) && !genderList.Contains(gender))
                {
                    genderList.Add(gender);
                }
                var hospital = item.Fields["HospitalAffiliations"].ToString();
                if (string.IsNullOrEmpty(hospital)) continue;
                var hospitals = hospital.Split('|');
                foreach (var h in hospitals)
                {
                    if (!hospitalList.Contains(h))
                    {
                        hospitalList.Add(h);
                    }
                }
            }
            specList.Sort();
            langList.Sort();
            genderList.Sort();
            hospitalList.Sort();
            response.Facets = new Dictionary<string, List<string>>
            {
                {"specialties_pipe", specList},
                {"gender_s", genderList},
                {"languages_pipe", langList},
                {"hospitalaffiliations_pipe", hospitalList}
            };

            return response;
        }
        protected static double ZipCodeDistance(ZipCode zip1, ZipCode zip2)
        {
            var zCode1 = zip1;
            var zCode2 = zip2;//GetLatLong(zip2); 

            if (zCode1 == null || zCode2 == null)
            {
                throw new ArgumentException("Code does not exist");
            }

            const double earthsRadius = 3956.08710710349;

            var latRadians = (zCode1.lat / 180) * Math.PI;
            var longRadians = (zCode1.lon / 180) * Math.PI;

            var latRadians2 = (zCode2.lat / 180) * Math.PI;
            var longRadians2 = (zCode2.lon / 180) * Math.PI;

            var distance = (earthsRadius * 2) * Math.Asin(
                Math.Sqrt(
                    Math.Pow(
                        Math.Sin((latRadians - latRadians2) / 2), 2) +
                    Math.Cos(latRadians) * Math.Cos(latRadians2) *
                    Math.Pow(
                        Math.Sin((longRadians - longRadians2) / 2), 2)));

            return distance;
        }
        private static ZipCode GetLatLong(Models.Address address)
        {
          var zip = new ZipCode();
          var jsonString = "";
          try
          {
            if (address?.Zip != null)
            {
              jsonString = JsonConvert.SerializeObject(address);
              var client = new RestClient(Bingapi);
              var request = new RestRequest(Method.GET);
              request.AddHeader("Address", jsonString);
              var response = client.Execute(request);
              if (!response.IsSuccessful) return null;

              try
              {
                var coords = JsonConvert.DeserializeObject<BingMapsRESTToolkit.Coordinate>(response.Content);
                zip.lat = coords.Latitude;
                zip.lon = coords.Longitude;
              }
              catch (Exception e)
              {
                Log.Error("There was an error getting Lat/Long from Microservice " + jsonString, e.Message);
              }
            }
          }
          catch (Exception ex)
          {
                Log.Error("GetLatLong -> Could not obtain coordinates for zip" + jsonString, ex.Message);
          }
          return zip;
        }
 
        public static SitecoreResponse GetClosestPhysicians(List<Item> temp, SitecoreQuery obj,
            SitecoreResponse response)
        {
          try
          {
            var distanceItems = new List<PhysicianObj>();
            var list = new List<Dictionary<string, object>>();
            var defaultUrlOptions = LinkManager.GetDefaultUrlBuilderOptions();
            defaultUrlOptions.EncodeNames = true;
            var address = new Models.Address {StreetAddress = "", City = "", State = "", Zip = obj.SearchZip};
            var searchZipcode = GetLatLong(address);

            foreach (var r in temp.Where(x => x.Fields["PrimaryLocationLatLng"].Value != null &&
                                              x.Fields["PrimaryLocation"].Value != null))
            {
                try
                {
                    var physician = new PhysicianObj();
                    var zip = new ZipCode
                    {
                        lat = float.Parse(r.Fields["PrimaryLocationLatLng"].ToString().Split(',')[0]),
                        lon = float.Parse(r.Fields["PrimaryLocationLatLng"].ToString().Split(',')[1])
                    };
                    var dist = ZipCodeDistance(zip, searchZipcode);

                    if (!(dist <= double.Parse(obj.Distance))) continue;
                    physician.AcceptedInsurance = r.Fields["AcceptedInsurance"].Value;
                    physician.AgesTreated = r.Fields["AgesTreated"].Value;
                    physician.BackupBiography = r.Fields["BackupBiography"].Value;
                    physician.Biography = r.Fields["Biography"].Value;
                    physician.CellPhoneNumber = r.Fields["CellPhoneNumber"].Value;
                    physician.Certification = r.Fields["Certification"].Value;
                    physician.Distance = Math.Round(dist, 2).ToString(CultureInfo.InvariantCulture);
                    physician.Education = r.Fields["Education"].Value;
                    physician.ExcludeFromSearch = r.Fields["ExcludeFromSearch"].Value;
                    physician.FirstName = r.Fields["FirstName"].Value;
                    physician.Gender = r.Fields["Gender"].Value;
                    physician.HospitalAffiliation = r.Fields["HospitalAffiliations"].Value;
                    physician.IsAcceptingPatients = r.Fields["IsAcceptingPatients"].Value;
                    physician.IsBPP = r.Fields["IsBPP"].Value;
                    physician.JobTitle = r.Fields["JobTitle"].Value;
                    physician.Languages = r.Fields["Languages"].Value;
                    physician.LastName = r.Fields["LastName"].Value;
                    physician.MiddleName = r.Fields["MiddleName"].Value;
                    
                    if (!string.IsNullOrEmpty(r.Fields["OtherLocations"].Value))
                    {
                        physician.OtherLocations = r.Fields["OtherLocations"].Value;
                    }
                    if (!string.IsNullOrEmpty(r.Fields["OtherLocationsLatLngs"].Value))
                    {
                        physician.OtherLocationsLatLngs = r.Fields["OtherLocationsLatLngs"].Value;
                    }
                    physician.OtherSpecialties = r.Fields["OtherSpecialties"].Value;
                    physician.PagerNumber = r.Fields["PagerNumber"].Value;
                    physician.PracticeInformation = r.Fields["PracticeInformation"].Value;
                    if (!string.IsNullOrEmpty(r.Fields["PrimaryLocation"].Value))
                    {
                        physician.PrimaryLocation = r.Fields["PrimaryLocation"].Value;
                    }
                    if (!string.IsNullOrEmpty(r.Fields["PrimaryLocationLatLng"].Value))
                    {
                        physician.PrimaryLocationLatLng = r.Fields["PrimarylocationlatLng"].Value;
                    }
                    physician.PrimarySpecialty = r.Fields["PrimarySpecialty"].Value;
                    physician.Residency = r.Fields["Residency"].Value;
                    physician.Suffix = r.Fields["Suffix"].Value;
                    var path = LinkManager.GetItemUrl(r, defaultUrlOptions);
                    physician.Path = path;
                    physician.PhotoURL = r.Fields["PhotoURL"].Value;
                    //physician.Path = r.Paths.FullPath;
                    physician.ContentID = r.Name;
                    distanceItems.Add(physician);
                }
                catch (Exception e)
                {
                    Log.Error("Exception occurred in GetClosestPhysicians ", e.Message);
                }
            }
            response.TotalHits = distanceItems.Count;
            // sort by distance
            var children = distanceItems.OrderBy(e => Convert.ToDouble(e.Distance))
                .Skip(obj.Start)
                .Take(10)
                .ToList();

            foreach (var r in children)
            {
                var pair = new Dictionary<string, object>
                {
                    {"FirstName", r.FirstName},
                    {"MiddleName", r.MiddleName},
                    {"LastName", r.LastName},
                    {"Suffix", r.Suffix},
                    {"Gender", r.Gender},
                    {"JobTitle", r.JobTitle},
                    {"Languages", r.Languages},
                    {"PrimarySpecialty", r.PrimarySpecialty},
                    {"OtherSpecialties", r.OtherSpecialties},
                    {"Education", r.Education},
                    {"Certification", r.Certification},
                    {"Residency", r.Residency},
                    {"Biography", r.Biography},
                    {"BackupBiography", r.BackupBiography},
                    {"PracticeInformation", r.PracticeInformation},
                    {"HospitalAffiliations", r.HospitalAffiliation},
                    {"AcceptedInsurance", r.AcceptedInsurance},
                    {"IsAcceptingPatients", r.IsAcceptingPatients},
                    {"AgesTreated", r.AgesTreated},
                    {"IsBPP", r.IsBPP},
                    {"ExcludeFromSearch", r.ExcludeFromSearch},
                    {"CellPhoneNumber", r.CellPhoneNumber},
                    {"PagerNumber", r.PagerNumber}
                };
                if (!string.IsNullOrEmpty(r.PrimaryLocation))
                {
                    pair.Add("PrimaryLocation", r.PrimaryLocation);
                }
                if (!string.IsNullOrEmpty(r.PrimaryLocationLatLng))
                {
                    pair.Add("PrimaryLocationLatLng", r.PrimaryLocationLatLng);
                }
                if (!string.IsNullOrEmpty(r.OtherLocations))
                {
                    pair.Add("OtherLocations", r.OtherLocations);
                }
                if (!string.IsNullOrEmpty(r.OtherLocationsLatLngs))
                {
                    pair.Add("OtherLocationsLatLngs", r.OtherLocationsLatLngs);
                }
                if (!string.IsNullOrEmpty(r.PhotoURL))
                {
                    pair.Add("PhotoURL", r.PhotoURL);
                }
                // pair.Add("Template", r.Template);
                pair.Add("Path", r.Path);
                pair.Add("ContentID", r.ContentID);
                pair.Add("Distance", r.Distance);
                list.Add(pair);

            }
            //list.OrderBy(dict => dict["Distance"]);
            response.Results = list;
            return response;
          }
          catch (Exception e)
          {
            Log.Error("Exception :: GetClosestPhysicians" + e.Message, e.Message);
            return null;
          }
            
        }
        public static List<Dictionary<string, object>> GetPhysicians(List<Item> master, SitecoreQuery obj)
        {
            List<Item> list;
            var returnList = new List<Dictionary<string, object>>();
            var defaultUrlOptions = LinkManager.GetDefaultUrlBuilderOptions();
            defaultUrlOptions.EncodeNames = true;
            if (obj.Sort != null && obj.Sort.Equals("desc"))
            {
                var children = master
                    .OrderByDescending(e => e.Fields["LastName"].Value.ToString())
                    .ThenByDescending(e => e.Fields["FirstName"].Value.ToString())
                    .Skip(obj.Start)
                    .Take(10)
                    .ToList();
                list = children;
            }
            else
            {
                var children = master
                    .OrderBy(e => e.Fields["LastName"].Value.ToString())
                    .ThenBy(e => e.Fields["FirstName"].Value.ToString())
                    .Skip(obj.Start)
                    .Take(10)
                    .ToList();
                list = children;
            }

            foreach (var r in list)
            {
                var pair = new Dictionary<string, object>
                {
                    {"FirstName", r.Fields["FirstName"].Value},
                    {"MiddleName", r.Fields["MiddleName"].Value},
                    {"LastName", r.Fields["LastName"].Value},
                    {"Suffix", r.Fields["Suffix"].Value},
                    {"Gender", r.Fields["Gender"].Value},
                    {"JobTitle", r.Fields["JobTitle"].Value},
                    {"Languages", r.Fields["Languages"].Value},
                    {"PrimarySpecialty", r.Fields["PrimarySpecialty"].Value},
                    {"OtherSpecialties", r.Fields["OtherSpecialties"].Value},
                    {"Education", r.Fields["Education"].Value},
                    {"Certification", r.Fields["Certification"].Value},
                    {"Residency", r.Fields["Residency"].Value},
                    {"Biography", r.Fields["Biography"].Value},
                    {"BackupBiography", r.Fields["BackupBiography"].Value},
                    {"PracticeInformation", r.Fields["PracticeInformation"].Value},
                    {"HospitalAffiliations", r.Fields["HospitalAffiliations"].Value},
                    {"AcceptedInsurance", r.Fields["AcceptedInsurance"].Value},
                    {"IsAcceptingPatients", r.Fields["IsAcceptingPatients"].Value},
                    {"AgesTreated", r.Fields["AgesTreated"].Value},
                    {"IsBPP", r.Fields["IsBPP"].Value},
                    {"ExcludeFromSearch", r.Fields["ExcludeFromSearch"].Value},
                    {"CellPhoneNumber", r.Fields["CellPhoneNumber"].Value},
                    {"PagerNumber", r.Fields["PagerNumber"].Value}
                };
                if (!string.IsNullOrEmpty(r.Fields["PrimaryLocation"].Value))
                {
                    pair.Add("PrimaryLocation", r.Fields["PrimaryLocation"].Value);
                }
                if (!string.IsNullOrEmpty(r.Fields["PrimaryLocationLatLng"].Value))
                {
                    pair.Add("PrimaryLocationLatLng", r.Fields["PrimaryLocationLatLng"].Value);
                }
                //pair.Add("OtherLocations", r.Fields["OtherLocations"].Value);
                if (!string.IsNullOrEmpty(r.Fields["OtherLocations"].Value))
                {
                    pair.Add("OtherLocations", r.Fields["OtherLocations"].Value);
                }
                if (!string.IsNullOrEmpty(r.Fields["OtherLocationsLatLngs"].Value))
                {
                    pair.Add("OtherLocationsLatLngs", r.Fields["OtherLocationsLatLngs"].Value);
                }
                if (!string.IsNullOrEmpty(r.Fields["PhotoURL"].Value))
                {
                    pair.Add("PhotoURL", r.Fields["PhotoURL"].Value);
                }
                // pair.Add("Template", r.Template);
                var path = LinkManager.GetItemUrl(r, defaultUrlOptions);
                //pair.Add("Path", r.Paths.FullPath);
                pair.Add("Path", path);
                pair.Add("ContentID", r.Name);

                returnList.Add(pair);

            }
            return returnList;
        }
        public static List<Item> Filter(List<Item> list, SitecoreQuery obj)
        {
            var returnList = new List<Item>();
            var numFilters = obj.Filters.Count(filters => !string.IsNullOrEmpty(filters.Value));


            //filter specialties
            foreach (var items in list)
            {
                var numFiltersFound = 0;
                foreach (var filters in obj.Filters)
                {
                    string filter = null;
                    switch (filters.Key)
                    {
                        case "specialties_pipe":
                        {
                            filter = filters.Value;
                            if (!string.IsNullOrEmpty(filter))
                            {
                                if (items.Fields["PrimarySpecialty"].Value.ToString() == filter)
                                {
                                    numFiltersFound++;
                                }
                            }

                            break;
                        }
                        case "gender_s":
                        {
                            filter = filters.Value;
                            if (!string.IsNullOrEmpty(filter))
                            {
                                if (items.Fields["Gender"].Value.ToString() == filter)
                                {
                                    numFiltersFound++;
                                }
                            }

                            break;
                        }
                        case "languages_pipe":
                        {
                            filter = filters.Value;
                            if (!string.IsNullOrEmpty(filter))
                            {
                                var languages = items.Fields["Languages"].Value.ToString().Split('|');
                                if (languages.Contains(filter))
                                { 
                                    numFiltersFound++;
                                }
                            }

                            break;
                        }
                        case "hospitalaffiliations_pipe":
                        {
                            filter = filters.Value;
                            if (!string.IsNullOrEmpty(filter))
                            {
                                var hospitals = items.Fields["HospitalAffiliations"].Value.ToString().Split('|');
                                if (hospitals.Contains(filter))
                                {
                                    numFiltersFound++;
                                }
                            }

                            break;
                        }
                    }
                }
                if (numFiltersFound==numFilters&& !returnList.Contains(items))
                {
                    returnList.Add(items);
                }
            }
            return returnList;
        }
    }

}