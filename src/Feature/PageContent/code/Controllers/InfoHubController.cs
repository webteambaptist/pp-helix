﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using Sitecore.Mvc.Presentation;
using PP93Helix.Feature.PageContent.Helpers;
using PP93Helix.Feature.PageContent.Models;
using Sitecore.Links;
using Sitecore.Web.UI.WebControls;

namespace PP93Helix.Feature.PageContent.Controllers
{
    public class InfoHubController : Controller
    {
        public ActionResult _infoHub()
        {
            InfoHub infoHub = new Models.InfoHub();
            infoHub.ArticleTeasers = new List<ArticleTeaser>();
            infoHub.ComponentTitle = RenderingHelper.GetValueFromCurrentRenderingParameters("ComponentTitle");

            Database db = Sitecore.Configuration.Factory.GetDatabase("web");
            Item[] articleItems = db.SelectItems("//sitecore/content/Home/Info Hub/*");

            List<Item> filteredItems = RenderingHelper.FilterItemsByTagsMatchingUserRoles(articleItems);
            var defaultUrlOptions = LinkManager.GetDefaultUrlBuilderOptions();
            defaultUrlOptions.EncodeNames = true;
            foreach (Item i in filteredItems)
            {
                ArticleTeaser articleTeaser = new Models.ArticleTeaser();

                // add Title, Path, Subheading, Thumb

                articleTeaser.Title = i.Fields["Title"].Value;

                //articleTeaser.Path = i.Paths.Path;
                articleTeaser.Path = LinkManager.GetItemUrl(i, defaultUrlOptions);

                articleTeaser.Subheading = new MvcHtmlString(FieldRenderer.Render(i, "Subheading"));//i.Fields["Subheading"].Value;
                
                ImageField imageField = i.Fields["Main Image"];
                if (imageField != null && imageField.MediaItem != null)
                {
                    MediaItem image = new MediaItem(imageField.MediaItem);
                    articleTeaser.Thumb = Sitecore.StringUtil.EnsurePrefix('/', Sitecore.Resources.Media.MediaManager.GetMediaUrl(image));
                }
                else
                {
                    articleTeaser.Thumb = "";
                }

                articleTeaser.Date = i.Fields["__Created"];

                infoHub.ArticleTeasers.Add(articleTeaser);

            }

            var data = infoHub.ArticleTeasers.OrderByDescending(x => ((DateField)x.Date).DateTime).ToList();
            infoHub.ArticleTeasers = data.ToList();
            return PartialView(infoHub);
        }
    }
}