﻿using Sitecore.Data;
using System.Collections.Generic;
using System.Web.Mvc;
using PP93Helix.Feature.PageContent.Models;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using PP93Helix.Feature.PageContent.Helpers;
using Sitecore.Configuration;
using PP93Helix.Feature.PageContent.Controllers;
using Sitecore.Mvc.Presentation;

namespace PP93Helix.Feature.PageContent.Controllers
{
    public class InfomaticsController : Controller
    {
        private static string InfomaticsFolderID = Settings.GetSetting("InfomaticsFolderID");
        // GET: Informatics
        public ActionResult Index()
        {
            ID DocFolderID = new ID(InfomaticsFolderID);

            Infomatics IList = new Infomatics();
            IList.AllDocsList = new List<Doc>();

            Item eventFolder = Sitecore.Context.Database.GetItem(DocFolderID);

            foreach (Item d in eventFolder.GetChildren())
            {
                if (d.Name != "Informatics")
                {
                    Doc doc = new Doc();
                    string content = d.Fields["Content"].Value.ToString();
                    if (content.Length > 700)
                    {
                        content = content.Substring(0, 700);
                        var lastClosingBracket = content.LastIndexOf(">");
                        var brokenTag = content.IndexOf("<",lastClosingBracket);
                        if (brokenTag >= 0)
                        {
                            content = content.Substring(0,brokenTag);
                        }

                    }
                    if (content.Substring(content.Length-1)!=" ")
                    {
                        content = content + "~";
                    }
                    
                    doc.DocHeading = d.Fields["DocHeading"].Value;
                    doc.NameTitle = d.Fields["NameTitle"].Value;
                    doc.MediaLibraryFolder = d.Fields["MediaLibraryFolderName"].Value;
                    var link = Sitecore.Links.LinkManager.GetItemUrl(d);
                    doc.Content = content + "<a href=\""+link +"?folderName="+doc.MediaLibraryFolder+"\">more... </a>";
                    ImageField imageField = d.Fields["Picture"];
                    if (imageField != null && imageField.MediaItem != null)
                    {
                        MediaItem img = new MediaItem(imageField.MediaItem);
                        doc.Picture = Sitecore.StringUtil.EnsurePrefix('/', Sitecore.Resources.Media.MediaManager.GetMediaUrl(img));
                    }
                    else
                    {
                        doc.Picture = "";
                    }

                    IList.AllDocsList.Add(doc);
                }
            }

            return PartialView(IList);
        }

        public ActionResult Articles(string folderName)
        {
            MediaLibrary fileList = new MediaLibrary();
            fileList.FileList = new List<MediaFiles>();
            // get id of path
            var path = "/sitecore/media library/Infomatics/" + folderName;
            var item = Sitecore.Context.Database.GetItem(path);
            var files = MediaLibraryController.GetMediaArticles(item.ID);
            
            fileList.FileList = files;
            
            return PartialView(fileList);
        }
    }

}