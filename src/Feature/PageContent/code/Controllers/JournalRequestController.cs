﻿using System;
using System.Collections.Generic;
using Sitecore.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using PP93Helix.Feature.PageContent.Helpers;
using PP93Helix.Feature.PageContent.Models;
using Sitecore.Data;
using Sitecore.Diagnostics;
using Sitecore.Links;
using Sitecore.Links.UrlBuilders;
using Sitecore.Mvc.Configuration;
using Sitecore.Shell.Applications.ContentEditor;
using DateTime = System.DateTime;
using Link = PP93Helix.Feature.PageContent.Models.Link;

namespace PP93Helix.Feature.PageContent.Controllers
{
    public class JournalRequestController : Controller
    {
        private readonly string _from = Settings.GetSetting("librarySearchRequestFrom");
        private readonly string _to = Settings.GetSetting("SearchRequestRecipient");

        private readonly string _journalRequest = Settings.GetSetting("ppMicroserviceMedLibJournalRequest");
        // GET: MedicalLibary
        public ActionResult Index()
        {
            var jr = new JournalRequest();
            var lists = Helpers.MediaLibrarySearchRequest.GetOptionValues();
            if (lists == null) return View(jr);

            var professionDd = new List<SelectListItem>();
            var locationDd = new List<SelectListItem>();
            var deliveryTypeOptions = new List<SelectListItem>();

            var defaultValue = new SelectListItem {Text = "", Value = ""};

            professionDd.Add(defaultValue);
            professionDd.AddRange(lists.professionList.Select(profession => new SelectListItem {Text = profession.OptionValue, Value = profession.OptionValue}));
            jr.Professions = professionDd;

            locationDd.Add(defaultValue);
            locationDd.AddRange(lists.locationList.Select(location=> new SelectListItem { Text = location.OptionValue, Value = location.OptionValue}));
            jr.Locations = locationDd;

            deliveryTypeOptions.AddRange(lists.deliveryTypes.Select(type => new SelectListItem {Text = type.OptionValue, Value = type.OptionValue}));
            jr.DeliveryTypes = deliveryTypeOptions;
            jr.CurrentDate = DateTime.Now.Date;
            jr.DateNeededBy = DateTime.Now.Date;
            return View(jr);
        }

        public ActionResult SubmitJournalRequest(JournalRequest jr)
        {
            var deliveryTypes = Request.Form["DeliveryTypes"];

            var o = new DefaultItemUrlBuilderOptions();
            var message = "";
            var body = "A new Article Request has been submitted.";
            body += "<br/><br/>";
            body += "Request Details:";
            body += "<br/><br/>";
            body += "Requester's Name: " + jr.Name;
            body += "<br/><br/>";
            body += "Email: " + jr.Email;
            body += "<br/><br/>";
            body += "Requested On: " + jr.CurrentDate;
            body += "<br/><br/>";
            body += "Date Needed: " + jr.DateNeededBy;
            body += "<br/><br/>";
            body += "Profession: " + jr.SelectedProfession;
            body += "<br/><br/>";
            body += "Department: " + jr.Dept;
            body += "<br/><br/>";
            body += "Location: " + jr.SelectedLocation;
            body += "<br/><br/>";
            body += "Requested Articles";
            body += "<br/><br/>";
            body += jr.JournalArticleRequest;
            body += "<br/><br/>";
            body += "Requested Books";
            body += "<br/><br/>";
            body += jr.BookRequests;
            body += "<br/><br/>";
            body += "Requested Delivery Method: " + deliveryTypes;
            body += "<br/><br/>";
            body += "Phone: " + jr.PhoneNumber;
            body += "<br/><br/>";
            var rush = jr.Rush ? "Yes" : "No";
            body += "Rush Delivery? " + rush;
            body += "<br/><br/>";
 
            var pathInfo = (LinkManager.GetItemUrl(Sitecore.Context.Database.GetItem(new ID("{A240F832-8C1D-4EB3-BB51-192F596A461A}")), o));
            try
            {
                var m = new MailMessage(_from, _to)
                {
                    Subject = "A new Article Request has been submitted",
                    Body = body
                };

                message = Mail.SendMail(m);
                try
                {
                  var journalRequest = new JournalRequestSubmit
                  {
                    Name = jr.Name,
                    Email = jr.Email,
                    CurrentDate = jr.CurrentDate,
                    DateNeededBy = jr.DateNeededBy,
                    Rush = jr.Rush,
                    Profession = jr.SelectedProfession,
                    Department = jr.Dept,
                    Location = jr.SelectedLocation,
                    JournalArticleRequest = jr.JournalArticleRequest,
                    BookRequest = jr.BookRequests,
                    DeliveryType = deliveryTypes,
                    Phone = jr.PhoneNumber
                  };
                  var json = JsonConvert.SerializeObject(journalRequest);
                  var response = APIHelper.PostDataToMicroServiceWithHeaders(_journalRequest, null, "JournalRequest|"+json);
                  if (response.StatusCode != HttpStatusCode.OK)
                  {
                    Log.Error("Bad Request Journal Request Microservice " + response.StatusDescription, this);
                  }
                }
                catch (Exception e)
                {
                  Log.Error("Exception sending data to Library Request Microservice " + e.Message, this);
                } 
                var journalRequestUrl = (LinkManager.GetItemUrl(Sitecore.Context.Database.GetItem(new ID("{829EA169-8E24-4022-B215-2723C8906217}")), o));
                if (string.IsNullOrEmpty(message)) return Redirect(journalRequestUrl);
            }
            catch (Exception e)
            {
                return RedirectToRoute(MvcSettings.SitecoreRouteName, new { pathInfo = pathInfo.TrimStart(new char[] { '/' }), error=e.Message });
            }
            return RedirectToRoute(MvcSettings.SitecoreRouteName, new { pathInfo = pathInfo.TrimStart(new char[] { '/' }), error=message });
        }

    }

}