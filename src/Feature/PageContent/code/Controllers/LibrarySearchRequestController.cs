﻿using System;
using System.Collections.Generic;
using Sitecore.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PP93Helix.Feature.PageContent.Helpers;
using PP93Helix.Feature.PageContent.Models;
using RestSharp.Serialization;
using Sitecore.Data;
using Sitecore.Diagnostics;
using Sitecore.Links;
using Sitecore.Links.UrlBuilders;
using Sitecore.Mvc.Configuration;

namespace PP93Helix.Feature.PageContent.Controllers
{
    public class LibrarySearchRequestController : Controller
    {
        // GET: LibrarySearchRequest
        private readonly string _from = Settings.GetSetting("librarySearchRequestFrom");
        private readonly string _to = Settings.GetSetting("SearchRequestRecipient");
        private readonly string _libraryRequest = Settings.GetSetting("ppMicroserviceMedLibSearchRequest");
        
        public ActionResult Index()
        {
            var medicalLibraryOptions = new List<MedicalLibraryOptions>();
            var ls = new LibrarySearch();

            var lists = Helpers.MediaLibrarySearchRequest.GetOptionValues();
            if (lists == null) return View(ls);
            var professionDd = new List<SelectListItem>();
            var locationDd = new List<SelectListItem>();
            var deliveryTypeOptions = new List<SelectListItem>();
            var qualificationsOptions = new List<SelectListItem>();
            var agesOptions = new List<SelectListItem>();
            var kindOfSearchOptions = new List<SelectListItem>();
            var audiencesOptions = new List<SelectListItem>();

            var defaultValue = new SelectListItem {Text = "", Value = ""};

            professionDd.Add(defaultValue);
            professionDd.AddRange(lists.professionList.Select(profession => new SelectListItem {Text = profession.OptionValue, Value = profession.OptionValue}));
            ls.Professions = professionDd;

            locationDd.Add(defaultValue);
            locationDd.AddRange(lists.locationList.Select(location=> new SelectListItem { Text = location.OptionValue, Value = location.OptionValue}));
            ls.Locations = locationDd;

            deliveryTypeOptions.AddRange(lists.deliveryTypes.Select(type => new SelectListItem {Text = type.OptionValue, Value = type.OptionValue}));
            ls.DeliveryTypes = deliveryTypeOptions;

            qualificationsOptions.AddRange(lists.qualificationList.Select(qualification=>new SelectListItem {Text = qualification.OptionValue, Value = qualification.OptionValue}));
            ls.Qualifications = qualificationsOptions;

            agesOptions.AddRange(lists.age.Select(age=>new SelectListItem {Text = age.OptionValue, Value = age.OptionValue}));
            ls.Ages = agesOptions;

            kindOfSearchOptions.AddRange(lists.kindsList.Select(kind=>new SelectListItem {Text = kind.OptionValue, Value = kind.OptionValue}));
            ls.SearchKinds = kindOfSearchOptions;

            audiencesOptions.AddRange(lists.audiences.Select(audience=>new SelectListItem { Text = audience.OptionValue, Value = audience.OptionValue}));
            ls.Audiences = audiencesOptions;
            ls.CurrentDate = DateTime.Now.Date;
            ls.DateNeededBy = DateTime.Now.Date;
            return View(ls);
        }

        public ActionResult SubmitLibrarySearchRequest(LibrarySearch ls)
        {

            var qualifications = Request.Form["QualificationIds"];
            var ages = Request.Form["Ages"];
            var kinds = Request.Form["Kinds"];
            var audiences = Request.Form["Audiences"];
            var deliverTypes = Request.Form["DeliveryTypes"];
            var o = new DefaultItemUrlBuilderOptions();
            var message = "";

            var body = "A Literature Search Request has been submitted by: " + ls.Name;
            body += "<br/><br/>";
            body += "Details:";
            body += "<br/><br/>";
            body += "Email: " + ls.Email;
            body += "<br/><br/>";
            body += "Date Submitted: " + ls.CurrentDate;
            body += "<br/><br/>";
            body += "Date Needed By: " + ls.DateNeededBy;
            body += "<br/><br/>";
            body += "Profession: " + ls.SelectedProfession;
            body += "<br/><br/>";
            body += "Department: " + ls.Dept;
            body += "<br/><br/>";
            body += "Location: " + ls.SelectedLocation;
            body += "<br/><br/>";
            body += "Search Request: " + ls.SearchRequest;
            body += "<br/><br/>";
            body += "Search Qualifications: " + qualifications;
            body += "<br/><br/>";
            body += "Ages: " + ages;
            body += "<br/><br/>";
            body += "Years Covered: " + ls.YearsToBeCovered;
            body += "<br/><br/>";
            body += "Kind of Search: " + kinds;
            body += "<br/><br/>";
            body += "Purpose/Audience: " + audiences;
            body += "<br/><br/>";
            body += "Delivery Type: " + deliverTypes;
            body += "<br/><br/>";
            body += "Phone: " + ls.PhoneNumber;
            body += "<br/><br/>";
            var rush = ls.Rush ? "Yes" : "No";
            body += "Rush: " + rush;

            var pathInfo = (LinkManager.GetItemUrl(Sitecore.Context.Database.GetItem(new ID("{A13BC59B-DB5F-423B-9EEE-2724EEB507B9}")), o));
            try
            {
                var m = new MailMessage(_from, _to)
                {
                    Subject = "A Literature Search Request",
                    Body = body
                };
                message = Mail.SendMail(m);

                message = Mail.SendMail(m);

                try
                {
                  var libraryRequest = new LibrarySearchSubmit
                  {
                    DateNeededBy = ls.DateNeededBy,
                    CurrentDate = ls.CurrentDate,
                    YearsToBeCovered = ls.YearsToBeCovered,
                    Name = ls.Name,
                    Email = ls.Email,
                    Rush = ls.Rush,
                    Ages = ages,
                    Profession = ls.SelectedProfession,
                    Department = ls.Dept,
                    Location = ls.SelectedLocation,
                    SearchRequest = ls.SearchRequest,
                    SearchQualifications = qualifications,
                    KindOfSearch = kinds,
                    PurposeOfSearch = audiences,
                    DeliveryType = deliverTypes,
                    Phone = ls.PhoneNumber
                  };
                  var json = JsonConvert.SerializeObject(libraryRequest);
                  var response = APIHelper.PostDataToMicroServiceWithHeaders(_libraryRequest, null, "LibraryRequest|"+json);
                  if (response.StatusCode != HttpStatusCode.OK)
                  {
                    Log.Error("Bad Request Library Request Microservice " + response.StatusDescription, this);
                  }
                }
                catch (Exception e)
                {
                  Log.Error("Exception sending data to Library Request Microservice " + e.Message, this);
                } 
                var librarySearchUrl = (LinkManager.GetItemUrl(Sitecore.Context.Database.GetItem(new ID("{B8C1724A-39A1-4FB6-BFB5-54DA909F71FF}")), o));
                

                if (string.IsNullOrEmpty(message)) return Redirect(librarySearchUrl);
            }
            catch (Exception e)
            {
                return RedirectToRoute(MvcSettings.SitecoreRouteName, new { pathInfo = pathInfo.TrimStart(new char[] { '/' }), error=e.Message });
            }

            return RedirectToRoute(MvcSettings.SitecoreRouteName, new { pathInfo = pathInfo.TrimStart(new char[] { '/' }), error=message });
        }
    }
}