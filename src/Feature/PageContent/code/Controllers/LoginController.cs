﻿using System;
using System.Web.Mvc;
using Sitecore.Analytics;
using Sitecore.XConnect;
using Sitecore.XConnect.Client;
using Sitecore.XConnect.Collection.Model;
using System.Linq;
using System.Security.Claims;
using System.Web;
using PP93Helix.Feature.PageContent.Models;
using Sitecore;
using Sitecore.Diagnostics;

namespace PP93Helix.Feature.PageContent.Controllers
{
    public class LoginController : Controller
    {
        public object UserSettings { get; private set; }

        // GET: Login
        public ActionResult Index()
        {
            if (Context.User.IsAuthenticated)
            {
                return Redirect("~/Home/");
            }
            else
            {
                return View();
            }
        }
    }
}