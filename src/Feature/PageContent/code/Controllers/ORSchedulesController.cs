﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PP93Helix.Feature.PageContent.Helpers;
using PP93Helix.Feature.PageContent.Models;
using Sitecore.Data;
using Sitecore.Data.Items;
using System;
using System.Collections.Generic;
using Sitecore.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using System.Web.Script.Serialization;
namespace PP93Helix.Feature.PageContent.Controllers
{
  public class ORSchedulesController : Controller
  {
    string GetORSchedulesPDF = Settings.GetSetting("GetORSchedulesPDF");
    string GetORSchedulePDFByFilename = Settings.GetSetting("GetORSchedulePDFByFilename");
    string ppAPIBaseAddress = Settings.GetSetting("ppAPIBaseAddress");
    string ORScheduleFolderID = Settings.GetSetting("ORScheduleFolderID");

    private string ORSchedulesKeyID = Settings.GetSetting("ORSchedulesKeyID");
    // GET: OR Schedule
    public ActionResult GetORSchedules()
    {
      HttpWebResponse httpWebResponse = null;
      httpWebResponse = APIHelper.GetDataFromAPI(GetORSchedulesPDF);
      List<ORSchedule> schedules = new List<ORSchedule>();
      var jsonData = "";
      if (httpWebResponse != null)
      {
        Sitecore.Diagnostics.Log.Info("PhysicanPortal.Controllers.ScheduleController :: GetOrSchedules -> API Response : " + httpWebResponse, httpWebResponse);
        Sitecore.Diagnostics.Log.Info("PhysicanPortal.Controllers.ScheduleController :: GetOrSchedules -> API Response Code: " + httpWebResponse.StatusCode, httpWebResponse);
      }
      if (httpWebResponse != null)
      {
        using (var reader = new StreamReader(httpWebResponse.GetResponseStream()))
        {
          var objText = reader.ReadToEnd();
          JObject data = JObject.Parse(objText);
          schedules = data["schedules"].Select(c => c.ToObject<ORSchedule>()).ToList();
          jsonData = JsonConvert.SerializeObject(schedules);
        }

        var jsonResult = Json(new { data = jsonData }, JsonRequestBehavior.AllowGet);
        //schedule.schedules = schedules;
        //return PartialView(schedule);
        return jsonResult;
      }
      else
      {
        return null;
      }
    }
    public ActionResult GetPGetSchedule(string filename)
    {
      WebClient client = new WebClient();
      client.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
      client.Headers.Add("FILENAME", filename.ToString());

      byte[] result = client.UploadData(ppAPIBaseAddress + GetORSchedulePDFByFilename,
          "POST",
          System.Text.Encoding.UTF8.GetBytes("ID="));

      string fileName = System.DateTime.Now.ToString();
      if (!String.IsNullOrEmpty(client.ResponseHeaders["Content-Disposition"]))
      {
        fileName = client.ResponseHeaders["Content-Disposition"].Substring(client.ResponseHeaders["Content-Disposition"].IndexOf("filename=") + 9).Replace("\"", "");
      }
      return File(result, "application/octet-stream", fileName);
    }
    public ActionResult ORSchedules()
    {
      MediaLibrary scheduleList = new MediaLibrary();
      scheduleList.FileList = new List<MediaFiles>();

      //Database db = Sitecore.Configuration.Factory.GetDatabase("master");//Sitecore.Context.Database;
      ID ArticleFolderID = new ID(ORSchedulesKeyID);

      scheduleList.FileList = MediaLibraryController.GetMediaArticles(ArticleFolderID);

      return PartialView(scheduleList);
    }
  }

}