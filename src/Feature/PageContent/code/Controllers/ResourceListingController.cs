﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using System.Web.Mvc;
using PP93Helix.Feature.PageContent.Helpers;
using PP93Helix.Feature.PageContent.Models;

namespace PP93Helix.Feature.PageContent.Controllers
{
    public class ResourceListingController : Controller
    {
        // GET: ResourceListing
        public ActionResult Index()
        {
            ResourceListing listing = new Models.ResourceListing();
            listing.isValid = false;
            listing.Resources = new List<Resource>();
            listing.ComponentTitle = RenderingHelper.GetValueFromCurrentRenderingParameters("ComponentTitle");
            int max = 5;
            int.TryParse(RenderingHelper.GetValueFromCurrentRenderingParameters("MaxShown"), out max);

            Item[] folders = RenderingHelper.GetItemsFromCurrentRenderingParameters("SelectedFolders");

            ID LinkTemplateID = new Sitecore.Data.ID("393F6EAF-556B-4967-95F2-DA024537C006");
            ID DocumentTemplateID = new Sitecore.Data.ID("7E87CF06-9FDC-434C-BDAA-37808A4F751F");

            for (int i = 0;i < folders.Length && listing.Resources.Count < max; i++)
            {
                Item start = Sitecore.Context.Database.GetItem(folders[i].ID);

                foreach (Item j in start.GetChildren())
                {
                    if (j.TemplateID == LinkTemplateID || j.TemplateID == DocumentTemplateID)
                    {
                        Resource resource = new Models.Resource();

                        if (j.TemplateID == DocumentTemplateID)
                        {
                            resource.Type = "Document";

                            resource.Title = j.Fields["Title"].Value;

                            //resource.Path = j.Paths.Path;

                            FileField docField = j.Fields["Document"];
                            if (docField != null && docField.MediaItem != null)
                            {
                                resource.Path = Sitecore.Resources.Media.MediaManager.GetMediaUrl(docField.MediaItem);
                            }
                        }
                        if (j.TemplateID == LinkTemplateID)
                        {
                            resource.Type = "Link";

                            LinkField _link = j.Fields["Link"];
                            if (_link != null)
                            {
                                string url = "";
                                url = _link.GetFriendlyUrl();
                                url = RenderingHelper.parseGeneralLink(_link);
                                resource.Path = url;
                                resource.Title = _link.Text;

                            }
                        }
                        resource.Date = j.Fields["__Created"];
                        listing.Resources.Add(resource);
                    }

                    if (!listing.isValid && listing.Resources.Count >= 1)
                    {
                        listing.isValid = true;
                    }

                    if (listing.Resources.Count >= max)
                    {
                        break;
                    }
                }
            }

            return View(listing);
        }
    }
}