﻿using System.Web.Mvc;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using PP93Helix.Feature.PageContent.Models;

namespace PP93Helix.Feature.PageContent.Controllers
{
    public class SampleController : Controller
    {
        // GET: Sample
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SampleControllerRendering()
        {
            return View();
        }

        public ActionResult _workingWithTags()
        {
            Tags myTags = new Tags();

            Item myItem = Sitecore.Context.Item;

            MultilistField myItemsTags = myItem.Fields["__Semantics"];
            foreach (Item tagItem in myItemsTags.GetItems())
            {
                Sitecore.Diagnostics.Log.Info("Tag Name: " + tagItem.Name, tagItem);
            }

            myTags.theTags = myItemsTags.GetItems();

            return PartialView(myTags);
        }
    }
}