﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using Sitecore.Mvc.Presentation;
using PP93Helix.Feature.PageContent.Helpers;
using PP93Helix.Feature.PageContent.Models;
using System.Net.Http;
using Sitecore.Configuration;
using System.Net;
using System.IO;
using System.Web.Script.Serialization;
using Sitecore;

namespace PP93Helix.Feature.PageContent.Controllers
{
    public class ScheduleController : Controller
    {
        string ppAPIGetOnCallSchedule = Settings.GetSetting("ppAPIGetOnCallSchedule");
        // GET: Schedule
        public ActionResult Index()
        {
            HttpWebResponse httpWebResponse = null;
            httpWebResponse = APIHelper.GetDataFromAPI(ppAPIGetOnCallSchedule);
            List<Schedule> schedules = new List<Schedule>();
            Schedule schedule = new Models.Schedule();
            if (httpWebResponse != null)
            {
                Sitecore.Diagnostics.Log.Info("PhysicanPortal.Controllers.ScheduleController :: GetSchedule -> API Response : " + httpWebResponse, httpWebResponse);
                Sitecore.Diagnostics.Log.Info("PhysicanPortal.Controllers.ScheduleController :: GetSchedule -> API Response Code: " + httpWebResponse.StatusCode, httpWebResponse);
            }
            if (httpWebResponse != null)
                {
                using (var reader = new StreamReader(httpWebResponse.GetResponseStream()))
                {
                    JavaScriptSerializer js = new JavaScriptSerializer();
                    var objText = reader.ReadToEnd();
                    schedules = js.Deserialize<List<Schedule>>(objText);
                }
                schedule.schedules = schedules;
                return PartialView(schedule);
            }
            else
            {
                return null;
            }
        }
    }
}