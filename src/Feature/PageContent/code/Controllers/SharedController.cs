﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using Sitecore.Mvc.Presentation;
using PP93Helix.Feature.PageContent.Helpers;
using PP93Helix.Feature.PageContent.Models;
using Sitecore.Configuration;

namespace PP93Helix.Feature.PageContent.Controllers
{
    public class SharedController : Controller
    {
    public ActionResult logout()
        {
            Sitecore.Security.Authentication.AuthenticationManager.Logout();
            System.Web.Security.FormsAuthentication.SignOut();
            return Redirect("~/Login/");
          //return Redirect("/Home");
          //return Redirect(RedirectUri);
        }

        public ActionResult _relatedLinks()
        {
            RelatedLinks rLinks = new Models.RelatedLinks();
            rLinks.ComponentTitle = RenderingHelper.GetValueFromCurrentRenderingParameters("ComponentTitle");
            rLinks._relLinks = new List<Models.Link>();
            //List<RelatedLinks> rLinks = new List<RelatedLinks>();
            Item[] Links = RenderingHelper.GetItemsFromCurrentRenderingParameters("RelatedLinks");

            foreach (Item i in Links)
            {
                LinkField _link = i.Fields["Link"];
                if (_link != null)
                {
                    string url = "";
                    url = _link.GetFriendlyUrl();
                    url = RenderingHelper.parseGeneralLink(_link);
                    Link _lnk = new Link();
                    _lnk.LinkUrl = url;
                    _lnk.LinkText = _link.Text;

                    rLinks._relLinks.Add(_lnk);
                }
            }

            return PartialView(rLinks); // we're passing the rLinks object to our PartialView
        }
        public ActionResult _medicalDatabaseCarousel()
        {
            MedDatabaseCarousel carousel = new MedDatabaseCarousel();
            ID carouselID = new ID("600E1089-2FBC-4973-942C-A946A223CE74");
            Item carouselItem = Sitecore.Context.Database.GetItem(carouselID);
            carousel.DatabaseSlides = new List<DatabaseSlide>();
            foreach(Item slide in carouselItem.Children)
            {
                DatabaseSlide _slide = new DatabaseSlide();
                ImageField imgField = slide.Fields["BackgroundImage"];
                _slide.ImageUrl = Sitecore.Resources.Media.MediaManager.GetMediaUrl(imgField.MediaItem);
                LinkField linkField = slide.Fields["Link"];
                _slide.ActionText = linkField.Text;
                _slide.ActionUrl = RenderingHelper.parseGeneralLink(linkField);
                carousel.DatabaseSlides.Add(_slide);
            }

            return PartialView(carousel);
        }

        public ActionResult _pageLinkBannerAd()
        {
            PageLinkAdBanner adBanner = new PageLinkAdBanner();

            var ad = Sitecore.Context.Database.GetItem(RenderingContext.Current.Rendering.Parameters["PageAd"]);

            ImageField image = ad.Fields["Image"];
            if (image != null && image.MediaItem != null)
            {
                adBanner.imgUrl = Sitecore.Resources.Media.MediaManager.GetMediaUrl(image.MediaItem);
            } else
            {
                adBanner.imgUrl = "";
            }

            LinkField link = ad.Fields["Link"];
            if (link != null)
            {
                adBanner.linkUrl = RenderingHelper.parseGeneralLink(link);
            } else
            {
                adBanner.linkUrl = "";
            }

            return PartialView(adBanner);
        }

        public ActionResult _eventLinkBannerAd()
        {
            PageLinkAdBanner adBanner = new PageLinkAdBanner();

            var ad = Sitecore.Context.Database.GetItem(RenderingContext.Current.Rendering.Parameters["EventAd"]);

            ImageField image = ad.Fields["Image"];
            if (image != null && image.MediaItem != null)
            {
                adBanner.imgUrl = Sitecore.Resources.Media.MediaManager.GetMediaUrl(image.MediaItem);
            }
            else
            {
                adBanner.imgUrl = "";
            }

            ID eventField = new ID("5D22ADEF-0B67-4BA7-BB13-0B7D657BCF5C");
            Item[] eventAd = RenderingHelper.GetMultiListValues(ad, eventField);

            foreach(Item ev in eventAd)
            {
                adBanner.linkUrl = "/sitecore/content/home/cme/events and announcements?selected="
                    + ev.ID.ToString().Replace("{", "").Replace("}", "");
            }
            
            return PartialView(adBanner);
        }

        public ActionResult _documents()
        {
            //Initialize relatedDocuments variable
            RelatedDocuments relatedDocuments = new RelatedDocuments();
            //Initialize Documents List of variable
            relatedDocuments.Documents = new List<Document>();
            relatedDocuments.ComponentTitle = RenderingHelper.GetValueFromCurrentRenderingParameters("ComponentTitle");
            //Get Selected Documents Field from rendering parameters of Related Documents Rendering
            Item[] parameters = RenderingHelper.GetItemsFromCurrentRenderingParameters("SelectedDocuments");

            //Uncomment the following line to filter documents by tags matching the current user roles
            parameters = RenderingHelper.FilterItemsByTagsMatchingUserRoles(parameters).ToArray();

            //Loop
            foreach (Item selectedDoc in parameters)
            {
                //Initialize Document variable
                Document doc = new Document();
                //Add info from one selected doc to doc variable
                doc.Title = selectedDoc.Fields["Title"].Value;

                ImageField docIcon = selectedDoc.Fields["Icon"];
                doc.Icon = Sitecore.Resources.Media.MediaManager.GetMediaUrl(docIcon.MediaItem);

                FileField docField = selectedDoc.Fields["Document"];
                doc.Path = Sitecore.Resources.Media.MediaManager.GetMediaUrl(docField.MediaItem);

                //Add doc to list of documents on RelatedDocuments variable
                relatedDocuments.Documents.Add(doc);
            }
            //End Loop

            //Pass filled model to view
            return PartialView(relatedDocuments);
        }
    }
}