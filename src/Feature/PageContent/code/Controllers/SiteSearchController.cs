﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BHSearch.Models;
using BHSearch.Components;
using BHSearch.Helpers;
using Sitecore.Configuration;
using System.Reflection;
using System.Collections.ObjectModel;
using PP93Helix.Feature.PageContent.Models;
using Sitecore.Data.Items;
using PP93Helix.Feature.PageContent.Helpers;
using Sitecore.Links;

namespace PP93Helix.Feature.PageContent.Controllers
{
  public class SiteSearchController : Controller
  {
    static string FAPFolderKey = Settings.GetSetting("FAPFolderKey");
    
    // GET: SiteSearch
    public ActionResult Index()
    {
      return View();
    }

    [HttpPost]
    public JsonResult SiteQuery(SolrQuery obj)
    {
      try
      {
        //get url item
        var fapList = new Models.MediaLibrary();
        fapList.FileList = new List<Models.MediaFiles>();

        Sitecore.Data.ID ArticleFolderID = new Sitecore.Data.ID(FAPFolderKey);

        fapList.FileList = MediaLibraryController.GetMediaArticles(ArticleFolderID);
        ExecuteSearch search = new ExecuteSearch();
        obj.Handler = "/pp_physician_index/siteSearch";

        obj.Facets = new List<string>(new string[] {
                    "_template"
                 });

        SolrResponse response = search.Execute(obj);

        //Map Results to SiteSearchResult Model
        List<SiteSearchResult> results = new List<SiteSearchResult>();
        PropertyInfo[] properties = typeof(SiteSearchResult).GetProperties();


        foreach (Dictionary<string, object> doc in response.Results)
        {
          try
          {


            SiteSearchResult result = new SiteSearchResult();

            foreach (PropertyInfo prop in properties)
            {
              try
              {

                object value = "";
                if (doc.TryGetValue(prop.Name, out value))
                {
                  prop.SetValue(result, value);
                }
                else
                {
                  prop.SetValue(result, null);
                }
              }
              catch (Exception e)
              {
                
              }
            }

            if (result.PhotoURL == null)
            {
              bool found = false;
              foreach (var t in fapList.FileList)
              {
                if (t.Title != result.ContentID) continue;
                result.PhotoURL = t.Path;
                found = true;
                break;
              }

              if (!found)
              {
                result.PhotoURL = "../../Images/placeholder.png";
              }
            }


            result.UniqueID = result.UniqueID.Substring(result.UniqueID.LastIndexOf('{') + 1, 36).ToUpper();

            if (result.Template.ToLower() == "1A889B78C81E424FA416E51C4C34D343".ToLower())
            {
              //If Physician Template Type = People
              result.Type = "People";

            }
            else if (result.Template.ToLower() == "693F3916A5434FD2894628B9CE58BF53".ToLower())
            {
              //If Event Template Type = Event
              result.Type = "Event";
              Item item = Sitecore.Context.Database.GetItem(result.UniqueID);
              // details is not indexing so I will add it here from sitecore
              result.Details = new ArrayList {item["Details"]};
            }
            else if (result.Template.ToLower() == "28345347C9B4460EB4E77F5890A42724".ToLower())
            {
              //If Article Template Type = Article
              result.Type = "Article";
            }
            else if (result.Template.ToLower() == "0603F16635B8469F8123E8D87BEDC171".ToLower())
            {
              //If PDF Template Type = PDF
              result.Type = "Document";
              var item = Sitecore.Context.Database.GetItem(result.UniqueID);
              if (item.Paths.IsMediaItem)
              {
                var mediaItem = new MediaItem(item);
                var URL = Sitecore.Resources.Media.MediaManager.GetMediaUrl(mediaItem);
                result.Path = Sitecore.Resources.Media.HashingUtils.ProtectAssetUrl(URL);
              }
            }
            else if (result.Template.ToLower() == "166927339A6145E6B0D44C0C06F8DD3C".ToLower() ||
                     result.Template.ToLower() == "7BB0411F50CD4C21AD8F1FCDE7C3AFFE".ToLower())
            {
              //If Word Doc Template Type = Doc
              result.Type = "Word Document";
              var item = Sitecore.Context.Database.GetItem(result.UniqueID);
              if (item.Paths.IsMediaItem)
              {
                var mediaItem = new MediaItem(item);
                var URL = Sitecore.Resources.Media.MediaManager.GetMediaUrl(mediaItem);
                result.Path = Sitecore.Resources.Media.HashingUtils.ProtectAssetUrl(URL);
              }
            }
            else if (result.Template.ToLower() == "962B53C4F93B4DF99821415C867B8903".ToLower())
            {
              //If File Template Type = File
              result.Type = "File";
              var item = Sitecore.Context.Database.GetItem(result.UniqueID);
              if (item.Paths.IsMediaItem)
              {
                var mediaItem = new MediaItem(item);
                var URL = Sitecore.Resources.Media.MediaManager.GetMediaUrl(mediaItem);
                result.Path = Sitecore.Resources.Media.HashingUtils.ProtectAssetUrl(URL);
              }
            }
            else
            {
              //Otherwise, Type = Page
              var defaultUrlOptions = LinkManager.GetDefaultUrlBuilderOptions();
              defaultUrlOptions.EncodeNames = true;
              result.Type = "Page";
              var item = Sitecore.Context.Database.GetItem(result.UniqueID);
              result.Path = LinkManager.GetItemUrl(item, defaultUrlOptions);
            }

            results.Add(result);
          }
          catch (Exception e)
          {
            
          }
        }

        foreach (var res in results)
        {
          Sitecore.Diagnostics.Log.Info("***************SiteSearchController -> result : " + res.ContentID.ToString(), this);
        }

        JsonResponse json = new JsonResponse();
        json.TotalHits = response.TotalHits;
        json.OriginalQuery = response.OriginalQuery;

        json.Results = ViewHelper.RenderViewToString(this.ControllerContext, "_SearchResults", new SiteSearchResultView()
        {
          Start = obj.Start,
          Results = results
        });

        // facets
        RenderFacetView(response, "_template", json, "_FacetCheckboxes");

        return Json(json);
      }
      catch (Exception ex)
      {
        Sitecore.Diagnostics.Log.Error("SiteSearchController -> SiteQuery : " + ex.ToString(), this);
        return Json(ex);
      }
    }

    [HttpPost]
    public JsonResult SiteTypeahead(SolrQuery obj)
    {
      try
      {
        // Execute Search
        ExecuteSearch search = new ExecuteSearch();
        obj.Handler = "/pp_physician_index/siteSuggest";
        SolrResponse response = search.Execute(obj);

        // Map Results to Physician Model
        List<SiteSearchResult> Results = new List<SiteSearchResult>();
        PropertyInfo[] properties = typeof(SiteSearchResult).GetProperties();

        foreach (Dictionary<string, object> doc in response.Results)
        {
          SiteSearchResult result = new SiteSearchResult();

          foreach (PropertyInfo prop in properties)
          {
            object value = "";
            if (doc.TryGetValue(prop.Name, out value))
            {
              prop.SetValue(result, value);
            }
            else
            {
              prop.SetValue(result, null);
            }
          }
          Results.Add(result);
        }

        return Json(Results);
      }
      catch (Exception e)
      {
        return BuildJsonErrorResponse(e);
      }
    }
    [HttpPost]
    public JsonResult SiteFacets(SolrQuery obj)
    {
      try
      {
        // Execute Search
        ExecuteSearch search = new ExecuteSearch();
        obj.Query = "*";
        obj.Rows = 1;
        obj.Handler = "/pp_physician_index/siteSuggest";
        obj.Facets = new List<string>(new string[] {
                    "primaryspecialty_s",
                    "firstname_s",
                    "lastname_s",
                    "title_s"
                 });
        SolrResponse response = search.Execute(obj);

        //Convert the Facets to Sitecore Facets
        SitecoreResponse res = new SitecoreResponse();
        res.Facets = new Dictionary<string, List<string>>();
        foreach (var item in response.Facets)
        {
          var key = item.Key;

          List<string> value = new List<String>();
          foreach (var v in item.Value)
          {
            value.Add(v.Key);
          }
          res.Facets.Add(key, value);
        }
        res.Facets = res.Facets;
        // Convert responseOriginalQuery to sitecoreoriginal query
        SitecoreQuery query = new SitecoreQuery();
        query.Location = response.OriginalQuery.Location;
        query.query = response.OriginalQuery.Query;
        query.Rows = response.OriginalQuery.Rows;
        query.Sort = response.OriginalQuery.Sort;
        query.Start = response.OriginalQuery.Start;

        res.OriginalQuery = query;
        res.Results = response.Results;
        res.TotalHits = response.TotalHits;


        //sResponse.Pagination = response.Results.
        return Json(RenderFacetList(res));
      }
      catch (Exception e)
      {
        return BuildJsonErrorResponse(e);
      }
    }
    
    private Dictionary<string, List<string>> RenderFacetList(SitecoreResponse response)
    {
      Dictionary<string, List<string>> Facets = new Dictionary<string, List<string>>();
      Facets = response.Facets;
    
      return Facets;
    }
    private void RenderFacetView(SolrResponse response, string facetField, JsonResponse json, string partial)
    {
      FacetView facetView = new FacetView();
      ICollection<KeyValuePair<string, int>> facets = new Collection<KeyValuePair<string, int>>();
      if (response.Facets.TryGetValue(facetField, out facets))
      {
        string filter = "";
        if (response.OriginalQuery.Filters.TryGetValue(facetField, out filter))
        {
          facetView.Filter = filter.Split(',').ToList();
        }

        foreach (var facet in facets)
        {
          Sitecore.Diagnostics.Log.Info("**************** Site Search Render Facets view key " + facet.Key.ToString(), facet);
          Sitecore.Diagnostics.Log.Info("**************** Site Search Render Facets view value " + facet.Value.ToString(), facet);
        }

        facetView.Facet = new KeyValuePair<string, ICollection<KeyValuePair<string, int>>>(facetField, facets);
        json.Facets.Add(facetField, ViewHelper.RenderViewToString(this.ControllerContext, partial, facetView));
      }
    }

    private JsonResult BuildJsonErrorResponse(Exception e)
    {
      Dictionary<string, object> error = new Dictionary<string, object>();
      error.Add("Status", "Error");
      // Comment Below to Hide Debugging
      object stacktrace = new Dictionary<string, string>();
      if (e.InnerException == null)
      {
        stacktrace = new
        {
          Message = e.Message != null ? e.Message : "",
          Trace = e.StackTrace != null ? e.StackTrace : ""
        };
      }
      else
      {
        stacktrace = new
        {
          Message = e.InnerException.Message != null ? e.InnerException.Message : "",
          Trace = e.InnerException.StackTrace != null ? e.InnerException.StackTrace : ""
        };
      }
      error.Add("Exception", new
      {
        Message = e.Message,
        //Trace = e.StackTrace +
        InnerException = stacktrace
      });

      return Json(error);
    }
  }
}