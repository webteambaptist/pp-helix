﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using Sitecore.Mvc.Presentation;
using PP93Helix.Feature.PageContent.Helpers;
using PP93Helix.Feature.PageContent.Models;
using Sitecore.Configuration;
using System.Net;
using System.IO;
using System.Web.Script.Serialization;

using System.Text;
using System.Net.Mail;
using System.Security.Claims;

namespace PP93Helix.Feature.PageContent.Controllers
{
  public class TaskManagerController : Controller
  {
    string ppAPIGetTasks = Settings.GetSetting("ppAPIGetTasks");
    string ppAPIUpdateTask = Settings.GetSetting("ppAPIUpdateTask");
    string fileDirectory = Settings.GetSetting("fileDirectory");

    public ActionResult _taskManager()
    {
      TaskManager taskManager = new TaskManager();
      taskManager.Tasks = new List<MSTask>();
      var claim = ClaimsPrincipal.Current.Identities.First().Claims.FirstOrDefault(x => x.Type == "echoid");
      string echoID = "";
      if (claim!=null)
      {
        echoID = claim.Value;
      }
      taskManager.Tasks = GetTasks(echoID);

      return PartialView(taskManager);
    }


    private List<MSTask> GetTasks(string echoID)
    {
      List<MSTask> userTasks = new List<MSTask>();


      if (!string.IsNullOrEmpty(echoID))
      {
        try
        {
          HttpWebResponse httpWebResponse = null;
          httpWebResponse = APIHelper.GetDataFromAPI(ppAPIGetTasks, "echoid|" + echoID);
          if (httpWebResponse != null)
          {
            Sitecore.Diagnostics.Log.Info("PhysicanPortal.Controllers.TaskManagerController :: GetTasks -> API Response : " + httpWebResponse, httpWebResponse);
            Sitecore.Diagnostics.Log.Info("PhysicanPortal.Controllers.TaskManagerController :: GetTasks -> API Response Code: " + httpWebResponse.StatusCode, httpWebResponse);

            using (var reader = new StreamReader(httpWebResponse.GetResponseStream()))
            {
              JavaScriptSerializer js = new JavaScriptSerializer();
              var objText = reader.ReadToEnd();
              userTasks = js.Deserialize<List<MSTask>>(objText);
            }
            userTasks.RemoveAll(IsCompleted);
            userTasks = userTasks.OrderBy(t => t.DueDate).ToList();
          }
        }
        catch (Exception ex)
        {
          Sitecore.Diagnostics.Log.Error("PhysicanPortal.Controllers.TaskManagerController :: GetTasks -> Exception : " + ex.ToString(), ex, ex.Data);
          userTasks = null;
          TempData["TaskManagerResponse"] = "500 Internal Error";
        }
      }
      else
      {
        userTasks = null;
        TempData["TaskManagerResponse"] = "No Echo ID";
      }

      return userTasks;
    }

    private static bool IsCompleted(MSTask task)
    {
      if (task.IsComplete == null || task.IsComplete == false)
      {
        return false;
      }
      else
      {
        return true;
      }
    }
    [HttpPost]
    public JsonResult UploadFile()
    {
      string status = null;
      List<string> returnList = new List<string>();
      if (Request.Files.Count > 0)
      {
        try
        {
          HttpFileCollectionBase files = Request.Files;

          HttpPostedFileBase file = files[0];
          string fileName = file.FileName;

          string path = Path.Combine(fileDirectory, fileName);
          file.SaveAs(path);
          status = "success";
        }
        catch (Exception ex)
        {
          status = null;
        }
      }
      returnList.Add(status);
      return Json(returnList, JsonRequestBehavior.DenyGet);
    }
    [HttpPost]
    public ActionResult UpdateTask(string taskID, string comments, string file)
    {
      string status = null;
      List<string> returnList = new List<string>();

      if (!string.IsNullOrEmpty(taskID))
      {
        try
        {
          HttpWebResponse httpWebResponse = null;
          httpWebResponse = APIHelper.PostDataToAPIwithHeaders(ppAPIUpdateTask, null, "taskID|" + taskID + "^" + "comments|" + comments + "^" + "file|" + file);
          if (httpWebResponse != null)
          {
            Sitecore.Diagnostics.Log.Info("PhysicanPortal.Controllers.TaskManagerController :: GetTasks -> API Response : " + httpWebResponse, httpWebResponse);
            Sitecore.Diagnostics.Log.Info("PhysicanPortal.Controllers.TaskManagerController :: GetTasks -> API Response Code: " + httpWebResponse.StatusCode, httpWebResponse);

            if (((int)httpWebResponse.StatusCode >= 200) && ((int)httpWebResponse.StatusCode <= 299))
            {
              status = "success";
            }
          }
        }
        catch (Exception ex)
        {

          Sitecore.Diagnostics.Log.Error("PhysicanPortal.Controllers.TaskManagerController :: GetTasks -> Exception : " + ex.ToString(), ex);
          status = null;
          TempData["TaskManagerResponse"] = "500 Internal Error";
        }
      }
      else
      {
        status = null;
        TempData["TaskManagerResponse"] = "No Task ID";
      }

      returnList.Add(status);
      return Json(returnList, JsonRequestBehavior.DenyGet);
    }
  }
}