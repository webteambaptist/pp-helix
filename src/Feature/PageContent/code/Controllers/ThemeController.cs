﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using PP93Helix.Feature.PageContent.Models;
using PP93Helix.Feature.PageContent.Helpers;
using Sitecore;
using Sitecore.Diagnostics;
using Sitecore.Links;

namespace PP93Helix.Feature.PageContent.Controllers
{
    public class ThemeController : Controller
    {
        // GET: Theme
        public ActionResult Header()
        {
            try
            {
                  if ((!Context.User.IsInRole("ad\\Physician_Portal_Access") && !Context.User.IsInRole("ad\\Physician_Portal_Support_Access") && !Context.User.IsInRole("sitecore\\PP_Access")))
                  {
                    Response.Cookies.Clear();
                    Session.Abandon();
                    var userName = Context.GetUserName();
                    Log.Info(string.Concat("User not physician (redirecting to apps.bmcjax.com) - ", userName), this);
                    return new RedirectResult("https://storefront.bmcjax.com/");
                    //Redirect("https://storefront.bmcjax.com/");
                  }
                  PpHeader _ppHeader = new PpHeader();
                _ppHeader.nav = new List<PpNav>();
                _ppHeader.icons = new List<PpIconLink>();
                _ppHeader.logo = new Models.PpLogo();

                // ("/sitecore/content/Components/Header Control/Logo Control")
                ID logoItemID = new Sitecore.Data.ID("C78986C3-1376-4368-AFAE-830C08C903A7");
                Item headerLogoItem = Sitecore.Context.Database.GetItem(logoItemID);

                ImageField imageField = headerLogoItem.Fields["Logo"];
                if (imageField != null && imageField.MediaItem != null)
                {
                    MediaItem siteLogo = new MediaItem(imageField.MediaItem);
                    _ppHeader.logo.SiteLogoURL = Sitecore.StringUtil.EnsurePrefix('/', Sitecore.Resources.Media.MediaManager.GetMediaUrl(siteLogo));
                    _ppHeader.logo.SiteLogo = siteLogo.Alt;
                }

                // ("/sitecore/content/Components/Navigation/Nav Control")
                ID navItemID = new Sitecore.Data.ID("30577276-28B1-46BE-874C-74A4D67B7541");
                Item headerNavItem = Sitecore.Context.Database.GetItem(navItemID);
                MultilistField headerNavMultiList = null;
                headerNavMultiList = headerNavItem.Fields["NavLinks"];
                MultilistField headerIconsMultiList = null;
                headerIconsMultiList = headerNavItem.Fields["IconLinks"];
                var defaultUrlOptions = LinkManager.GetDefaultUrlBuilderOptions();
                defaultUrlOptions.EncodeNames = true;
                foreach (Item parentNavItem in headerNavMultiList.GetItems())
                {
                    List<Item> childItems = null;
                    childItems = parentNavItem.GetChildren().ToList();
                    List<PpNavLinks> _childNavItem = new List<PpNavLinks>();
                    foreach (Item child in childItems)
                    {
                        bool isLink = false;
                        ID linkTemplate = new ID("393F6EAF-556B-4967-95F2-DA024537C006");
                        if (child.TemplateID == linkTemplate)
                        {
                            isLink = true;
                        }

                        if (!isLink)
                        {
                            _childNavItem.Add(new PpNavLinks
                            {
                                NavLinkText = child.DisplayName,
                               // NavLinkURL = child.Paths.Path,
                                NavLinkURL = LinkManager.GetItemUrl(child, defaultUrlOptions),
                                IsLink = isLink
                            });
                        } else
                        {
                            LinkField linkField = child.Fields["Link"];
                            _childNavItem.Add(new PpNavLinks
                            {
                                NavLinkText = child.DisplayName,
                                NavLinkURL = RenderingHelper.parseGeneralLink(linkField),
                                IsLink = isLink
                            }); 
                        } 
                    }

                    _ppHeader.nav.Add(new PpNav
                    {
                        NavItemText = parentNavItem.DisplayName,
                        //NavItemURL = parentNavItem.Paths.Path,
                        NavLinks = _childNavItem
                    });
                }

                foreach (Item iconLink in headerIconsMultiList.GetItems())
                {
                    ImageField iconImgField = iconLink.Fields["Icon"];
                    LinkField iconLinkField = iconLink.Fields["Link"];

                    if (iconLinkField != null && iconImgField != null && iconImgField.MediaItem != null)
                    {
                        _ppHeader.icons.Add(new PpIconLink
                        {
                            LinkUrl = RenderingHelper.parseGeneralLink(iconLinkField),
                            IconText = iconLink.Fields["Display Text"].Value,
                            IconImgUrl = Sitecore.StringUtil.EnsurePrefix('/', Sitecore.Resources.Media.MediaManager.GetMediaUrl(iconImgField.MediaItem))
                        });
                    }
                }

                return PartialView(_ppHeader);
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Info("Header/Nav Exception : " + ex.ToString(), this);
                return null;
            }
        }

        public ActionResult NonPhysicianHeader()
        {
            try
            {
                PpHeader _ppHeader = new PpHeader();
                //_ppHeader.nav = new List<PpNav>();
                //_ppHeader.icons = new List<PpIconLink>();
                _ppHeader.logo = new Models.PpLogo();

                // ("/sitecore/content/Components/Header Control/Logo Control")
                ID logoItemID = new Sitecore.Data.ID("C78986C3-1376-4368-AFAE-830C08C903A7");
                Item headerLogoItem = Sitecore.Context.Database.GetItem(logoItemID);

                ImageField imageField = headerLogoItem.Fields["Logo"];
                if (imageField != null && imageField.MediaItem != null)
                {
                    MediaItem siteLogo = new MediaItem(imageField.MediaItem);
                    _ppHeader.logo.SiteLogoURL = Sitecore.StringUtil.EnsurePrefix('/', Sitecore.Resources.Media.MediaManager.GetMediaUrl(siteLogo));
                    _ppHeader.logo.SiteLogo = siteLogo.Alt;
                }

                return PartialView(_ppHeader);
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Info("NonPhysicianHeader/Nav Exception : " + ex.ToString(), this);
                return null;
            }
        }

        public ActionResult Alert()
        {

            return PartialView();
        }

        public ActionResult Breadcrumbs()
        {
            Breadcrumbs breadcrumbs = new Breadcrumbs();

            //Remember to set the correct Home Item ID in the GetBreadcrumbs method definition
            breadcrumbs = RenderingHelper.GetBreadcrumbs(Sitecore.Context.Item);
            
            return PartialView(breadcrumbs);
        }

        public ActionResult Footer()
        {
            return PartialView();
        }

        public ActionResult NonPhysicianFooter()
        {
            return PartialView();
        }
    }
}