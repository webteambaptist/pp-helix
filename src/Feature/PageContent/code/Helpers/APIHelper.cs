﻿using System;
using System.Collections.Generic;
using Sitecore.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;

namespace PP93Helix.Feature.PageContent.Helpers
{
  public class APIHelper
  {
    private static string ppMicroserviceBaseAddress = Settings.GetSetting("ppMicroServiceBaseAddress");
    
    public static HttpWebResponse GetDataFromMicroService(string strMicroServiceMethod, string input = "", string method = "GET")
    {
      Sitecore.Diagnostics.Log.Info("PhysicianPortal.Helpers.APIHelper :: GetDataFromAPI(" + strMicroServiceMethod + ") -> Input : " + input, strMicroServiceMethod);
      HttpWebResponse webResponse = null;
      try
      {
        var httpWebRequest = (HttpWebRequest)WebRequest.Create(ppMicroserviceBaseAddress + strMicroServiceMethod);
        httpWebRequest.ContentType = "application/json";
        httpWebRequest.Method = method;
        if (method.ToLower().Equals("post"))
          httpWebRequest.ContentLength = 0;
        httpWebRequest.Timeout = 20000;
        if (!string.IsNullOrEmpty(input))
        {
          if (input.Contains('^'))
          {
            foreach (var strHeaders in input.Split('^'))
            {
              if (!strHeaders.Contains('|')) continue;
              var strHeader = strHeaders.Split('|');
              httpWebRequest.Headers.Add(strHeader[0].Replace(".", ""), strHeader[1]);
            }
          }
          else if (input.Contains('|'))
          {
            var strHeader = input.Split('|');
            httpWebRequest.Headers.Add(strHeader[0].Replace(".", ""), strHeader[1]);
          }
        }
        webResponse = (HttpWebResponse)httpWebRequest.GetResponse();
      }
      catch (WebException wex)
      {
        Sitecore.Diagnostics.Log.Error("PhysicianPortal.Helpers.APIHelper :: GetDataFromMicroService(" + method + ") - (" + strMicroServiceMethod + ") / Header Input(" + input + ") -> APIException : " + wex.ToString(), wex, wex.Response);
      }
      catch (Exception ex)
      {
        Sitecore.Diagnostics.Log.Error("PhysicianPortal.Helpers.APIHelper :: GetDataFromMicroService(" + method + ") - (" + strMicroServiceMethod + ") / Header Input(" + input + ") -> Exception : " + ex.ToString(), ex, ex.Data);
      }
      return webResponse;
    }
    public static HttpWebResponse PostDataToMicroService(string strMicroServiceMethod, string input)
    {
      HttpWebResponse webResponse = null;
      try
      {
        Sitecore.Diagnostics.Log.Info("PhysicianPortal.Helpers.APIHelper :: PostDataToAPI(" + strMicroServiceMethod + ") -> DATA : " + input, strMicroServiceMethod);
        var httpWebRequest = (HttpWebRequest)WebRequest.Create(ppMicroserviceBaseAddress + strMicroServiceMethod);
        httpWebRequest.ContentType = "application/json";
        httpWebRequest.Method = "POST";
        httpWebRequest.Timeout = 20000;
        using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
        {
          streamWriter.Write(input);
          streamWriter.Flush();
          streamWriter.Close();
        }
        webResponse = (HttpWebResponse)httpWebRequest.GetResponse();
      }
      catch (WebException wex)
      {
        Sitecore.Diagnostics.Log.Error("PhysicianPortal.Helpers.APIHelper :: PostDataToMicroService(" + strMicroServiceMethod + ") -> APIException : " + wex.ToString(), wex, wex.Response);
      }
      catch (Exception ex)
      {
        Sitecore.Diagnostics.Log.Error("PhysicianPortal.Helpers.APIHelper :: PostDataToMicroService(" + strMicroServiceMethod + ") -> Exception : " + ex.ToString(), ex, ex.Data);
      }
      return webResponse;
    }
    public static HttpWebResponse PostDataToMicroServiceWithHeaders(string strMicroServiceMethod, string input, string headers)
    {
      HttpWebResponse webResponse = null;
      try
      {
        var httpWebRequest = (HttpWebRequest)WebRequest.Create(ppMicroserviceBaseAddress + strMicroServiceMethod);

        httpWebRequest.ContentType = "application/json";
        httpWebRequest.Method = "POST";
        httpWebRequest.Timeout = 200000;
        using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
        {
          streamWriter.Write(input);
          streamWriter.Flush();
          streamWriter.Close();
        }
        if (!string.IsNullOrEmpty(headers))
        {
          if (headers.Contains('^'))
          {
            foreach (var strHeaders in headers.Split('^'))
            {
              if (!strHeaders.Contains('|')) continue;
              var strHeader = strHeaders.Split('|');
              httpWebRequest.Headers.Add(strHeader[0], strHeader[1]);
            }
          }
          else if (headers.Contains('|'))
          {
            var strHeader = headers.Split('|');
            httpWebRequest.Headers.Add(strHeader[0], strHeader[1]);
          }
        }
        webResponse = (HttpWebResponse)httpWebRequest.GetResponse();
      }
      catch (WebException wex)
      {
        Sitecore.Diagnostics.Log.Error("PhysicianPortal.Helpers.APIHelper :: PostDataToMicroServiceWithHeaders(" + strMicroServiceMethod + ") -> APIException : " + wex.ToString(), wex, wex.Response);
      }
      catch (Exception ex)
      {
        Sitecore.Diagnostics.Log.Error("PhysicianPortal.Helpers.APIHelper :: PostDataToMicroServiceWithHeaders(" + strMicroServiceMethod + ") -> APIException : " + ex.ToString(), ex, ex.Data);
      }
      return webResponse;
    }

  }
}