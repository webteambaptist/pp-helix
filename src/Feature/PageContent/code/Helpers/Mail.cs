﻿using System;
using System.Collections.Generic;
using Sitecore.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Web;

namespace PP93Helix.Feature.PageContent.Helpers
{
    public class Mail
    {
        #region Properties
        public string From { get; set; }
        public string SendTo { get; set; }
        public string Cc { get; set; }
        public string Bcc { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public Attachment Attach { get; set; }
        public int Priority { get; set; }
        #endregion

        public static string SendMail(MailMessage m)
        {
            var smtpClient = new SmtpClient {DeliveryMethod = SmtpDeliveryMethod.Network};

            try
            {
                m.IsBodyHtml = true;

                var smtp = new SmtpClient();
                smtpClient.Host = Settings.GetSetting("RELAY");
                smtpClient.Port = 25;
                smtpClient.EnableSsl = false;

                var fromAddress = m.From;

                if (string.IsNullOrEmpty(m.To.ToString())) return "NO SENDER"; //must have a sender

                smtpClient.Send(m);

                return "";

            }
            catch (Exception err)
            {
                return err.Message;
            }
        }
        public class Msg
        {
            internal string SendTo;
            internal string Attachment;

            public Msg(string sendTo, string attachment)
            {
                SendTo = sendTo;
                Attachment = attachment;
            }
        }

    }
}