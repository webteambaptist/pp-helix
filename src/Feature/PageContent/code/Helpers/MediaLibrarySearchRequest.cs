﻿using System;
using System.Collections.Generic;
using Sitecore.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using PP93Helix.Feature.PageContent.Models;
using Sitecore.Pipelines.GetPagePreviewUrl;

namespace PP93Helix.Feature.PageContent.Helpers
{
    public class MediaLibrarySearchRequest
    {
        private static readonly string Options = Settings.GetSetting("MedicalLibraryOptions");
        public static MedicalLibraryOptionLists GetOptionValues()
        {
            var medicalLibraryOptions = new List<MedicalLibraryOptions>();
            var response = APIHelper.GetDataFromMicroService(Options);
            var lists = new MedicalLibraryOptionLists();
            if (response == null) return null;
            using (var reader = new StreamReader(response.GetResponseStream() ))
            {
                try
                {
                    var objText = reader.ReadToEnd();
                    if (!string.IsNullOrEmpty(objText))
                    {
                        medicalLibraryOptions = JsonConvert.DeserializeObject<List<MedicalLibraryOptions>>(objText);
                    }
                    else
                    {
                        Sitecore.Diagnostics.Log.Error("PhysicanPortal.Helpers.MediaLibrarySearchRequest :: GetOptionValues -> objText is null", objText);
                    }
                }
                catch (Exception e)
                {
                    Sitecore.Diagnostics.Log.Error("PhysicanPortal.Helpers.MediaLibrarySearchRequest :: GetOptionValues -> Error with JsonObject ", e.Message + " " + e.InnerException.Message);
                }
            }

            if (medicalLibraryOptions == null) return null;
            lists.professionList= medicalLibraryOptions.Where(x=>x.OptionName=="Profession").ToList();
            lists.locationList = medicalLibraryOptions.Where(x => x.OptionName == "Location").ToList();
            lists.deliveryTypes = medicalLibraryOptions.Where(x => x.OptionName == "DeliveryType").ToList();
            lists.qualificationList = medicalLibraryOptions.Where(x => x.OptionName == "Qualifications").ToList();
            lists.age = medicalLibraryOptions.Where(x => x.OptionName == "Ages").ToList();
            lists.kindsList = medicalLibraryOptions.Where(x => x.OptionName == "KindOfSearch").ToList();
            lists.audiences = medicalLibraryOptions.Where(x => x.OptionName == "PurposeOfSearch").ToList();

            return lists;
        }
    }
}