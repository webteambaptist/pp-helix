﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using Sitecore.Sites;
using System.Linq;
using Sitecore.Mvc.Presentation;
using System.Web;
using Sitecore.Links;
using Sitecore.Resources.Media;
using PP93Helix.Feature.PageContent.Models;
using Sitecore.Web.UI.WebControls;
using Sitecore.Security.Accounts;

namespace PP93Helix.Feature.PageContent.Helpers
{
    public class RenderingHelper
    {
        public static string GetValueFromCurrentRenderingParameters(string parameterName)
        {
            var rc = RenderingContext.CurrentOrNull;
            if (rc == null || rc.Rendering == null) return (string)null;
            var parametersAsString = rc.Rendering.Properties["Parameters"];
            var parameters = HttpUtility.ParseQueryString(parametersAsString);
            return parameters[parameterName];
        }

        public static Item[] GetItemsFromCurrentRenderingParameters(string parameterName)
        {
            List<Item> result = new List<Item>();
            var rc = RenderingContext.CurrentOrNull;
            if (rc != null)
            {
                var itemIds = GetValueFromCurrentRenderingParameters(parameterName)
                    ?? string.Empty;
                var db = rc.ContextItem.Database;

                var selectedItemIds = itemIds.Split('|');
                foreach (var itemId in selectedItemIds)
                {
                    Guid id = Guid.Empty;
                    if (Guid.TryParse(itemId, out id))
                    {
                        var found = db.GetItem(new ID(id));
                        if (found != null)
                        {
                            result.Add(found);
                        }
                    }
                }
            }
            return result.ToArray();
        }

        public static string parseGeneralLink(LinkField lf)
        {
            switch (lf.LinkType.ToLower())
            {
                case "internal":
                    // Use LinkMananger for internal links, if link is not empty
                    return lf.TargetItem != null ? LinkManager.GetItemUrl(lf.TargetItem) : string.Empty;
                case "media":
                    // Use MediaManager for media links, if link is not empty
                    return lf.TargetItem != null ? MediaManager.GetMediaUrl(lf.TargetItem) : string.Empty;
                case "external":
                    // Just return external links
                    return lf.Url;
                case "anchor":
                    // return anchor or # if null or empty
                    return !string.IsNullOrEmpty(lf.Anchor) ? lf.Anchor : "#";
                case "mailto":
                    // Just return mailto link
                    return lf.Url;
                case "javascript":
                    // Just return javascript
                    return lf.Url;
                default:
                    // Just please the compiler, this
                    // condition will never be met
                    return lf.Url;
            }
        }

        public static Item[] GetMultiListValues(Item item, ID fieldID)
        {
            return (new MultilistField(item.Fields[fieldID])).GetItems();
        }

        public static string[] GetValuesFromMultiListField(MultilistField multiField)
        {
            string[] values = new string[multiField.Count];
            int i = 0;
            foreach (Item item in multiField.GetItems())
            {
                values[i] = item.Name;
                i++;
            }
            return values;
        }

        public static string[] GetTagNamesFromItem(Item item)
        {
            MultilistField semantics = item.Fields["__semantics"];
            return RenderingHelper.GetValuesFromMultiListField(semantics);
        }

        public static List<Item> FilterItemsByTagsMatchingUserRoles(Item[] items)
        {
            List<string> rolesAsStrings = new List<string>();
            foreach (var role in Sitecore.Context.User.Roles)
            {
                rolesAsStrings.Add(role.Name.Substring(role.Name.LastIndexOf("\\") + 1));

                var baseRoles = RolesInRolesManager.GetRolesForRole(role, true);
                foreach (var bRole in baseRoles)
                {
                    rolesAsStrings.Add(bRole.Name.Substring(bRole.Name.LastIndexOf("\\") + 1));
                }
            }

            List<Item> filteredItems = new List<Item>();

            foreach (Item item in items)
            {
                //var tagNames = GetTagNamesFromItem(item);
                //if (tagNames.Contains("All Users and Groups"))
               // {
                    filteredItems.Add(item);
               // }
                //else
                //{
                //    var matches = tagNames.Intersect(rolesAsStrings.ToArray(), StringComparer.OrdinalIgnoreCase);
                //    if (matches.Count() > 0)
                //    {
                //        filteredItems.Add(item);
                //    }
                //}
            }

            return filteredItems;
        }

        public static Breadcrumbs GetBreadcrumbs(Item current)
        {

            ID homeID = new ID("E97CBF7C-BEE4-4A8B-8736-4C2129B69673");
            //Item homeItem = Sitecore.Context.Database.GetItem(homeID);

            Breadcrumbs breadcrumbs = new Breadcrumbs();
            breadcrumbs.Crumbtrail = new List<Crumb>();
            var defaultUrlOptions = LinkManager.GetDefaultUrlBuilderOptions();
            defaultUrlOptions.EncodeNames = true;
            while (current != null)
            {
                //Additional logic may be wanted here to filter items based on template ID etc

                Crumb crumb = new Crumb();
                crumb.Text = current.Name;
                //crumb.Url = current.Paths.Path;
                crumb.Url = LinkManager.GetItemUrl(current, defaultUrlOptions);

                ID defaultFolderID = new ID("A87A00B1-E6DB-45AB-8B54-636FEC3B5523");

                if (current.TemplateID == defaultFolderID)
                {
                    crumb.Type = "label";
                } else
                {
                    crumb.Type = "link";
                }

                breadcrumbs.Crumbtrail.Add(crumb);

                if (current.ID == homeID)
                {
                    break;
                } else
                {
                    current = current.Parent;
                } 
            }

            breadcrumbs.Crumbtrail.Reverse();

            return breadcrumbs;
        }
    }
}