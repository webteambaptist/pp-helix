﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data.Fields;

namespace PP93Helix.Feature.PageContent.Models
{
    public class Alerts
    {
        public List<SystemAlert> SystemAlerts { get; set; }
        public List<Alert> CurrentAlerts { get; set; }
    }

    public class SystemAlert //For Alert from API
    {
        public int id { get; set; }
        public string Message { get; set; }
        public bool IsPhysicianPortalVisible { get; set; }
        public DateTime? StartDt { get; set; }
        public DateTime? EndDt { get; set; }
        public bool IsEverywhere { get; set; }
    }

    public class Alert //For Sitecore Content Items
    {
        public string Title { get; set; }
        public string Details { get; set; }
        public DateField ExpireDate { get; set; }
    }
}