﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PP93Helix.Feature.PageContent.Models
{
    public class AppCatalogQuickLinks
    {
        public List<AppThumb> AppThumbs { get; set; }
        public AppCred AppCred { get; set; } 
        public List<QuickLink> QuickLinks { get; set; }
        public List<QuickLink> BHLinks { get; set; }

    }

    public class AppThumb
    {
        public string ID { get; set; }
        public string AppTitle { get; set; }
        public string AppLaunchURL { get; set; }
        public string AppDesc { get; set; }
        public string AppIcon { get; set; }
    }

    public class AppCred
    {
        [Newtonsoft.Json.JsonProperty("ASP.NET_SessionId")]
        public string SessionId { get; set; }
        public string CsrfToken { get; set; }
        [Newtonsoft.Json.JsonProperty("CtxsAuthId")]
        public string AuthID { get; set; }
        public string NSC_ESNS { get; set; }
    }
    public class QuickLink
    {
        public string ID { get; set; }
        public string ECHOID { get; set; }
        public string LinkTitle { get; set; }
        public string LinkURL { get; set; }
    }

}