﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using System.Web;

namespace PP93Helix.Feature.PageContent.Models
{
    public class ArticleListing
    {
        public string ComponentTitle { get; set; }

        public bool isValid { get; set; }

        public List<ArticleTeaser> ArticleTeasers { get; set; }
    }

    public class ArticleTeaser
    {

        public DateField Date { get; set; }

        public string Title { get; set; }

        public string Path { get; set; }

        public IHtmlString Subheading { get; set; }

        public string Thumb { get; set; }

    }
}