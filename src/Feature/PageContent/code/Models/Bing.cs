﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PP93Helix.Feature.PageContent.Models
{
    public class Bing
    {
        public string authenticationResultCode { get; set; }
        public string brandLogoUri { get; set; }
        public string copyright { get; set; }
        public List<ResourceSets> resourceSets { get; set; }
        public string statusCode { get; set; }
        public string statusDescription { get; set; }
        public string traceId { get; set; }
    }
    public class ResourceSets
    {
        public int estimatedTotal { get; set; }
        public List<Resources> resources { get; set; }
    }
    public class Resources
    {
        public string __type { get; set; }
        public float[] bbox { get; set; }
        public string name { get; set; }
        public Point point { get; set; }
        public _Address address { get; set; }
        public string confidence { get; set; }
        public string entityType { get; set; }
        public List<GeoPoints> geocodePoints { get; set; }
        public List<string> matchCodes { get; set; }
    }
    public class Point
    {
        public string type { get; set; }
        public float[] coordinates { get; set; }
    }
    public class _Address
    {
        public string addressLine { get; set; }
        public string adminDistrict { get; set; }
        public string adminDistrict2 { get; set; }
        public string countryRegion { get; set; }
        public string formattedAddress { get; set; }
        public string locality { get; set; }
        public string postalCode { get; set; }
    }
    public class GeoPoints
    {
        public string type { get; set; }
        public float[] coordinates { get; set; }
        public string calculationMethod { get; set; }
        public string[] usageTypes { get; set; }
    }
}