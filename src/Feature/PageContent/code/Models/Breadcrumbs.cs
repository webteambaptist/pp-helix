﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data.Items;

namespace PP93Helix.Feature.PageContent.Models
{
    public class Breadcrumbs
    {
        public List<Crumb> Crumbtrail { get; set; }
    }
    public class Crumb
    {
        public string Text { get; set; }
        public string Url { get; set; }
        public string Type { get; set; }
    }
}