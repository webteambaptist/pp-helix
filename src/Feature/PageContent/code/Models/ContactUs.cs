﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PP93Helix.Feature.PageContent.Models
{
    public class ContactUs
    {
        [Display(Name = "First Name")]
        [Required(ErrorMessage = "First Name is Required")]
        public string FirstName { get; set; }
        [Display(Name="Last Name")]
        [Required(ErrorMessage = "Last Name is Required")]
        public string LastName { get; set; }
        [Display(Name = "Email")]
        [Required(ErrorMessage = "Email is Required")]
        public string Email { get; set; }
        [Display(Name = "Subject")]
        [Required(ErrorMessage = "Subject is Required")]
        public string Subject { get; set; }
        [Display(Name = "Comments")]
        [Required(ErrorMessage = "Comments are Required")]
        public string Comments { get; set; }
        public string Error { get; set; }
    }
}