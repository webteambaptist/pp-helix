﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PP93Helix.Feature.PageContent.Models
{
    public class Department
    {
        public string Department1 { get; set; }
        public int FacilityId { get; set; }
        public int ID { get; set; }
        public string link { get; set; }
    }
}