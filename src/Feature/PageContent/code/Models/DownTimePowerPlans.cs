﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PP93Helix.Feature.PageContent.Models
{
    public class DownTimePowerPlans
    {
        public string Name { get; set; }
        public string ServiceLineValue { get; set; }
        public int ID { get; set; }
    }
}