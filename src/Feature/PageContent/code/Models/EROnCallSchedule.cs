﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PP93Helix.Feature.PageContent.Models
{
    public class EROnCallSchedule
    {
        public string Title { get; set; }
        public int ID { get; set; }
        public string Specialty { get; set; }
    }
}