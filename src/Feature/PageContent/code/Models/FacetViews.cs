﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PP93Helix.Feature.PageContent.Models
{
    public class FacetViews
    {
       public FacetViews()
        {
            Facet = new KeyValuePair<string, List<string>>();
            //Filter = "";
            Filter = new List<string>();
        }

        public KeyValuePair<string, List<string>> Facet { get; set; }
        //public string Filter { get; set; }
        public List<string> Filter { get; set; }
    }
}