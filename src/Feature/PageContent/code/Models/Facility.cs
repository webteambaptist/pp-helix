﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PP93Helix.Feature.PageContent.Models
{
    public class Facility
    {
        public string FacilityName { get; set; }
        public List<EROnCallSchedule> ErOnCallSchedule { get; set; }
    }
}