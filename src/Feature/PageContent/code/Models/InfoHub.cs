﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data.Fields;

namespace PP93Helix.Feature.PageContent.Models
{
    public class InfoHub
    {
        public string ComponentTitle { get; set; }

        public List<ArticleTeaser> ArticleTeasers { get; set; }
    }

}