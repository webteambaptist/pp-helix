﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PP93Helix.Feature.PageContent.Models
{
    public class Infomatics
    {
        public List<Doc> AllDocsList { get; set; }
    }
    public class Doc
    {
        public string DocHeading { get; set; }
        public string Picture { get; set; }
        public string NameTitle { get; set; }
        public string Content { get; set; }
        public string MediaLibraryFolder { get; set; }
    }
}