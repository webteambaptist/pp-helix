﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace PP93Helix.Feature.PageContent.Models
{
    public class JournalRequest
    {
        [Display(Name="Name")]
        [Required(ErrorMessage = "Name is Required")]
        public string Name { get; set; }
        [Display(Name = "Email")]
        [Required(ErrorMessage = "Email is Required")]
        public string Email { get; set; }
        [Display(Name = "Current Date")]
        public DateTime CurrentDate { get; set; } 
        [Display(Name = "Date Needed By")]
        public DateTime DateNeededBy { get; set; }
        public string SelectedProfession { get; set; }
        [Display(Name = "Profession")]
        public List<SelectListItem> Professions { get; set; } 
        [Display(Name = "Dept")]
        public string Dept { get; set; }
        public string SelectedLocation { get; set; }
        [Display(Name = "Location")]
        public List<SelectListItem> Locations { get; set; }
        public string SearchRequest { get; set; }
        [Display(Name = "Journal Article Requests (List all article citations in the box below)")]
        public string JournalArticleRequest { get; set; }
        [Display(Name = "Book Requests")]
        public string BookRequests { get; set; }
        public IList<string> SelectedDeliveryTypes { get; set; }
        [Display(Name = "Delivery Type")]
        public IList<SelectListItem> DeliveryTypes { get; set; }
        [Display(Name = "Phone #")]
        public string PhoneNumber { get; set; }
        [Display(Name = "Rush")]
        public bool Rush { get; set; }
    }
    public class JournalRequestSubmit
    {
      public string Name { get;set; }
      public string Email { get;set; }
      public DateTime CurrentDate { get; set; }
      public DateTime DateNeededBy { get;set; }
      public bool Rush { get; set; }
      public string Profession { get; set; }
      public string Department { get; set; }
      public string Location { get; set; }
      public string JournalArticleRequest { get; set; }
      public string BookRequest { get; set; }
      public string DeliveryType { get; set; }
      public string Phone { get; set; }
      public string FirstPublicationSupplied { get; set; }
      public string FirstPublicationYearDate { get; set; }
      public string SecondPublicationSupplied { get; set; }
      public string SecondPublicationYearDate { get; set; }
      public string ThirdPublicationSupplied { get; set; }
      public string ThirdPublicationYearDate { get; set; }
      public string ForthPublicationSupplied { get; set; }
      public string ForthPublicationYearDate { get; set; }
      public string FifthPublicationSupplied { get; set; }
      public string FifthPublicationYearDate { get; set; }
    }


}