﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PP93Helix.Feature.PageContent.Models
{
    public class LibrarySearch
    {
        [Display(Name = "Name")]
        [Required(ErrorMessage = "Name is Required")]
        public string Name { get; set; }
        [Display(Name = "Email")]
        [Required(ErrorMessage = "Email is Required")]
        public string Email { get; set; }
        [Display(Name = "Current Date")]
        public DateTime CurrentDate { get; set; } 
        [Display(Name = "Date Needed By")]
        public DateTime DateNeededBy { get; set; }
        public string SelectedProfession { get; set; }
        [Display(Name = "Profession")]
        public List<SelectListItem> Professions { get; set; } 
        [Display(Name = "Dept")]
        public string Dept { get; set; }
        public string SelectedLocation { get; set; }
        [Display(Name = "Location")]
        public List<SelectListItem> Locations { get; set; }
        [Display(Name = "Search Request")]
        public string SearchRequest { get; set; }
        [Display(Name = "Search Qualifications")]
        public IList<SelectListItem> Qualifications { get; set; }
        [Display(Name = "Ages")]
        public IList<SelectListItem> Ages { get; set; }
        [Display(Name = "Years to be covered")]
        public string YearsToBeCovered { get; set; }
        [Display(Name = "Kind of search you want")]
        public IList<SelectListItem> SearchKinds { get; set; }
        [Display(Name = "Purpose of the search/audience")]
        public IList<SelectListItem> Audiences { get; set; }
        [Display(Name = "Delivery Type")]
        public IList<SelectListItem> DeliveryTypes { get; set; }
        [Display(Name = "Phone #")]
        public string PhoneNumber { get; set; }
        [Display(Name = "Rush")]
        public bool Rush { get; set; }
    }

    public class LibrarySearchSubmit
    {
      public string Name { get;set; }
      public string Email { get;set; }
      public DateTime CurrentDate { get; set; }
      public DateTime DateNeededBy { get;set; }
      public bool Rush { get; set; }
      public string Profession { get; set; }
      public string Department { get; set; }
      public string Location { get; set; }
      public string SearchRequest { get; set; }
      public string SearchQualifications { get; set; }
      public string Ages { get; set; }
      public string YearsToBeCovered { get; set; }
      public string KindOfSearch { get; set; }
      public string PurposeOfSearch { get; set; }
      public string DeliveryType { get; set; }
      public string Phone { get; set; }
    }
}