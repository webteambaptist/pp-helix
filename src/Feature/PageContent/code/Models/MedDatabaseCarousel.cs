﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data.Items;

namespace PP93Helix.Feature.PageContent.Models
{
    public class MedDatabaseCarousel
    {
        public List<DatabaseSlide> DatabaseSlides { get; set; }
    }

    public class DatabaseSlide
    {
        public string ImageUrl { get; set; }
        public string ActionText { get; set; }
        public string ActionUrl { get; set; }
    }
}