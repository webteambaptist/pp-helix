﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Sitecore.Pipelines.GetPagePreviewUrl;

namespace PP93Helix.Feature.PageContent.Models
{
    public class MedicalLibraryOptionLists
    {
        public List<MedicalLibraryOptions> professionList { get; set; }
        public List<MedicalLibraryOptions> locationList { get; set; }
        public List<MedicalLibraryOptions> deliveryTypes { get; set; }
        public List<MedicalLibraryOptions> qualificationList { get; set; }
        public List<MedicalLibraryOptions> age { get; set; }
        public List<MedicalLibraryOptions> kindsList { get; set; }
        public List<MedicalLibraryOptions> audiences { get; set; }
    }
}