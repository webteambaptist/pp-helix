﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PP93Helix.Feature.PageContent.Models
{
    public class MedicalLibraryOptions
    {
        public string OptionName { get; set; }
        public string OptionValue { get; set; }
    }
}