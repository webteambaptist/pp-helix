﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PP93Helix.Feature.PageContent.Models
{
    public class PageLinkAdBanner
    {
        public string imgUrl { get; set; }
        public string linkUrl { get; set; }
    }
}