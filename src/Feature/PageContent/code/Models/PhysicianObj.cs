﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PP93Helix.Feature.PageContent.Models
{
    public class PhysicianObj
    {
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Suffix { get; set; }
        public string Gender { get; set; }
        public string JobTitle { get; set; }
        public string Languages { get; set; }
        public string PrimarySpecialty { get; set; }
        public string OtherSpecialties { get; set; }
        public string Education { get; set; }
        public string Certification { get; set; }
        public string Residency { get; set; }
        public string Biography { get; set; }
        public string BackupBiography { get; set; }
        public string PracticeInformation { get; set; }
        public string HospitalAffiliation { get; set; }
        public string AcceptedInsurance { get; set; }
        public string IsAcceptingPatients { get; set; }
        public string AgesTreated { get; set; }
        public string IsBPP { get; set; }
        public string ExcludeFromSearch { get; set; }
        public string CellPhoneNumber { get; set; }
        public string PagerNumber { get; set; }
        public string PrimaryLocation { get; set; }
        public string PrimaryLocationLatLng { get; set; }
        public string OtherLocations { get; set; }
        public string OtherLocationsLatLngs { get; set; }
        public string Distance { get; set; }
        public string Path { get; set; }
        public string ContentID { get; set; }
        public string PhotoURL {get; set; }
    }
}