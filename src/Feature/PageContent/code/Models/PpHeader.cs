﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data;

namespace PP93Helix.Feature.PageContent.Models
{
    public class PpHeader
    {
        public PpLogo logo { get; set; }
        public List<PpNav> nav { get; set; }
        public List<PpIconLink> icons { get;set;}
    }
    public class PpLogo
    {
        public string SiteLogo { get; set; }
        public string SiteLogoURL { get; set; }
    }
    public class PpNav
    {
        public string NavItemText { get; set; }
        public string NavItemURL { get; set; }
        public List<PpNavLinks> NavLinks { get; set; }
    }
    public class PpNavLinks
    {
        public string NavLinkText { get; set; }
        public string NavLinkURL { get; set; }
        public bool IsLink { get; set; }
    }
    public class PpIconLink
    {
        public string IconImgUrl { get; set; }
        public string IconText { get; set; }
        public string LinkUrl { get; set; }
    }
}