﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PP93Helix.Feature.PageContent.Models
{
    public class RelatedDocuments
    {
        public string ComponentTitle { get; set; }
        public List<Document> Documents { get; set; }
    }

    public class Document
    {
        public string Title { get; set; }
        public string Icon { get; set; }
        public string Path { get; set; }
    }
}