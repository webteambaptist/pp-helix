﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace PP93Helix.Feature.PageContent.Models
{
    public class RelatedLinks
    {
        public string ComponentTitle { get; set; }
        public List<Link> _relLinks { get; set; }
    }

    public class Link
    {
        public string LinkText { get; set; }
        public string LinkUrl { get; set; }
    }
}