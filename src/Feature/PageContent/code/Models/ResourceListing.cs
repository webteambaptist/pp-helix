﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Data.Fields;
using System.Web;

namespace PP93Helix.Feature.PageContent.Models
{
    public class ResourceListing
    {
        public string ComponentTitle { get; set; }

        public bool isValid { get; set; }

        public List<Resource> Resources { get; set; }
    }

    public class Resource
    {

        public DateField Date { get; set; } 

        public string Title { get; set; }

        public string Path { get; set; }

        public string Type { get; set; }

    }
}