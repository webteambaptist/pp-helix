﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PP93Helix.Feature.PageContent.Models
{
    public class Schedule
    {
        public int ID { get; set; }
        public string facility { get; set; }
        public List<Department> departments { get; set; }

        public List<Schedule> schedules { get; set; }
    }
}