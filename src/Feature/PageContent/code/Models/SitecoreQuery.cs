﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PP93Helix.Feature.PageContent.Models
{
    public class SitecoreQuery
    {

        public SitecoreQuery()
        {
            Rows = 10;
            Start = 0;
            query = "*";
            Location = "";
            Distance = "";
            Facets = new List<string>();
            Filters = new Dictionary<string, string>();
            SearchZip = "";
            Sort = "";
            OmniSearch = "";
        }
        public string query { get; set; }
        public int Start { get; set; }
        public int Rows { get; set; }
        public string Location { get; set; }
        public string Distance { get; set; }
        public List<string> Facets { get; set; }
        public Dictionary<string, string> Filters { get; set; }
        public string Sort { get; set; }
        public string OmniSearch { get; set; }
        public string SearchZip { get; set; }
    }
}