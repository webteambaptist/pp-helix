﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PP93Helix.Feature.PageContent.Models
{
    public class SitecoreViewResponse
    {
        public SitecoreViewResponse()
        {
            Facets = new Dictionary<string, string>();
        }
        public string Results { get; set; }
        public Dictionary<string, string> Facets { get; set; }
        public SitecoreQuery OriginalQuery { get; set; }
        public int TotalHits { get; set; }
        public string Pagination { get; set; }
    }
}