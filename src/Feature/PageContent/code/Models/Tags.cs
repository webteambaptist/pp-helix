﻿using Sitecore.Data.Items;

namespace PP93Helix.Feature.PageContent.Models
{
    public class Tags
    {
        public Item[] theTags { get; set; }
    }
}