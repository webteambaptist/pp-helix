﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data.Fields;

namespace PP93Helix.Feature.PageContent.Models
{
    public class TaskManager
    {
        public List<MSTask> Tasks { get; set; }
    }

    public class MSTask
    {
        public int ID { get; set; }
        public string EchoDoctorNumber { get; set; }
        public string Task { get; set; }
        public string DoctorName { get; set; }
        public DateTime DueDate { get; set; }
        public Nullable<bool> IsComplete { get; set; }
        public Nullable<DateTime> UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public Nullable<bool> IsSubmitted { get; set; }
        public Nullable<DateTime> SubmitDate { get; set; }

        public string dueDateCSS()
        {
            var daysLeft = (DueDate - DateTime.Now).TotalDays;

            if (daysLeft < 3) return "red-date";
            else if (daysLeft < 7) return "orange-date";
            else return "";
        }

        //Sample Task JSON
        //"ID": 1,
        //"EchoDoctorNumber": "1512",
        //"Task": "State License Expiring",
        //"DoctorName": "Tellam, George S., DPM",
        //"DueDate": "2018-03-31T00:00:00",
        //"IsComplete": true,
        //"UpdatedDate": "2018-03-22T13:26:28.23",
        //"UpdatedBy": "MSTIL003",
        //"IsSubmitted": null,
        //"SubmitDate": null
    }
}