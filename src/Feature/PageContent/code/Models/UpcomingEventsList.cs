﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Data.Fields;

namespace PP93Helix.Feature.PageContent.Models
{
    public class UpcomingEventsList
    {
        public List<UpcomingEvent> Events { get; set; }
 
        public string ComponentTitle { get; set; }
        public int NumEventsToShow { get; set; }
    }

    public class UpcomingEvent
    {
        public string ID { get; set; }
        public string Title { get; set; }
        public string Location { get; set; }
        public string Details { get; set; }
        public DateField Start { get; set; }
        public DateField End { get; set; }
        public string AllDay { get; set; }
    }
}