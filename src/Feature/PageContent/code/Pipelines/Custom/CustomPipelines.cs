﻿using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Pipelines;
using System;
using System.Collections.Generic;
using Sitecore.Configuration;
using System.Linq;
using System.Web;
using System.Web.Caching;

namespace PP93Helix.Feature.PageContent.Pipelines.Custom
{
    public class CustomPipelines
    {
        static string strProviderParentID = Settings.GetSetting("providerParentID");
        public void Process(PipelineArgs args)
        {
            LoadFAP();
        }
        private void LoadFAP()
        {
            try
            {
                var fap = (List<Item>)System.Web.HttpContext.Current.Cache["sitecoredata"];
                if (fap == null)
                {
                    using (new Sitecore.SecurityModel.SecurityDisabler())
                    {
                        Database db = Database.GetDatabase("web");

                        var item = db.GetItem("/sitecore/content/Search/Providers");

                        var children = item.GetChildren()
                            .Where(x => x.Fields["JobTitle"].ToString() != "CRNA")
                            .OrderBy(f => f.Fields["LastName"].ToString())
                            .ThenBy(f => f.Fields["FirstName"].ToString())
                            .ToList();

                        //System.Web.HttpContext.Current.Cache.Insert("sitecoredata", children);
                        var now = DateTime.Now;
                        DateTime expiresAt = now.AddDays(1); // expire every 24 hours
                        System.Web.HttpContext.Current.Cache.Insert(
                            "sitecoredata", children, null, expiresAt, Cache.NoSlidingExpiration);

                    }
                }
            }
            catch (Exception e)
            {
                Sitecore.Diagnostics.Log.Error("PhysicianPortal.Pipelines.Custom.CustomPipelines ", e.InnerException.Message);
            }
        }
    }
}