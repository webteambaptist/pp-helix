﻿using System;
using System.Collections.Generic;
using Sitecore.Diagnostics;
using Sitecore.Pipelines;
using SolrNet;
using Sitecore.Configuration;
namespace PP93Helix.Feature.PageContent.Pipelines.Initialize
{
    
    public class InitConnection
    {
        public void Process(PipelineArgs args)
        {
            Log.Info("Starting up Solr Connection is starting", this);
            string solrConnection = Settings.GetSetting("solrConnection");
            //SolrNet.Startup.Init<Dictionary<string, object>>(solrConnection);
            // SolrNet.SolrNet.GetBasicServer<Dictionary<string, object>>(solrConnection);
            Startup.Init<Dictionary<string, object>>(solrConnection);
        }
    }
}