﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Sitecore.Pipelines;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Configuration;
using Sitecore;

namespace PP93Helix.Feature.PageContent.Pipelines.Initialize
{
    public class RegisterApiRoutes : Sitecore.Mvc.Pipelines.Loader.InitializeRoutes
    {
        static string strProviderParentID = Settings.GetSetting("providerParentID");
        public override void Process(PipelineArgs args)
        {
            
            try
            {
                //Database db = Sitecore.Configuration.Factory.GetDatabase("master");
                using (new Sitecore.SecurityModel.SecurityDisabler())
                {
                    Database db = Database.GetDatabase("web");

                    var item = db.GetItem("/sitecore/content/Search/Providers");
                    //Item parentItem = db.GetItem(strProviderParentID);

                    var children = item.GetChildren()
                        .Where(x => x.Fields["JobTitle"].ToString() != "CRNA")
                        .OrderBy(f => f.Fields["LastName"].ToString())
                        .ThenBy(f => f.Fields["FirstName"].ToString())
                        .ToList();

                    System.Web.HttpContext.Current.Cache.Insert("sitecoredata", children);
                }
            } 
            catch(Exception e)
            {
                Sitecore.Diagnostics.Log.Error("PhysicianPortal.Pipelines.Initialize.RegisterApiroutes.Process ", e.InnerException.Message);
            }
        }
    }
}