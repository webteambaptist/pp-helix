﻿function buildSelect(table) {
    table.columns([0, 1]).every(function () {
        var column = table.column(this, { search: 'applied' });
        var select = $('<select><option value="">No Filter</option></select>')
        .appendTo($(column.footer()).empty())
        .on('change', function () {
            var val = $.fn.dataTable.util.escapeRegex(
              $(this).val()
            );

            column
            .search(val ? '^' + val + '$' : '', true, false)
            .draw();
        });

        column.data().unique().sort().each(function (d, j) {
            select.append('<option value="' + d + '">' + d + '</option>');
        });

        // The rebuild will clear the exisiting select, so it needs to be repopulated
        var currSearch = column.search();
        if (currSearch) {
            select.val(currSearch.substring(1, currSearch.length - 1));
        }
    });
}