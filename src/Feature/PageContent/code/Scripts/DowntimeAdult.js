﻿var api = "DowntimeMicroservice/api/downtime/DowntimePowerPlansAdultsByID";
$(document).ready(function () {
    var table = $('#downtimeadult').DataTable({
        "dom": '<"top"if>rt<"bottom"lp><"clear">',
        "bAutoWidth": false,
        "ajax": {
            "url": "/api/Sitecore/DownTimePowerPlans/GetAdultPowerPlans",
            "error": function (errResp) {
                window.location.href = "../";
            },
            "dataSrc": function (json) {
                if (json != null && json.data != null && json.data.length > 0) {
                    if (json.data == "no-access")
                        window.location.href = "../";
                    else
                        return JSON.parse(json.data);
                }
                else { return ""; }
            },
            "type": "GET",
            "datatype": "json"
        },
        "language": {
            "emptyTable": "No Specialties!"
        },
        "columns": [
            {
                "data": "ServiceLineValue", "orderable": true
            },
            {
                "data": "Name",
                "orderable": true,
                render: function (data, type, row, meta) {
                    var link = '<a href="/api/Sitecore/DownTimePowerPlans/GetPDF?ID=' + row.ID + '&ppAPIGetPDF=' + api + "\"" + ' target="_blank">' + data + '</a>'
                    return link;
                }
            },
            { "data": "ID" },
        ],
        "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, 'All']],
        "pageLength": 25,
        "pagingType": "full_numbers",
        "aaSorting" : [[0, 'asc'], [1, 'asc']],
        responsive: true,
        select: {
            style: 'os',
            selector: 'tr td:last-child'
        },
       // orderData: [[0, 'asc'], [1, 'asc']],
    }).columns([2]).visible(false, false);

    buildSelect(table);
    table.on('draw', function () {
       // table.order([[1, 'desc']]);
        buildSelect(table);
    });

    
})
