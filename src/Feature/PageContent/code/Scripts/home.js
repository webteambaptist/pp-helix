﻿$(document).ready(function () {
    var wrapper = $(".input_fields_wrap");

    $('body').on('click', '.addlink-action', function () {
        addlink();
    })

    //EDITING LINK
    $(wrapper).on('click', '.addlink-edit-icon', function (e) {
        var target = $(this).closest('li').find('a');
        var edit_name = target.html();
        var edit_address = target.attr('href');

        //Modal
        var title = "Edit Link";
        var body = '<div class="input-group mb-3 col-sm-12 adlink_modal"><div class=input-group-prepend><span class=input-group-text id=inputGroup-sizing-default>Name</span></div><input aria-describedby=inputGroup-sizing-default aria-label=Default class="form-control link_name-edit" value ="' + edit_name + '"><div class=input-group-prepend><span class=input-group-text id=inputGroup-sizing-default>Link</span></div><input aria-describedby=inputGroup-sizing-default aria-label=Default class="form-control link_address-edit" value="' + edit_address + '"><span data-text-check-edit="&#10004" data-text-x-edit="&#10060" class="link-check-edit"></span></div>';

        showModal(title, body, true);
        var check_span_edit = $(".link-check-edit");

        //enable disable button inside edit modal
        (function () {

            //$('.confirm-btn').attr('disabled', true);

            var valid_check = $('.link_address-edit,.link_name-edit');
            //var link_check = $('.link_address-edit');
            valid_check.keyup(function () {

                RegExForUrlMatch($(".link_address-edit").val());
                if (RegExForUrlMatch($(".link_address-edit").val()) && $('.link_name-edit').val().length !== 0) {

                    check_span_edit.text(check_span_edit.data("text-check-edit") + " Link is Good");
                    check_span_edit.removeClass("addlink-check-span-bad").addClass("addlink-check-span-good");
                    $('.confirm-btn').prop('disabled', false);

                }
                else {
                    check_span_edit.text(check_span_edit.data("text-x-edit") + " Enter a Name and Use 'http:// or https://' in front of the Link (ex:https://www.google.com/)");
                    check_span_edit.removeClass("addlink-check-span-good").addClass("addlink-check-span-bad");
                    $('.confirm-btn').prop('disabled', true);
                }
            
            })



        })();
        //changing to latest value on modal click save/ok
        $('.confirm-btn').unbind().click(function (e) {
            var link_name_edit = $(".link_name-edit").val();
            var link_address_edit = $(".link_address-edit").val();
            //console.log("target", target);
            target.html(link_name_edit);
            target.attr('href', link_address_edit);
            postQLinks();
            link_name_edit = null;
            link_address_edit = null;
        });
    });



    // DELETING LIST
    $(wrapper).on("click", ".remove_field", function (e) { //user click on remove text
        e.preventDefault();
        $(this).closest('.addlink_dynlist').remove();
        postQLinks();
    });

    //HIDE AND SHOW DELETE 'X' FOR MANAGEING LINKS
    $(".addlink_manage").click(function () {
        toggleEditBtn();
    });


    //console.log('building addlink definition')
    //MODAL FOR ADDING LINK
    function addlink() {
        if ($('.addlink_manage').find('i').hasClass('fa-check')) {
            toggleEditBtn();
        }
        var title = "Add Link";
        var body = '<div class="input-group mb-3 col-sm-12 adlink_modal"><div class=input-group-prepend><span class=input-group-text id=inputGroup-sizing-default>Name</span></div><input aria-describedby=inputGroup-sizing-default aria-label=Default class="form-control link_name"><div class=input-group-prepend><span class=input-group-text id=inputGroup-sizing-default>Link</span></div><input aria-describedby=inputGroup-sizing-default aria-label=Default class="form-control link_address" value="http://"><span data-text-check="&#10004" data-text-x="&#10060" class="link-check"></span></div>';
        showModal(title, body, true);
        var check_span = $(".link-check");


        $('.confirm-btn').attr('disabled', true);

        var valid_check = $('.link_address,.link_name');
        //var link_check = $('.link_address');


        valid_check.keyup(function () {

            RegExForUrlMatch($(".link_address").val());
            if (RegExForUrlMatch($(".link_address").val()) && $('.link_name').val().length !== 0) {

                check_span.text(check_span.data("text-check") + " Link is Good");
                check_span.removeClass("addlink-check-span-bad").addClass("addlink-check-span-good");
                $('.confirm-btn').prop('disabled', false);

            }
            else {
                check_span.text(check_span.data("text-x") + " Enter a Name and use 'http:// or https://' in front of the Link (ex:https://www.google.com/)");
                check_span.removeClass("addlink-check-span-good").addClass("addlink-check-span-bad");
                $('.confirm-btn').prop('disabled', true);
            }
        })

        //APPENDING MODAL VALUES TO LINK LIST
        $('.confirm-btn').unbind().click(function (e) {
            //console.log('confirm click')
            var wrapper = $(".input_fields_wrap");
            var link_name = $(".link_name").val();
            var link_address = $(".link_address").val();


            if (link_name != null) {
                var $qlinks = $('.addlink_dynlist');
                var nextLinkID = '';
                if ($qlinks) {
                    nextLinkID = parseInt($qlinks.eq($qlinks.length - 1).find('.addlink-a').attr('linkid'), 10) + 1;
                }
                var echoID = $('#echoID').text() != '' ? $('#echoID').text() : '1';
              
                $(wrapper).append('<div class="addlink_dynlist"><li><a target="_blank" linkid="' + nextLinkID + '" echoid="' + echoID + '" class="addlink-a" href="' + link_address + '">' + link_name + '</a><span class="show_field" hidden><i class="fa fa-times addlink_dynlistX remove_field hand-cursor "></i>|<i class="fa fa-pencil-square-o addlink-edit-icon hand-cursor" aria-hidden="true"></i></span></li></div>'); //add dynamic list
            }

            link_name = null;
            link_address = null;

            postQLinks();

        });

    };  // End addLink()

    function toggleEditBtn() {
        var $removeField = $(".show_field");
        $removeField.prop('hidden', !$removeField.is(":hidden"));
        var $icon = $('.addlink_manage').find('i');
        $icon.toggleClass('fa-pencil fa-check');
        var $text = $('.addlink_manage').find('.addlink_manage-txt');
        $text.text() == "Manage Links" ? $text.text("Finished Editing") : $text.text("Manage Links");
    }
});


function RegExForUrlMatch(link) {

    var expression = /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/g;

    var regex = new RegExp(expression);
    var t = link;

    if (t.match(regex)) {

        return true;
    } else {
        return false;
    }
}

