﻿$(document).ready(function () {
    //init site facets
    querySiteFacets();

    //Site Search Search
    $('.gs-submit').on('click', function () {
        SiteSearch();
    });

    // Detect Enter Keypress
    $(document).keypress(function (e) {
        var keycode = (e.keyCode ? e.keyCode : e.which);
        if (keycode == '13') {
            if ($('#gs-input').is(':focus') || $('#gs-mobile-input').is(':focus')) {
                SiteSearch();
            }
        }
    });

    
    ////////////// HEADER //////////////
    //search btn displays text of selected dropdown-menu anchor
    $('.input-group-btn .dropdown-menu li').on('click', function () {
        $(this).parents(".dropdown").find('.btn').html($(this).text() + ' <i class="fa fa-angle-down"></i>');
        var newTxt = $(this).closest('.input-group-btn').find('.gs-filter').text().trim();
        $('.gs-filter').text(newTxt);
    })

    ////////////// PROFILE PAGE //////////////

    // Toggle Font Awesome fa-angle-down to fa-angle-up on click (profile details accordion)
    $('a.fa-toggle').click(function () {
        $(this).find('i').toggleClass('fa-angle-down fa-angle-up')
    });

    $('a.fa-toggle-faq').click(function () {
        $(this).find('i').toggleClass('fa-plus fa-minus')
    });

    ////////////// LANDING PAGE (REHAB) & MEDICAL LIBRARY PAGE //////////////

    $(".pp-breadcrumbs ol li a").each(function () {
        if (this.href == window.location.href) {
            $(this).addClass("current-link");
        }
    });

    $('.landing-slick').slick({
        slidesToShow: 1,
        arrows: true,
        dots: false,
        fade: true,
        responsive: [
	  		{
	  		    breakpoint: 560,
	  		    settings: {
	  		        arrows: false,
	  		        dots: true
	  		    }
	  		}
        ]
    });

    var $librariesSlidesCounter = $('.landing-libraries-row .slick-slide').length;

    if ($librariesSlidesCounter <= 3) {
        $('.library-slick').slick({
            infinite: true,
            slidesToShow: $librariesSlidesCounter,
            slidesToScroll: $librariesSlidesCounter,
            arrows: false,
            dots: false,
            fade: false,
            //autoplay: true,
            //autoplaySpeed: 2000,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        arrows: false,
                        dots: true
                    }
                }
            ]
        });
    } else if ($librariesSlidesCounter === 4) {
        $('.library-slick').slick({
            infinite: true,
            slidesToShow: 4,
            slidesToScroll: 4,
            arrows: false,
            dots: false,
            fade: false,
            //autoplay: true,
            //autoplaySpeed: 2000,
            responsive: [
                {
                    breakpoint: 1025,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        arrows: true,
                        dots: false,
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        arrows: false,
                        dots: true
                    }
                }
            ]
        });
    } else if ($librariesSlidesCounter > 4) {
        $('.library-slick').slick({
            infinite: true,
            slidesToShow: 4,
            slidesToScroll: 4,
            arrows: true,
            dots: false,
            fade: false,
            //autoplay: true,
            //autoplaySpeed: 2000,
            responsive: [
		  		{
		  		    breakpoint: 1025,
		  		    settings: {
		  		        slidesToShow: 3,
		  		        slidesToScroll: 3,
		  		        arrows: true,
		  		        dots: false,
		  		    }
		  		},
		  		{
		  		    breakpoint: 768,
		  		    settings: {
		  		        slidesToShow: 1,
		  		        slidesToScroll: 1,
		  		        arrows: false,
		  		        dots: true
		  		    }
		  		}
            ]
        });
    }
})

////////////// SITE SEARCH RESULTS PAGE //////////////

// Set viewport with to device width
function viewport() {
    var e = window, a = 'inner';
    if (!('innerWidth' in window)) {
        a = 'client';
        e = document.documentElement || document.body;
    }
    return { width: e[a + 'Width'], height: e[a + 'Height'] };
}

// On resize, if viewport width < 992px, make site-search filter collapsable
(function ($) {
    var $window = $(window),
        $siteSearchFilter = $('#accordion-site-search-filter');
    $siteSearchFilterWrapper = $('.site-search-filter-wrapper');
    $landingRelated = $('#accordion-landing-related');
    $detailsRelated = $('#accordion-details-related');
    $accordionLibrary = $('#accordion-library');
    $landingRelatedWrapper = $('.landing-related-wrapper');
    // $landingColWrap = $('.landing-col-wrapper');

    $window.resize(function () {
        if (viewport().width < 992) {
            //site search results page
            $siteSearchFilter.addClass('collapse');
            $siteSearchFilterWrapper.addClass('site-search-filter-wrapper-mobile');
            $siteSearchFilterWrapper.removeClass('site-search-filter-wrapper');
            //landing page
            $landingRelated.addClass('collapse');
            //medical library
            $detailsRelated.addClass('collapse');
            $accordionLibrary.addClass('collapse');
            //console.log('greater than 992!');

            return;
        }
        //site search results page
        $siteSearchFilter.removeClass('collapse');
        $siteSearchFilterWrapper.addClass('site-search-filter-wrapper');
        $siteSearchFilterWrapper.removeClass('site-search-filter-wrapper-mobile');
        //landing page
        // $landingRelatedWrapper.css("height", "100%");
        // $landingRelatedWrapper.addClass('landing-related-wrapper');
        // $landingRelatedWrapper.removeClass('landing-related-wrapper-mobile');
        $landingRelated.removeClass('collapse');
        $landingRelated.css("height", "auto");
        $detailsRelated.removeClass('collapse');
        $detailsRelated.css("height", "auto");
        // $detailsRelated.removeClass('collapsed');
        $accordionLibrary.removeClass('collapse');
        $accordionLibrary.css("height", "auto");
        // $landingColWrap.addClass('vertical-align');
        //console.log('less than 992!');

    }).trigger('resize');
})(jQuery);

function SiteSearch() {
    var query;
    if ($('#gs-input').is(':visible')) {
        query = $('#gs-input').val().trim();
    } else {
        query = $('#gs-mobile-input').val().trim();
    }

    if (query && query != '') {
        var filter;

        if ($('#gs-input').is(':visible')) {
            filter = $('#gs-input').closest('.input-group').find('.gs-filter').text().trim();
        } else {
            filter = $('#gs-mobile-input').closest('.input-group').find('.gs-filter').text().trim();
        }

        //console.log('filter = ', filter);

        var filters = {};

        if (filter == "People") {
            filters["_template"] = "1a889b78c81e424fa416e51c4c34d343"; // Physician Template ID
        } else if (filter == "Articles") {
            filters["_template"] = "28345347c9b4460eb4e77f5890a42724"; //Article Template ID
        } else if (filter == "Events") {
            filters["_template"] = "693f3916a5434fd2894628b9ce58bf53"; //Event Template ID
        } else if (filter == "Documents") {
            filters["_template"] = "7e87Cf069fdc434cbdaa37808a4F751f,166927339a6145e6b0d44c0c06f8dd3c,7bb0411f50cd4c21ad8f1fcde7c3affe,0603f16635b8469f8123e8d87bedc171"; //Document Template ID
        } else if (filter == "Pages") {
            filters["_template"] = "11df60986a874552b1b3ea0c17bed057,c4b668aa8c554e8daaa9824e1f1562fc,8e5c6d895e7a4a778209dadd7f17a539"; // 1 Column Template ID, 2 Column Left Template ID, Two Column Even Template ID
        }

        var searchQuery = {};
        searchQuery.Query = (query != '*' && query != "Any" && query != "any") ? query : '*';
        searchQuery.Filters = filters;
        searchQuery.Start = 0;

        sessionStorage.setItem('sessionQuery', JSON.stringify(searchQuery));

        window.location = "/search/site search results";
    }
}


function parseSiteFacetsTypeahead(facets, type) {
    //console.log('parseSiteFacetsTypeahead')
    var json = [];
    $(facets).each(function () {
        json.push({
            "label": this.Key,
            // "value": actual value
            // "link": url
            "type": type
        })
    });
    //console.log(json);
    return json;
}

// parses solr response to build typeahead array
function parseSiteTypeahead(data) {
    //console.log('parseTypeahead');
    var json = [];
    $(data).each(function () {
        var name;
        if (this.FirstName && this.LastName) {
            name = this.FirstName + ' ' + this.LastName;
            if (this.Suffix && this.Suffix !== '') {
                name += ', ' + this.Suffix;
            }
        }
        
        if (this.Title) {
            name = this.Title;
        }

        json.push({
            "label": name,
            // "value": actual value
            // "link": url
            "type": "Doctor"

        });
    });

    return json;
}

// Show universal modal
function showModal(modalTitle, modalBody, close, redirectUrl, submitFunction, mode) {
    $('#universalModal .modal-title').html(modalTitle);
    $('#universalModal .modal-body').html(modalBody);

    if (submitFunction) {
        $('#universalModal .confirm-btn').removeClass('close-modal');
        //console.log('binding function ', submitFunction);
        $('#//universalModal').on('click', '.confirm-btn', function () {
            console.log('calling function');
            submitFunction();
            $('#universalModal').modal('hide');
        });
    } else {
        //console.log('no function to bind');
        $('#universalModal').off('click', '.confirm-btn');
        $('#universalModal .confirm-btn').addClass('close-modal');
    }
    if (typeof mode === "undefined" || mode === null || mode == '') {
        $('#saveModal').show();
        $('#cancelModal').show();
        $('#closeModal').hide();
    }
    else if (mode === 1) {
        $('#saveModal').hide();
        $('#cancelModal').hide();
        $('#closeModal').show();
    }

    if (redirectUrl) {
        $('.close-modal').on('click', function () {
            window.location.href = redirectUrl;
        });
    } else {
        $('.close-modal').off('click');
    }

    if (close == false) {
        $('#universalModal').modal({ backdrop: 'static', keyboard: false, show: true });
    } else {
        $('#universalModal').modal('show');
    }
}


