﻿var queryResponse;
var filters = {};

$(document).ready(function () {
    //Check for a previous query
    var sessionQuery = JSON.parse(sessionStorage.getItem("sessionQuery"));
    if (sessionQuery) {
        console.log(sessionQuery);
        if (sessionQuery.Query) {
            //Populate global site search inputs w/ previous query
            $('#gs-input, #gs-mobile-input').val(sessionQuery.Query);
        }
        sessionStorage.removeItem("sessionQuery");
        querySolr(sessionQuery);
    }

    $('#site-search-form').on('submit', function (e) {
        e.preventDefault();
        querySolr(buildQuery());
    })

    $('.query-solr').on('click', function (e) {
        e.preventDefault();
        querySolr(buildQuery());
    });

    $('.site-search-filter-clear').on('click', function () {
        //Uncheck all filters
        $('input[name="type"]').prop('checked', false);
        //Query Solr
        querySolr(buildQuery());
    });

    $('.site-search-filter').on('click', function () {
        var filtering;
        while (filtering == undefined || filtering == false) {
            filtering = true;
            querySolr(buildQuery());
            filtering = false;
            break;
        }
    });
});



function buildQuery() {
    var searchQuery = {};
    filters = {};

    filters['_template'] = $('input[name="type"]:checked').map(function () {
        return this.value;
    }).get().join();

    var query = $('#site-search-input').val();
    $('#gs-input, #gs-mobile-input').val(query);
    searchQuery.Query = (query != '*' && query != "Any" && query != "any") ? query : '*';
    searchQuery.Filters = filters;
    searchQuery.Start = 0;
    //searchQuery.Rows = rows;
    //searchQuery.Sort = sort;
    //searchQuery.SortOrder = sortOrder;

    return searchQuery;
}


function showResults(data) {
    if (data.Status == "Error") {
        //console.log('Error - ', data);
    } else {
        //console.log('data ', data);
        queryResponse = data;

        $('#results').html(data.Results);

        var resultCount = data.TotalHits;
        
        var term;

        if (data.OriginalQuery && data.OriginalQuery.Query) {
            term = data.OriginalQuery.Query;
        }

        var searchTerm = (term && term != "*" && term != "Any" && term != 'any') ? term : "Any";

        $('#site-search-input').val(searchTerm);

        $('#result-count').text(resultCount);
        $('#search-term').text('"' + searchTerm + '"');

        //if (data.OriginalQuery && data.OriginalQuery.Filters && data.OriginalQuery.Filters['_template']) {
        //    var splitFilters = data.OriginalQuery.Filters['_template'].split(',');

        //    for (var i = 0; i < splitFilters.length; i++) {
        //        $('input[name="type"').each(function () {
        //            if ($(this).val().indexOf(splitFilters[i]) != -1) {
        //                $(this).prop('checked', true);
        //            }
        //        })
        //    }
        //}

        // pagination
        pagination();

        // reveal results, counts, etc.
        $('.show-on-results').show();
    }
}

// pagination
function pagination() {
    $('#pagination').twbsPagination('destroy');
    if (queryResponse.TotalHits && queryResponse.OriginalQuery.Rows) {
        //Build and populate "now showing" info
        var totalPages = Math.ceil(queryResponse.TotalHits / queryResponse.OriginalQuery.Rows);
        var total = queryResponse.TotalHits;
        var start = queryResponse.OriginalQuery.Start + 1;
        var finish = start + (queryResponse.OriginalQuery.Rows - 1);
        finish = finish < total ? finish : total;
        var nowShowing = "Showing " + start + " - " + finish + " of " + total + " entries"
        $('#now-showing').text(nowShowing);

        //Calculate pages and build pagination
        var currentPage = queryResponse.OriginalQuery.Start != 0 ? (queryResponse.OriginalQuery.Start / queryResponse.OriginalQuery.Rows) + 1 : 1;

        if (total > 1) {
            $('#pagination').twbsPagination({
                totalPages: totalPages,
                startPage: currentPage,
                visiblePages: 5,
                initiateStartPageClick: false,
                first: '<span class="dcenter-first dcenter-page-link fa fa-angle-double-left"></span>',
                prev: '<span class="dcenter-prev dcenter-page-link fa fa-angle-left"></span>',
                next: '<span class="dcenter-next dcenter-page-link fa fa-angle-right"></span>',
                last: '<span class="dcenter-last dcenter-page-link fa fa-angle-double-right"></span>',
                onPageClick: function (event, page) {
                    pageQuery((page - 1) * queryResponse.OriginalQuery.Rows);
                }
            });
        }
    }
};

// pagination page query
function pageQuery(number) {
    //console.log(number);
    var query = queryResponse.OriginalQuery;
    query.Start = number;
    querySolr(query);
}