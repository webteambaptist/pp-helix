﻿$(document).ready(function () {
  var formID = $('#taskModal').find('form').attr('id');

  updateCounter();

  //If a Task Title is clicked, check the corresponding checkbox
  //Once a task is checked it should not be unchecked.
  $('.task-title').on('click', function () {
    var $checkbox = $(this).closest('li').find('.submit-task');

    if (!$checkbox.prop('checked')) {
      $checkbox.prop('checked', true).change();
    }

    updateCounter();
  });

  //If checkbox is checked, prepopulate fields & open modal
  $('.submit-task').on('change', function () {
    if ($(this).prop('checked')) {
      updateCounter();
      //Make Modal Title = Task Name
      $('#taskModal .modal-title').text($(this).val());
      var taskTitle = $(this).val();

      //Populate and Hide Task Name Input
      $('input[name="' + formID + '.Sections[0].Fields[0].Value"]').val($(this).val());
      $('input[name="' + formID + '.Sections[0].Fields[0].Value"]').closest('.form-group').hide();

      //Populate and Hide Task ID Input
      var taskID = $(this).attr('id');
      $('input[name="' + formID + '.Sections[0].Fields[1].Value"]').val(taskID);
      $('input[name="' + formID + '.Sections[0].Fields[1].Value"]').closest('.form-group').hide();

      //Add Task ID to session storage as lastTaskID
      sessionStorage.setItem("lastTaskID", taskID);

      //Populate and Hide Physician's Name Input
      var name = $('#FN').text() + " " + $('#LN').text();
      $('input[name="' + formID + '.Sections[0].Fields[2].Value"]').val(name);
      $('input[name="' + formID + '.Sections[0].Fields[2].Value"]').closest('.form-group').hide();

      var newHtml = "";
      //Name
      newHtml = '<div class="row">'
      newHtml += '<div class="col-md-12 text-center"><h2>' + name + '</h2></div>'
      newHtml += '</div>'

      newHtml += '<br/>'

      //Comments field
      newHtml += '<div class="row">'
      newHtml += '<div class="col-md-12 text-center">'
      newHtml += '<textarea class="form-control" id="taskComments" name="taskComments" placeholder="Comments" required rows="10" cols="30"></textarea>'
      newHtml += '</div>'
      newHtml += '</div>'

      newHtml += '<br/>'

      //Attachments
      newHtml += '<div class="row">'
      newHtml += '<div class="col-md-12 text-center">'
      newHtml += '<input type="file" id="taskAttachments" name="taskAttachments">'
      newHtml += '</div>'
      newHtml += '</div>'

      newHtml += '<br/>'

      //Submit button
      newHtml += '<div class="row">'
      newHtml += '<div class="col-md-3 text-center"></div>'
      newHtml += '<div class="col-md-6 text-center">'
      newHtml += '<input id="UpdateTask" type="submit" value="I Have Completed This Task"/>'
      newHtml += '<div class="col-md-3 text-center"></div>'
      newHtml += '</div>'

      $('#taskModal .modal-body').append(newHtml);

      //Open Completed Task Form Modal 
      $('#taskModal').modal({ backdrop: 'static', keyboard: true, show: true });

      $('#UpdateTask').on('click', function () {
        var comments = $('#taskComments').val();

        var echoId = $('#echoID').text();
        //var echoId = "01853";

        //Generate uploaded file
        var fileUpload = $('#taskAttachments').get(0).files[0];
        if (fileUpload != null) {
          var getExtension = fileUpload.name.split('.').pop();
          var newFileName = echoId + '_' + taskID + '_' + taskTitle + '.' + getExtension;
          var fileData = new FormData();

          fileData.append('file', fileUpload, newFileName);

          $.ajax({
            url: '/api/Sitecore/TaskManager/UploadFile',
            type: 'POST',
            datatype: 'json',
            contentType: false,
            processData: false,
            async: false,
            data: fileData,
            success: function (response) {
              if (response != null && response == 'success') {
                $.ajax({
                  url: '/api/Sitecore/TaskManager/UpdateTask',
                  type: 'POST',
                  datatype: 'json',
                  data: {
                    taskID: taskID,
                    comments: comments,
                    file: newFileName
                  },
                  success: function (response) {
                    if (response != null && response == 'success') {
                      document.location.reload();
                    }
                  },
                });
              }
            },
          });
        }
        else {
          $.ajax({
            url: '/api/Sitecore/TaskManager/UpdateTask',
            type: 'POST',
            datatype: 'json',
            data: {
              taskID: taskID,
              comments: comments
            },
            success: function (response) {
              if (response != null && response == 'success') {
                document.location.reload();
              }
            },
          });
        }
      })
    }
  });

  //If modal is dismissed without valid submission, uncheck checkbox
  $('#taskModal').on('hide.bs.modal', function () {
    //Only uncheck the most recently checked task
    var lastTaskID = sessionStorage.getItem("lastTaskID");
    if (lastTaskID) {
      $('.submit-task[id="' + lastTaskID + '"]').prop('checked', false);
    }
    updateCounter();
  });
});

//Update Counter
function updateCounter() {
  var numOpenTasks = $('.submit-task:not(:checked)').length;
  $('.tasklisk-counter').text(numOpenTasks);
}