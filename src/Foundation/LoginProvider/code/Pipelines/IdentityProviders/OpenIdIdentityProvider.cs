﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Microsoft.Owin.Infrastructure;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OpenIdConnect;
using Owin;
using Sitecore;
using Sitecore.Abstractions;
using Sitecore.Diagnostics;
using Sitecore.Owin.Authentication.Configuration;
using Sitecore.Owin.Authentication.Extensions;
using Sitecore.Owin.Authentication.Pipelines.IdentityProviders;
using Sitecore.Owin.Authentication.Services;

namespace PP93Helix.Foundation.LoginProvider.Pipelines.IdentityProviders
{
  class OpenIdIdentityProvider : IdentityProvidersProcessor
  {

    private readonly string _authority = Sitecore.Configuration.Settings.GetSetting("authority");
    private readonly string _clientId = Sitecore.Configuration.Settings.GetSetting("clientid");
    private readonly string _clientSecret = Sitecore.Configuration.Settings.GetSetting("clientsecret");
    private readonly string _redirectUri = Sitecore.Configuration.Settings.GetSetting("redirecturi");

    public OpenIdIdentityProvider(FederatedAuthenticationConfiguration federatedAuthenticationConfiguration,
      ICookieManager cookieManager, BaseSettings settings)
      : base(federatedAuthenticationConfiguration, cookieManager, settings)
    {
    }

    protected override string IdentityProviderName => "openID";

    protected override void ProcessCore(IdentityProvidersArgs args)
    {
      Assert.ArgumentNotNull(args, "args");
      var identityProvider = this.GetIdentityProvider();
      var authenticationType = this.GetAuthenticationType();
      args.App.UseOpenIdConnectAuthentication(new OpenIdConnectAuthenticationOptions
      {
        Authority = _authority,
        ClientId = _clientId,
        ClientSecret = _clientSecret,
        RedirectUri = _redirectUri,
        AuthenticationType = authenticationType,
        AuthenticationMode = AuthenticationMode.Passive,
        Caption = identityProvider.Caption,
        CookieManager = CookieManager,
        Notifications = new OpenIdConnectAuthenticationNotifications
        {
          SecurityTokenValidated = notification =>
          {
            try
            {
              notification.AuthenticationTicket.Identity.ApplyClaimsTransformations(
                new TransformationContext(FederatedAuthenticationConfiguration, identityProvider));

              Log.Info("Transform applied successfully (" + identityProvider.Transformations.Count + ")", this);
            }
            catch (Exception e)
            {
              Log.Error("Error applying transform" + e.Message, this);
            }
             
            return Task.CompletedTask;
          }
        }
      });
    }
  }
}
