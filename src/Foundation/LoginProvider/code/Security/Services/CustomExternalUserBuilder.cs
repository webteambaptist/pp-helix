﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Sitecore.Diagnostics;
using Sitecore.Owin.Authentication.Identity;
using Sitecore.Owin.Authentication.Services;
using Sitecore.SecurityModel.Cryptography;

namespace PP93Helix.Foundation.LoginProvider.Security.Services
{
    public class CustomExternalUserBuilder : DefaultExternalUserBuilder
    {
      public CustomExternalUserBuilder(ApplicationUserFactory applicationUserFactory, IHashEncryption hashEncryption) : base(applicationUserFactory, hashEncryption)
      {
        Log.Info($"Creating a new instance of {typeof(CustomExternalUserBuilder)}", this);
      }
      public override ApplicationUser BuildUser(UserManager<ApplicationUser> userManager, ExternalLoginInfo externalLoginInfo)
      {
        Log.Info("Starting to build user: ", userManager);
        ApplicationUser user = this.ApplicationUserFactory.CreateUser(this.CreateUniqueUserName(userManager, externalLoginInfo));
        user.IsVirtual = false;
        Log.Info("User Built: " + user.UserName, user);
        return user;
      }
      protected override string CreateUniqueUserName(UserManager<ApplicationUser> userManager, ExternalLoginInfo externalLoginInfo)
      {
        Log.Info("Creating new User ", userManager);
        Assert.ArgumentNotNull((object)userManager, nameof(userManager));
        Assert.ArgumentNotNull((object)externalLoginInfo, nameof(externalLoginInfo));
        var identityProvider = this.FederatedAuthenticationConfiguration.GetIdentityProvider(externalLoginInfo.ExternalIdentity);
        if (identityProvider == null)
          throw new InvalidOperationException("Unable to retrieve identity provider for given identity");
        var username = externalLoginInfo.ExternalIdentity.Claims.Where(x => x.Type == ClaimTypes.NameIdentifier)
          .Select(x => x.Value).FirstOrDefault();
              
        Log.Info("New User being created " + username, this);
        return username;
      }
    }
}
