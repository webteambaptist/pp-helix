﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Sitecore.Diagnostics;
using Sitecore.Owin.Authentication.Services;

namespace PP93Helix.Foundation.LoginProvider.Security.Transformations
{
    public class AddRoles : Transformation
    {
      public override void Transform(ClaimsIdentity identity, TransformationContext context)
      {
        Log.Info("Adding Claims... ", this);
        string[] claims = new string[] { };
        foreach (var claim in identity.FindAll("formatted"))
        {
          claims = claim.Value.Split(',').ToArray();
        }
        foreach (var claim in claims)
        {
          identity.AddClaim(new Claim(ClaimTypes.Role, @"ad\" + claim));
        }
        var username = identity.FindAll("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier");
          
        var value = username.Select(c => c.Value).SingleOrDefault();
        identity.RemoveClaim(identity.FindFirst("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier"));
        identity.AddClaim(value != null && value.Contains(@"ad\")
          ? new Claim(ClaimTypes.NameIdentifier, value)
          : new Claim(ClaimTypes.NameIdentifier, @"ad\" + value));
        //var email = identity.FindAll("preferred_username").Select(x => x.Value).FirstOrDefault();
        //identity.AddClaim(new Claim(ClaimTypes.Email, email));
        foreach (var claim in identity.Claims)
        {
          Log.Info("Claim.Type: " + claim.Type + " Claim.Value= " + claim.Value, claim);
        }
      }
    }
}
